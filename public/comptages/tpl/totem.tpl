{include file='./header.tpl'}
<style>
@font-face {
 font-family: "Dot Matrix";
 src: url("css/LED Dot-Matrix.ttf") format("truetype")
}

.totem
{
    width: calc(100% - 4em);
    position: relative;
    padding: 2em;
}

.totem .fond-totem
{
    background: #52d1c5;
    width: calc(80% - 4em);
    position: absolute;
    height: 100%;
    top: 0;
    right: 10%;
    padding: 2em;
    z-index: -1;
}

.totem .infos .label
{
    text-align: center;
    color: white;
    font-weight: bold;
    font-size: 3em;
}

.totem .compteur
{
    background: black;
    width: 100%;
    color: #2cff00;
    font-size: 8em;
    text-align: right;
    font-family: "Dot Matrix", "Open sans", serif;
}

@media screen and (max-width: 1023px) {
    #content
    {
        padding: 0;
    }

    .totem
    {
        width: 100%;
        position: relative;
        padding: 0;
    }

    .totem .fond-totem
    {
        background: #52d1c5;
        width: calc(80% - 1em);
        position: absolute;
        height: 100%;
        top: 0;
        right: 10%;
        padding: 0.5em;
        z-index: -1;
    }

    .totem .infos
    {
        padding-top: 5%;
        padding-bottom: 30%;
    }
    
    .totem .infos .label
    {
        padding-top: 1em;
        font-size: 1.5em;
        padding-left: 10%;
        padding-right: 10%;
    }

    .totem .compteur
    {
        font-size: 4em;
    }

    .totem .compteur.jour
    {
        font-size: 6em;
    }
}
</style>
<header>
    <h1>Comptages Nocle (site non officiel) - Point de comptage {$stationCode}</h1>
    <nav>
        <a href="index.php">&lt; Retour à l'accueil</a> |
        <a href="station.php?code={$stationCode}">&lt; Retour au point de comptage</a>
    </nav>
</header>
<div id="content">
    <div class="totem">
        <div class="infos">
            <div class="label">Cyclistes cette année</div>
            <div id="compteur-an" class="compteur">---</div>
            <div class="label">Cyclistes hier à la même heure</div>
            <div id="compteur-jour" class="compteur jour">---</div>
        </div>

        <div class="fond-totem"></div>
    </div>

    <script src="./js_comptage/totem.js"></script>
    <script>
        $(document).ready(function() {
            getDataTotem("{$stationCode}");
        });
    </script>
    {include file="./credits.tpl"}
</div>
{include file="./footer.tpl"}