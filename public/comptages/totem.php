<?php
require_once '../../config.php';
include_once '../../functions.php';
require_once '../../libs/Smarty.class.php';

$smarty = new Smarty();

if (!isset($_GET['code'])) {
    header('location: index.php');
    exit();
}

//On récupère la station
$code = htmlspecialchars($_GET['code']);
$requete = $pdo->query('SELECT * FROM comptage_stations WHERE code = ' . $code);
if ($requete === false) {
    header('location: index.php');
    exit();
}
$station = $requete->fetch();
if ($station === false) {
    header('location: index.php');
    exit();
}
$smarty->assign(array(
    'stationCode' => $code,
    'stationNom' => $station['nom'],
    'stationDateOuverture' => (new DateTime($station['dateOuverture']))->format('d/m/Y'),
    'stationLat' => $station['latitude'],
    'stationLong' => $station['longitude'],
));

$smarty->display('tpl/totem.tpl');
exit();
