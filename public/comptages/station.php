<?php
require_once '../../config.php';
include_once '../../functions.php';
require_once '../../libs/Smarty.class.php';

$smarty = new Smarty();

if (!isset($_GET['code'])) {
    header('location: index.php');
    exit();
}

//On récupère la station
$code = htmlspecialchars($_GET['code']);
$requete = $pdo->query('SELECT s.*, p.nom as parentNom FROM comptage_stations s LEFT JOIN comptage_stations p ON p.code = s.parent WHERE s.code = "' . $code . '"');
if ($requete === false) {
    header('location: index.php');
    exit();
}
$station = $requete->fetch();
if ($station === false) {
    header('location: index.php');
    exit();
}

$titre;
$enfants = array();
if ($station['type'] == 1) {
    $titre = 'Point de comptage ' . $station['code'] . ' − ' . $station['nom'];
} else {
    $titre = 'Regroupement ' . $station['code'] . ' − ' . $station['nom'];

    // Et on va rechercher ses enfants
    $requete = $pdo->query('SELECT code, nom FROM comptage_stations WHERE parent = "' . $code . '"');
    if ($requete !== false) {
        $codeEnfants = $requete->fetchAll();
        foreach ($codeEnfants as $stationEnfant) {
            $enfants[] = array(
                'code' => $stationEnfant['code'],
                'nom' => $stationEnfant['nom'],
            );
        }
    }
}

$smarty->assign(array(
    'stationCode' => $code,
    'stationNom' => $station['nom'],
    'stationTitre' => $titre,
    'stationHasEnfant' => count($enfants) > 0,
    'stationEnfants' => $enfants,
    'stationHasParent' => !is_null($station['parent']),
    'stationParentCode' => $station['parent'],
    'stationParentNom' => $station['parentNom'],
    'stationDateOuverture' => (new DateTime($station['dateOuverture']))->format('d/m/Y'),
    'stationLat' => $station['latitude'],
    'stationLong' => $station['longitude'],
));

$smarty->display('tpl/station.tpl');
exit();
