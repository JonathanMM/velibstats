import os
import toml
from service import Service

services = None


def loadService():
    pathConfigDir = "../config/"
    configsFile = os.listdir(pathConfigDir)
    configs = []
    for fileNameConf in configsFile:
        configToml = toml.load(pathConfigDir + fileNameConf)
        service = Service(configToml)
        configs.append(service)
    return configs


def getConfigService(slug):
    global services
    if services == None:
        services = loadService()

    for service in services:
        if service.slug == slug:
            return service

    raise Exception("Service non trouvé")
