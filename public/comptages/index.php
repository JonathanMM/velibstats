<?php
require_once '../../config.php';
include_once '../../functions.php';
require_once '../../libs/Smarty.class.php';

$smarty = new Smarty();

$smarty->assign(array(
    'pageId' => 'comptage',
));

//Filtre 24 heures
$hier = new DateTime("-10day");
$filtreDate = $hier->format('Y-m-d H:i:s');

//Dernière conso
$requete = $pdo->query('SELECT * FROM `comptage_resumeConso` where nbStation Is not null and date >= "' . $filtreDate . '" and duree = 1440 Order by date desc limit 0,1');
$conso = $requete->fetch();

$smarty->assign(array(
    'idConso' => $conso['id'],
    'dateResume' => $conso['date'],
    'nbStation' => $conso['nbStation'],
    'nbPassage' => $conso['nombre'],
    'dateDerniereConso' => (new DateTime($conso['date']))->format('d/m/Y'),
));

$smarty->display('tpl/index.tpl');
exit();
