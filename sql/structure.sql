-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 01 Mai 2020 à 16:44
-- Version du serveur :  10.1.44-MariaDB-0ubuntu0.18.04.1
-- Version de PHP :  7.0.22-0ubuntu0.17.04.1
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;
/*!40101 SET NAMES utf8mb4 */
;
--
-- Base de données :  `c6velib`
--
-- --------------------------------------------------------
--
-- Structure de la table `comptage_resumeConso`
--
CREATE TABLE `comptage_resumeConso` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `duree` int(4) NOT NULL,
  `nbStation` int(5) NOT NULL,
  `nombre` int(6) DEFAULT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `comptage_resumeStatus`
--
CREATE TABLE `comptage_resumeStatus` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `duree` int(4) NOT NULL,
  `nombre` int(6) NOT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = COMPACT;
-- --------------------------------------------------------
--
-- Structure de la table `comptage_stations`
--
CREATE TABLE `comptage_stations` (
  `code` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `dateOuverture` date DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `parent` int(11) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `comptage_status`
--
CREATE TABLE `comptage_status` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `idConso` int(11) NOT NULL,
  `nombre` int(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `comptage_statusConso`
--
CREATE TABLE `comptage_statusConso` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `nbStation` int(11) NOT NULL DEFAULT '0',
  `nombre` int(11) NOT NULL DEFAULT '0'
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `config`
--
CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `cle` varchar(64) NOT NULL,
  `valeur` varchar(256) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
--
-- Contenu de la table `config`
--
INSERT INTO `config` (`id`, `cle`, `valeur`)
VALUES (
    1,
    'comptage-derniere-maj',
    '2020-05-01T05:53:39+00:00'
  );
-- --------------------------------------------------------
--
-- Structure de la table `resumeConso`
--
CREATE TABLE `resumeConso` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `duree` int(4) NOT NULL,
  `nbStation` int(5) NOT NULL,
  `nbStationDetecte` int(5) DEFAULT NULL,
  `nbBikeMin` int(6) NOT NULL,
  `nbBikeMax` int(6) NOT NULL,
  `nbBikeMoyenne` decimal(8, 2) NOT NULL,
  `nbEBikeMin` int(6) NOT NULL,
  `nbEBikeMax` int(6) NOT NULL,
  `nbEBikeMoyenne` decimal(8, 2) NOT NULL,
  `nbBikeOverflowMin` int(6) DEFAULT NULL,
  `nbBikeOverflowMax` int(6) DEFAULT NULL,
  `nbBikeOverflowMoyenne` decimal(8, 2) DEFAULT NULL,
  `nbEbikeOverflowMin` int(6) DEFAULT NULL,
  `nbEbikeOverflowMax` int(6) DEFAULT NULL,
  `nbEbikeOverflowMoyenne` decimal(8, 2) DEFAULT NULL,
  `nbFreeEDockMin` int(6) NOT NULL,
  `nbFreeEDockMax` int(6) NOT NULL,
  `nbFreeEDockMoyenne` decimal(8, 2) NOT NULL,
  `nbEDock` int(6) NOT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `resumeStatus`
--
CREATE TABLE `resumeStatus` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `duree` int(4) NOT NULL,
  `nbBikeMin` int(3) NOT NULL,
  `nbBikeMax` int(3) NOT NULL,
  `nbBikeMoyenne` decimal(5, 2) NOT NULL,
  `nbBikePris` int(3) NOT NULL,
  `nbBikeRendu` int(3) NOT NULL,
  `nbEBikeMin` int(3) NOT NULL,
  `nbEBikeMax` int(3) NOT NULL,
  `nbEBikeMoyenne` decimal(5, 2) NOT NULL,
  `nbEBikePris` int(3) NOT NULL,
  `nbEBikeRendu` int(3) NOT NULL,
  `nbFreeEDockMin` int(3) NOT NULL,
  `nbFreeEDockMax` int(3) NOT NULL,
  `nbFreeEDockMoyenne` decimal(5, 2) NOT NULL,
  `nbEDock` int(3) NOT NULL,
  `nbBikeOverflowMin` int(3) NOT NULL,
  `nbBikeOverflowMax` int(3) NOT NULL,
  `nbBikeOverflowMoyenne` decimal(5, 2) NOT NULL,
  `nbEBikeOverflowMin` int(3) NOT NULL,
  `nbEBikeOverflowMax` int(3) NOT NULL,
  `nbEBikeOverflowMoyenne` decimal(5, 2) NOT NULL,
  `maxBikeOverflow` int(3) NOT NULL,
  `nbEDockPerdusMin` int(3) DEFAULT NULL,
  `nbEDockPerdusMax` int(3) DEFAULT NULL,
  `nbEDockPerdusMoyenne` decimal(5, 2) DEFAULT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = COMPACT;
-- --------------------------------------------------------
--
-- Structure de la table `signalement`
--
CREATE TABLE `signalement` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `dateSignalement` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estFonctionnel` tinyint(1) NOT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `stations`
--
CREATE TABLE `stations` (
  `code` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `latitude` decimal(16, 14) NOT NULL,
  `longitude` decimal(16, 14) NOT NULL,
  `type` varchar(256) NOT NULL,
  `dateOuverture` date DEFAULT NULL,
  `adresse` text,
  `insee` int(5) DEFAULT NULL,
  `stationId` varchar(16) DEFAULT NULL,
  `lastRefresh` datetime DEFAULT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `statsJournaliere`
--
CREATE TABLE `statsJournaliere` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `nbJour` int(3) NOT NULL,
  `nbBikePris` int(8) DEFAULT NULL,
  `nbBikeRendu` int(8) DEFAULT NULL,
  `nbEBikePris` int(8) DEFAULT NULL,
  `nbEBikeRendu` int(8) DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `status`
--
CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `idConso` int(11) NOT NULL,
  `state` varchar(32) NOT NULL,
  `nbBike` int(3) NOT NULL,
  `nbEBike` int(3) NOT NULL,
  `nbFreeEDock` int(3) NOT NULL,
  `nbEDock` int(3) NOT NULL,
  `nbBikeOverflow` int(3) NOT NULL,
  `nbEBikeOverflow` int(3) NOT NULL,
  `maxBikeOverflow` int(3) NOT NULL,
  `overflow` varchar(4) DEFAULT NULL,
  `overflowActivation` varchar(4) DEFAULT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8;
-- --------------------------------------------------------
--
-- Structure de la table `statusConso`
--
CREATE TABLE `statusConso` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nbStation` int(11) DEFAULT NULL,
  `nbStationDetecte` int(11) DEFAULT NULL,
  `nbBike` int(11) DEFAULT NULL,
  `nbEbike` int(11) DEFAULT NULL,
  `nbBikeOverflow` int(11) DEFAULT NULL,
  `nbEbikeOverflow` int(11) DEFAULT NULL,
  `nbFreeEDock` int(11) DEFAULT NULL,
  `nbEDock` int(11) DEFAULT NULL
) ENGINE = MyISAM DEFAULT CHARSET = utf8 ROW_FORMAT = FIXED;
--
-- Index pour les tables exportées
--
--
-- Index pour la table `comptage_resumeConso`
--
ALTER TABLE `comptage_resumeConso`
ADD PRIMARY KEY (`id`);
--
-- Index pour la table `comptage_resumeStatus`
--
ALTER TABLE `comptage_resumeStatus`
ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `duree` (`duree`);
--
-- Index pour la table `comptage_stations`
--
ALTER TABLE `comptage_stations`
ADD PRIMARY KEY (`code`);
--
-- Index pour la table `comptage_status`
--
ALTER TABLE `comptage_status`
ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Fusion` (`idConso`, `code`),
  ADD KEY `idConso` (`idConso`),
  ADD KEY `station` (`code`);
--
-- Index pour la table `comptage_statusConso`
--
ALTER TABLE `comptage_statusConso`
ADD PRIMARY KEY (`id`);
--
-- Index pour la table `config`
--
ALTER TABLE `config`
ADD PRIMARY KEY (`id`);
--
-- Index pour la table `resumeConso`
--
ALTER TABLE `resumeConso`
ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `duree` (`duree`);
--
-- Index pour la table `resumeStatus`
--
ALTER TABLE `resumeStatus`
ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `duree` (`duree`);
--
-- Index pour la table `signalement`
--
ALTER TABLE `signalement`
ADD PRIMARY KEY (`id`);
--
-- Index pour la table `stations`
--
ALTER TABLE `stations`
ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `stationId` (`stationId`),
  ADD KEY `insee` (`insee`),
  ADD KEY `latitude` (`latitude`),
  ADD KEY `longitude` (`longitude`);
--
-- Index pour la table `statsJournaliere`
--
ALTER TABLE `statsJournaliere`
ADD PRIMARY KEY (`id`);
--
-- Index pour la table `status`
--
ALTER TABLE `status`
ADD PRIMARY KEY (`id`),
  ADD KEY `station` (`code`),
  ADD KEY `idConso` (`idConso`);
--
-- Index pour la table `statusConso`
--
ALTER TABLE `statusConso`
ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`);
--
-- AUTO_INCREMENT pour les tables exportées
--
--
-- AUTO_INCREMENT pour la table `comptage_resumeConso`
--
ALTER TABLE `comptage_resumeConso`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `comptage_resumeStatus`
--
ALTER TABLE `comptage_resumeStatus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `comptage_status`
--
ALTER TABLE `comptage_status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `comptage_statusConso`
--
ALTER TABLE `comptage_statusConso`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `config`
--
ALTER TABLE `config`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `resumeConso`
--
ALTER TABLE `resumeConso`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `resumeStatus`
--
ALTER TABLE `resumeStatus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `signalement`
--
ALTER TABLE `signalement`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `statsJournaliere`
--
ALTER TABLE `statsJournaliere`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `status`
--
ALTER TABLE `status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `statusConso`
--
ALTER TABLE `statusConso`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Structure de la table `comptage_record`
--
CREATE TABLE `comptage_record` (
  `code` int(11) NOT NULL,
  `date` date NOT NULL,
  `nombre` int(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
ALTER TABLE `comptage_record`
ADD PRIMARY KEY (`code`),
  ADD KEY `date` (`date`) USING BTREE;
COMMIT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;