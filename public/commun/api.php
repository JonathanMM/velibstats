<?php
error_reporting(E_ALL);

if (!isset($_GET['action']) || strlen($_GET['action']) == 0) {
    exit();
}

if ((!isset($_GET['codeStation']) || intval($_GET['codeStation']) == 0) && (!isset($_GET['idConso']) || intval($_GET['idConso']) == 0)) {
    exit();
}

header('Content-Type: application/json');

if (isset($_GET['codeStation'])) {
    $codeStation = intval($_GET['codeStation']);
}

if (isset($_GET['idConso'])) {
    $idConso = intval($_GET['idConso']);
}

if (isset($_GET['lat'])) {
    $latitude = floatval($_GET['lat']);
} else {
    $latitude = null;
}

if (isset($_GET['long'])) {
    $longitude = floatval($_GET['long']);
} else {
    $longitude = null;
}

switch ($_GET['action']) {
    case 'getBikeInstantane':
        echo json_encode(getBikeInstantane($codeStation));
        exit();
        break;
    case 'getBikeResumeTroisHeures':
        echo json_encode(getBikeResume($codeStation, "-3hours", 5));
        exit();
        break;
    case 'getBikeResumeUnJour':
        echo json_encode(getBikeResume($codeStation, "-1day", 15));
        exit();
        break;
    case 'getBikeResumeSeptJours':
        echo json_encode(getBikeResume($codeStation, "-7days", 60));
        exit();
        break;
    case 'getBikeResumeUnMois':
        echo json_encode(getBikeResume($codeStation, "-1month", 360));
        exit();
        break;
    case 'getBikeResumeUnAn':
        echo json_encode(getBikeResume($codeStation, "-1year", 1440));
        exit();
        break;
    case 'getFreeDockInstantane':
        echo json_encode(getFreeDockInstantane($codeStation));
        exit();
        break;
    case 'getFreeDockResumeTroisHeures':
        echo json_encode(getFreeDockResume($codeStation, "-3hours", 5));
        exit();
        break;
    case 'getFreeDockResumeUnJour':
        echo json_encode(getFreeDockResume($codeStation, "-1day", 15));
        exit();
        break;
    case 'getFreeDockResumeSeptJours':
        echo json_encode(getFreeDockResume($codeStation, "-7days", 60));
        exit();
        break;
    case 'getFreeDockResumeUnMois':
        echo json_encode(getFreeDockResume($codeStation, "-1month", 360));
        exit();
        break;
    case 'getFreeDockResumeUnAn':
        echo json_encode(getFreeDockResume($codeStation, "-1year", 1440));
        exit();
        break;
    case 'vote':
        echo json_encode(compterVote($codeStation, $_GET['statut']));
        exit();
        break;
    case 'getConsoInstantane':
        echo json_encode(getConsoInstantane());
        exit();
        break;
    case 'getConsoResumeTroisHeures':
        echo json_encode(getConsoResume("-3hours", 5));
        exit();
        break;
    case 'getConsoResumeUnJour':
        echo json_encode(getConsoResume("-1day", 15));
        exit();
        break;
    case 'getConsoResumeSeptJours':
        echo json_encode(getConsoResume("-7days", 60));
        exit();
        break;
    case 'getConsoResumeUnMois':
        echo json_encode(getConsoResume("-1month", 360));
        exit();
        break;
    case 'getConsoResumeUnAn':
        echo json_encode(getConsoResume("-1year", 1440));
        exit();
        break;
    case 'getConsoBikeInstantane':
        echo json_encode(getConsoBikeInstantane());
        exit();
        break;
    case 'getConsoBikeResumeTroisHeures':
        echo json_encode(getConsoBikeResume("-3hours", 5));
        exit();
        break;
    case 'getConsoBikeResumeUnJour':
        echo json_encode(getConsoBikeResume("-1day", 15));
        exit();
        break;
    case 'getConsoBikeResumeSeptJours':
        echo json_encode(getConsoBikeResume("-7days", 60));
        exit();
        break;
    case 'getConsoBikeResumeUnMois':
        echo json_encode(getConsoBikeResume("-1month", 360));
        exit();
        break;
    case 'getConsoBikeResumeUnAn':
        echo json_encode(getConsoBikeResume("-1year", 1440));
        exit();
        break;
    case 'getDataConso':
        echo json_encode(getDataConso($idConso, $longitude, $latitude));
        exit();
        break;
    case 'getDataStation':
        echo json_encode(getDataStation($codeStation));
        exit();
        break;
    case 'getCommunesCarte':
        echo json_encode(getCommunesCarte($idConso));
        exit();
        break;
    case 'getSignalementCarte':
        echo json_encode(getSignalementCarte($idConso));
        exit();
        break;
    case 'getEtatCarte':
        echo json_encode(getEtatCarte());
        exit();
        break;
}

function getBikeInstantane($codeStation)
{
    $data = getDataBikeInstantane($codeStation);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => true),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataBikeInstantane($codeStation)
{
    global $pdo, $service;

    //Filtre 1 heure
    $hier = new DateTime("-1hour");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $proprietes = ['c.date', 's.nbBike', 's.nbEDock'];

    if ($service->config->eBike) {
        $proprietes[] = 's.nbEBike';
    }

    if ($service->config->overflow) {
        $proprietes[] = 's.nbBikeOverflow';
        if ($service->config->eBike) {
            $proprietes[] = 's.nbEBikeOverflow';
        }
    }

    $requete = $pdo->query('SELECT ' . implode(', ', $proprietes) . '
    FROM status s
    INNER JOIN statusConso c ON c.id = s.idConso
    WHERE s.service = ' . $service->id . ' AND s.code = ' . $codeStation . ' AND c.date >= "' . $filtreDate . '"
    ORDER BY c.date ASC');
    $statusStation = $requete->fetchAll();

    $datas = extractDataBikeGraph($service, $statusStation);

    return generateBikeGraphResponse($service, $datas);
}

function getBikeResume($codeStation, $filtreDate, $periode)
{
    $data = getDataBikeResume($codeStation, $filtreDate, $periode);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => false),
            ),
        ),
    );
    return array(
        'type' => 'bar',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataBikeResume($codeStation, $filtre, $periode)
{
    global $pdo, $service;

    //Filtre date
    $date = new DateTime($filtre);
    $filtreDate = $date->format('Y-m-d H:i:s');

    $proprietes = ['date', 'nbBikeMin', 'nbBikeMax', 'nbBikeMoyenne', 'nbBikePris', 'nbBikeRendu'];

    if ($service->config->eBike) {
        $proprietes[] = 'nbEBikeMin';
        $proprietes[] = 'nbEBikeMax';
        $proprietes[] = 'nbEBikeMoyenne';
        $proprietes[] = 'nbEBikePris';
        $proprietes[] = 'nbEBikeRendu';
    }

    if ($service->config->overflow) {
        $proprietes[] = 'nbBikeOverflowMin';
        $proprietes[] = 'nbBikeOverflowMax';
        $proprietes[] = 'nbBikeOverflowMoyenne';
        if ($service->config->eBike) {
            $proprietes[] = 'nbEbikeOverflowMin';
            $proprietes[] = 'nbEbikeOverflowMax';
            $proprietes[] = 'nbEbikeOverflowMoyenne';
        }
    }

    $requete = $pdo->query('SELECT ' . implode(', ', $proprietes) . '
    FROM resumeStatus
    WHERE service = ' . $service->id . ' AND code = ' . $codeStation . ' AND `date` >= "' . $filtreDate . '" AND duree = ' . $periode . '
    ORDER BY date ASC');
    $resumeStatusStation = $requete->fetchAll();

    $datesResume = [];
    $nbBikeMinData = [];
    $nbBikeMaxData = [];
    $nbBikeMoyenneData = [];
    $nbBikePrisData = [];
    $nbBikeRenduData = [];
    $nbEBikeMinData = [];
    $nbEBikeMaxData = [];
    $nbEBikeMoyenneData = [];
    $nbEBikePrisData = [];
    $nbEBikeRenduData = [];
    $nbOverflowMinData = [];
    $nbOverflowMaxData = [];
    $nbOverflowMoyenneData = [];
    foreach ($resumeStatusStation as $statut) {
        $datesResume[] = (new DateTime($statut['date']))->format("d/m H\hi");
        $nbBikeMinData[] = $statut['nbBikeMin'];
        $nbBikeMaxData[] = $statut['nbBikeMax'];
        $nbBikeMoyenneData[] = $statut['nbBikeMoyenne'];
        $nbBikePrisData[] = -1 * $statut['nbBikePris'];
        $nbBikeRenduData[] = $statut['nbBikeRendu'];
        if ($service->config->eBike) {
            $nbEBikeMinData[] = $statut['nbEBikeMin'];
            $nbEBikeMaxData[] = $statut['nbEBikeMax'];
            $nbEBikeMoyenneData[] = $statut['nbEBikeMoyenne'];
            $nbEBikePrisData[] = -1 * $statut['nbEBikePris'];
            $nbEBikeRenduData[] = $statut['nbEBikeRendu'];
        }
        if ($service->config->overflow) {
            $nbOverflowMinData[] = $statut['nbBikeOverflowMin'] + $statut['nbEbikeOverflowMin'];
            $nbOverflowMaxData[] = $statut['nbBikeOverflowMax'] + $statut['nbEbikeOverflowMax'];
            $nbOverflowMoyenneData[] = $statut['nbBikeOverflowMoyenne'] + $statut['nbEbikeOverflowMoyenne'];
        }
    }

    $datasets = array(
        array(
            'type' => 'line',
            'label' => 'Vélos mécaniques (Moyenne)',
            'borderColor' => 'rgba(104,221,46,0.7)',
            'fill' => false,
            'data' => $nbBikeMoyenneData,
        ),
        array(
            'type' => 'line',
            'label' => 'Vélos mécaniques (Min)',
            'borderColor' => 'rgba(104,221,46,0)',
            'backgroundColor' => 'rgba(104,221,46,0.3)',
            'fill' => "+1",
            'borderDash' => [5, 5],
            'data' => $nbBikeMinData,
        ),
        array(
            'type' => 'line',
            'label' => 'Vélos mécaniques (Max)',
            'borderColor' => 'rgba(104,221,46,0)',
            'backgroundColor' => 'rgba(104,221,46,0.3)',
            'fill' => false,
            'borderDash' => [5, 5],
            'data' => $nbBikeMaxData,
        ),
        array(
            'label' => 'Vélos mécaniques (Pris)',
            'backgroundColor' => 'rgba(104,221,46,0.5)',
            'data' => $nbBikePrisData,
        ),
        array(
            'label' => 'Vélos mécaniques (Rendu)',
            'backgroundColor' => 'rgba(104,221,46,0.5)',
            'data' => $nbBikeRenduData,
        ));

    if ($service->config->eBike) {
        $datasets[] =
        array(
            'type' => 'line',
            'label' => 'Vélos électriques (Moyenne)',
            'borderColor' => 'rgba(76, 213, 233, 0.7)',
            'fill' => false,
            'data' => $nbEBikeMoyenneData,
        );

        $datasets[] =
        array(
            'type' => 'line',
            'label' => 'Vélos électriques (Min)',
            'borderColor' => 'rgba(76, 213, 233, 0)',
            'backgroundColor' => 'rgba(76, 213, 233, 0.3)',
            'fill' => "+1",
            'borderDash' => [5, 5],
            'data' => $nbEBikeMinData,
        );

        $datasets[] =
        array(
            'type' => 'line',
            'label' => 'Vélos électriques (Max)',
            'borderColor' => 'rgba(76, 213, 233, 0)',
            'backgroundColor' => 'rgba(76, 213, 233, 0.3)',
            'fill' => false,
            'borderDash' => [5, 5],
            'data' => $nbEBikeMaxData,
        );

        $datasets[] =
        array(
            'label' => 'Vélos électriques (Pris)',
            'backgroundColor' => 'rgba(76, 213, 233, 0.5)',
            'data' => $nbEBikePrisData,
        );

        $datasets[] =
        array(
            'label' => 'Vélos électriques (Rendu)',
            'backgroundColor' => 'rgba(76, 213, 233, 0.5)',
            'data' => $nbEBikeRenduData,
        );
    }

    if ($service->config->overflow) {
        $datasets[] =
        array(
            'type' => 'line',
            'label' => 'Vélos en overflow (Moyenne)',
            'borderColor' => 'rgba(254, 216, 160, 0.7)',
            'fill' => false,
            'data' => $nbOverflowMoyenneData,
        );

        $datasets[] =
        array(
            'type' => 'line',
            'label' => 'Vélos en overflow (Min)',
            'borderColor' => 'rgba(254, 216, 160, 0)',
            'backgroundColor' => 'rgba(254, 216, 160, 0.3)',
            'fill' => "+1",
            'borderDash' => [5, 5],
            'data' => $nbOverflowMinData,
        );

        $datasets[] =
        array(
            'type' => 'line',
            'label' => 'Vélos en overflow (Max)',
            'borderColor' => 'rgba(254, 216, 160, 0)',
            'backgroundColor' => 'rgba(254, 216, 160, 0.3)',
            'fill' => false,
            'borderDash' => [5, 5],
            'data' => $nbOverflowMaxData,
        );
    }

    return array(
        'labels' => $datesResume,
        'datasets' => $datasets,
    );
}

function getFreeDockInstantane($codeStation)
{
    $data = getDataFreeDockInstantane($codeStation);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => true),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataFreeDockInstantane($codeStation)
{
    global $pdo, $service;

    //Filtre 1 heure
    $hier = new DateTime("-1hour");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT c.date, s.nbFreeEDock, s.nbEDock
    FROM status s
    INNER JOIN statusConso c ON c.id = s.idConso
    WHERE s.service = ' . $service->id . ' AND s.code = ' . $codeStation . ' AND c.date >= "' . $filtreDate . '"
    ORDER BY c.date ASC');
    $statusStation = $requete->fetchAll();

    $dates = [];
    $nbFreeEdockData = [];
    foreach ($statusStation as $statut) {
        $dates[] = (new DateTime($statut['date']))->format("d/m H\hi");
        $nbFreeEdockData[] = $statut['nbFreeEDock'];
    }

    return array(
        'labels' => $dates,
        'datasets' => array(
            array(
                'label' => 'Nombre de bornes libres',
                'backgroundColor' => 'rgba(173,0,130,0.5)',
                'data' => $nbFreeEdockData,
            ),
        ),
    );
}

function getFreeDockResume($codeStation, $filtreDate, $periode)
{
    $data = getDataFreeDockResume($codeStation, $filtreDate, $periode);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => false),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataFreeDockResume($codeStation, $filtre, $periode)
{
    global $pdo, $service;

    //Filtre date
    $date = new DateTime($filtre);
    $filtreDate = $date->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT `date`, nbFreeEDockMin, nbFreeEDockMax, nbFreeEDockMoyenne
    FROM resumeStatus
    WHERE service = ' . $service->id . ' AND code = ' . $codeStation . ' AND `date` >= "' . $filtreDate . '" AND duree = ' . $periode . '
    ORDER BY date ASC');
    $resumeStatusStation = $requete->fetchAll();

    $datesResume = [];
    $nbFreeEDockMinData = [];
    $nbFreeEDockMaxData = [];
    $nbFreeEDockMoyenneData = [];
    foreach ($resumeStatusStation as $statut) {
        $datesResume[] = (new DateTime($statut['date']))->format("d/m H\hi");
        $nbFreeEDockMinData[] = $statut['nbFreeEDockMin'];
        $nbFreeEDockMaxData[] = $statut['nbFreeEDockMax'];
        $nbFreeEDockMoyenneData[] = $statut['nbFreeEDockMoyenne'];
    }

    return array(
        'labels' => $datesResume,
        'datasets' => array(
            array(
                'label' => 'Nombre de bornes libres (Moyenne)',
                'borderColor' => 'rgba(173,0,130,0.7)',
                'fill' => false,
                'data' => $nbFreeEDockMoyenneData,
            ),
            array(
                'label' => 'Nombre de bornes libres (Min)',
                'borderColor' => 'rgba(173,0,130,0)',
                'backgroundColor' => 'rgba(173,0,130,0.3)',
                'fill' => "+1",
                'borderDash' => [5, 5],
                'data' => $nbFreeEDockMinData,
            ),
            array(
                'label' => 'Nombre de bornes libres (Max)',
                'borderColor' => 'rgba(173,0,130,0)',
                'backgroundColor' => 'rgba(173,0,130,0.3)',
                'fill' => false,
                'borderDash' => [5, 5],
                'data' => $nbFreeEDockMaxData,
            ),
        ),
    );
}

function compterVote($codeStation, $statut)
{
    global $pdo;

    $vote = null;
    if ($statut == 'oui') {
        $vote = 1;
    } elseif ($statut == 'non') {
        $vote = 0;
    } else {
        return false;
    }

    $pdo->exec('INSERT INTO signalement (code, estFonctionnel) VALUES (' . $codeStation . ', ' . $vote . ')');
    return true;
}

function getConsoInstantane()
{
    $data = getDataConsoInstantane();

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => true),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataConsoInstantane()
{
    global $pdo, $service;

    //Filtre 1 heure
    $hier = new DateTime("-1hour");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT * FROM `statusConso` Where service = ' . $service->id . ' AND date >= "' . $filtreDate . '" Order by id asc');
    $allConso = $requete->fetchAll();
    $dates = [];
    $nbStationsData = [];
    $nbStationsDetecteData = [];
    foreach ($allConso as $i => $c) {
        if ($c['nbStation'] > 0) {
            $dates[] = (new DateTime($c['date']))->format("d/m H\hi");
            $nbStationsData[] = $c['nbStation'];
            $nbStationsDetecteData[] = $c['nbStationDetecte'];
        }
    }

    return array(
        'labels' => $dates,
        'datasets' => array(
            array(
                'label' => 'Stations annoncées',
                'backgroundColor' => 'rgba(173,0,130,0.5)',
                'data' => $nbStationsData,
            ),
            array(
                'label' => 'Stations détectées',
                'backgroundColor' => 'rgba(208,74,5,0.5)',
                'data' => $nbStationsDetecteData,
            ),
        ),
    );
}

function getConsoBikeInstantane()
{
    $data = getDataConsoBikeInstantane();

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => true),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataConsoBikeInstantane()
{
    global $pdo, $service;

    //Filtre 1 heure
    $hier = new DateTime("-1hour");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT * FROM `statusConso` Where service = ' . $service->id . ' AND date >= "' . $filtreDate . '" Order by id asc');
    $allConso = $requete->fetchAll();

    $datas = extractDataBikeGraph($service, $allConso);

    return generateBikeGraphResponse($service, $datas);
}

function extractDataBikeGraph($service, $datas, $suffixe = '')
{
    $dates = [];
    $nbBikeData = [];
    $nbEbikeData = [];
    $nbOverflowData = [];
    foreach ($datas as $i => $c) {
        if (!isset($c['nbStation']) || $c['nbStation'] > 0) {
            $dates[] = (new DateTime($c['date']))->format("d/m H\hi");
            $nbBikeData[] = $c['nbBike' . $suffixe];
            if ($service->config->eBike) {
                $nbEbikeData[] = $c['nbEbike' . $suffixe];
            }

            if ($service->config->overflow) {
                $nbOverflow = $c['nbBikeOverflow' . $suffixe];
                if ($service->config->eBike) {
                    $nbOverflow += $c['nbEBikeOverflow' . $suffixe];
                }

                $nbOverflowData[] = $nbOverflow;
            }
        }
    }

    $dataRetour = array('bike' => $nbBikeData);

    if ($service->config->eBike) {
        $dataRetour['eBike'] = $nbEbikeData;
    }

    if ($service->config->overflow) {
        $dataRetour['overflow'] = $nbOverflowData;
    }

    return array(
        'dates' => $dates,
        'datas' => $dataRetour,
    );
}

function generateBikeGraphResponse($service, $datas)
{
    $datasets = array(
        array(
            'label' => 'Vélos mécaniques',
            'backgroundColor' => 'rgba(104,221,46,0.5)',
            'data' => $datas['datas']['bike'],
        ),
    );

    if ($service->config->eBike) {
        $datasets[] = array(
            'label' => 'Vélos électriques',
            'backgroundColor' => 'rgba(76, 213, 233, 0.5)',
            'data' => $datas['datas']['eBike'],
        );
    }

    if ($service->config->overflow) {
        $datasets[] = array(
            'label' => 'Vélos en overflow',
            'backgroundColor' => 'rgba(254, 216, 160, 0.5)',
            'data' => $datas['datas']['overflow'],
        );
    }

    return array(
        'labels' => $datas['dates'],
        'datasets' => $datasets,
    );
}

function getConsoResume($filtreDate, $periode)
{
    $data = getDataConsoResume($filtreDate, $periode);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => false),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataConsoResume($filtre, $periode)
{
    global $pdo, $service;

    //Filtre date
    $date = new DateTime($filtre);
    $filtreDate = $date->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT *
    FROM resumeConso
    WHERE service = ' . $service->id . ' AND `date` >= "' . $filtreDate . '" AND duree = ' . $periode . '
    ORDER BY date ASC');
    $resumeStatusStation = $requete->fetchAll();

    $datesResume = [];
    $nbStationsData = [];
    $nbStationsDetecteData = [];
    foreach ($resumeStatusStation as $statut) {
        $datesResume[] = (new DateTime($statut['date']))->format("d/m H\hi");
        $nbStationsData[] = $statut['nbStation'];
        $nbStationsDetecteData[] = $statut['nbStationDetecte'];
    }

    return array(
        'labels' => $datesResume,
        'datasets' => array(
            array(
                'label' => 'Stations annoncées',
                'backgroundColor' => 'rgba(173,0,130,0.5)',
                'data' => $nbStationsData,
            ),
            array(
                'label' => 'Stations détectées',
                'backgroundColor' => 'rgba(208,74,5,0.5)',
                'data' => $nbStationsDetecteData,
            ),
        ),
    );
}

function getConsoBikeResume($filtreDate, $periode)
{
    $data = getDataConsoBikeResume($filtreDate, $periode);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => false),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataConsoBikeResume($filtre, $periode)
{
    global $pdo, $service;

    //Filtre date
    $date = new DateTime($filtre);
    $filtreDate = $date->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT *
    FROM resumeConso
    WHERE service = ' . $service->id . ' AND `date` >= "' . $filtreDate . '" AND duree = ' . $periode . '
    ORDER BY date ASC');
    $resumeStatusStation = $requete->fetchAll();

    $datas = extractDataBikeGraph($service, $resumeStatusStation, 'Moyenne');

    return generateBikeGraphResponse($service, $datas);
}

function getDataConso($idConso, $longitude = null, $latitude = null)
{
    global $pdo;
    $whereCoord = null;
    if (!is_null($longitude) && !is_null($latitude)) {
        $whereCoord = implode(' AND ', array(
            'stations.longitude >= ' . ($longitude - 0.015),
            'stations.longitude <= ' . ($longitude + 0.015),
            'stations.latitude >= ' . ($latitude - 0.01),
            'stations.latitude <= ' . ($latitude + 0.01),
        ));
    }

    return array('data' => getStatusByIdConso($idConso, $whereCoord));
}

function getDataStation($codeStation)
{
    global $pdo, $service;
    //Filtre 24 heures
    $hier = new DateTime("-1day");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $proprietes = ['c.id', 'c.date', 's.nbBike', 's.nbFreeEDock', 's.nbEDock'];

    if ($service->config->eBike) {
        $proprietes[] = 's.nbEBike';
    }

    if ($service->config->overflow) {
        $proprietes[] = 's.nbBikeOverflow';
        if ($service->config->eBike) {
            $proprietes[] = 's.nbEBikeOverflow';
        }
    }

    //Stations
    $requete = $pdo->query('SELECT ' . implode(', ', $proprietes) . '
    FROM status s
    INNER JOIN statusConso c ON c.id = s.idConso
    WHERE s.service = ' . $service->id . ' AND s.code = ' . $codeStation . ' AND c.date >= "' . $filtreDate . '"
    ORDER BY c.id ASC');
    $statusStation = $requete->fetchAll(PDO::FETCH_ASSOC);
    $retour = [];
    foreach ($statusStation as $statut) {
        $data = array(
            'idConso' => $statut['id'],
            'date' => $statut['date'],
            'nbBike' => $statut['nbBike'],
            'nbFreeEDock' => $statut['nbFreeEDock'],
            'nbEDock' => $statut['nbEDock'],
        );

        if ($service->config->eBike) {
            $data['nbEbike'] = $statut['nbEBike'];
        }

        if ($service->config->overflow) {
            $data['nbBikeOverflow'] = $statut['nbBikeOverflow'];
            if ($service->config->eBike) {
                $data['nbEbikeOverflow'] = $statut['nbEBikeOverflow'];
            }
        }

        $retour[] = $data;
    }
    return array('data' => $retour);
}

function getCommunesCarte($idConso)
{
    global $pdo;

    $dataConso = getDataConso($idConso);
    $statusStation = $dataConso['data'];

    $dataSvg = genererObjetsCarteSVG($statusStation);

    $svg = genererCarteSVG(800, 500, $dataSvg['communes'], 'commune.php?insee=', '', '', $dataSvg['obj'], COMMUNE_AVM_VELIB);

    return $svg;
}

function getDataSignalement()
{
    global $pdo;

    $hier = new DateTime("-1week");
    $filtreSemaine = $hier->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT *
    FROM signalement
    WHERE dateSignalement >= "' . $filtreSemaine . '"
    ORDER BY code ASC, dateSignalement ASC');
    $signalements = $requete->fetchAll();
    $resumeSignalement = array();
    foreach ($signalements as $sign) {
        $code = $sign['code'];
        if (!isset($resumeSignalement[$code])) {
            $resumeSignalement[$code] = array(true => 0, false => 0);
        }

        $resumeSignalement[$code][$sign['estFonctionnel'] == 1]++;
        $resumeSignalement[$code]['dernier'] = $sign['estFonctionnel'] == 1;
    }

    //On compile
    $etatStations = array();
    foreach ($resumeSignalement as $code => $sign) {
        if ($sign[true] > $sign[false]) {
            $etatStations[$code] = true;
        } else if ($sign[true] < $sign[false]) {
            $etatStations[$code] = false;
        } else {
            $etatStations[$code] = $sign['dernier'];
        }

    }

    return $etatStations;
}

function getSignalementCarte($idConso)
{
    global $pdo;

    $dataConso = getDataConso($idConso);
    $statusStation = $dataConso['data'];

    $signalements = getDataSignalement();

    $liste_communes = array();
    $objets = array();

    define('ETAT_INCONNU', 0);
    define('ETAT_FONCTIONNE', 1);
    define('ETAT_NON_FONCTIONNE', 2);

    $couleur_etat = array(
        ETAT_INCONNU => 'lightgray',
        ETAT_FONCTIONNE => 'ForestGreen',
        ETAT_NON_FONCTIONNE => 'FireBrick',
    );

    $nombre_etat = array(
        ETAT_INCONNU => 0,
        ETAT_FONCTIONNE => 0,
        ETAT_NON_FONCTIONNE => 0,
    );

    foreach ($statusStation as $station) {
        $code = $station['code'];
        if (isset($signalements[$code])) {
            if ($signalements[$code]) {
                $etat = ETAT_FONCTIONNE;
                $s_info1 = 'Signalé comme fonctionnant';
            } else {
                $etat = ETAT_NON_FONCTIONNE;
                $s_info1 = 'Signalé comme ne fonctionnant pas';
            }
        } else {
            $etat = ETAT_INCONNU;
            $s_info1 = 'Aucun signalement récemment';
        }

        //forme en fonction de l'état annoncé
        if ($etat == ETAT_INCONNU) {
            $s_point = 'rond';
            $s_taille = 6;
            $s_angle = 0;
        } else {
            $s_point = 'carre';
            $s_taille = 6;
            $s_angle = 45;
        }

        //comptage du nombre de stations par etat
        $nombre_etat[$etat]++;

        //récupération de la commune correspondant à la station (d'après son numéro)
        $inseeCommune = getCommuneStation($station['code']);

        //ajout à la liste des communes
        if (!is_null($inseeCommune) && !in_array($inseeCommune, $liste_communes)) {
            $liste_communes[] = $inseeCommune;
        }

        //texte en fonction du nombre de bornes
        switch ($station['nbEDock']) {
            case 0:$s_info2 = 'aucune borne';
                break;
            case 1:$s_info2 = '1 borne';
                break;
            default:$s_info2 = $station['nbEDock'] . ' bornes';
                break;
        }

        $objets[] = array(
            'nature' => 'point',
            'point' => $s_point,
            'taille' => $s_taille,
            'angle' => $s_angle,
            'couleur' => $couleur_etat[$etat],
            'lon' => $station['longitude'],
            'lat' => $station['latitude'],
            'lien' => 'station.php?code=' . $code,
            'info' => 'Station ' . displayCodeStation($code) . "\n" . $station['name'] . "\n" . $s_info1 . ' - ' . $s_info2,
        );

    }

    //légende (point et texte, affiché uniquement pour les états ayant au moins une station
    if ($nombre_etat[ETAT_INCONNU]) {
        $objets[] = array('point' => 'rond', 'x' => -58, 'y' => 10, 'taille' => 6, 'angle' => 0,
            'couleur' => $couleur_etat[ETAT_INCONNU]);
        $objets[] = array('nature' => 'texte', 'x' => -60, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Pas de signalement (' . $nombre_etat[ETAT_INCONNU] . ')');
    }
    if ($nombre_etat[ETAT_NON_FONCTIONNE]) {
        $objets[] = array('point' => 'carre', 'x' => -18, 'y' => 210, 'taille' => 6, 'angle' => 45,
            'couleur' => $couleur_etat[ETAT_NON_FONCTIONNE]);
        $objets[] = array('nature' => 'texte', 'x' => -20, 'y' => 220, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Ne fonctionne pas (' . $nombre_etat[ETAT_NON_FONCTIONNE] . ')');
    }
    if ($nombre_etat[ETAT_FONCTIONNE]) {
        $objets[] = array('point' => 'carre', 'x' => -18, 'y' => 10, 'taille' => 6, 'angle' => 45,
            'couleur' => $couleur_etat[ETAT_FONCTIONNE]);
        $objets[] = array('nature' => 'texte', 'x' => -20, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Fonctionne (' . $nombre_etat[ETAT_FONCTIONNE] . ')');
    }

    $svg = genererCarteSVG(800, 500, $liste_communes, 'commune.php?insee=', '', '', $objets, COMMUNE_AVM_VELIB);

    return $svg;
}

function getDataEtat()
{
    global $pdo;

    $repere = new DateTime("-6hours");

    $requete = $pdo->query('SELECT * FROM stations');
    $stationsData = $requete->fetchAll();
    $stations = array();
    foreach ($stationsData as $station) {
        $lastRefresh = $station['lastRefresh'];
        $objStation = array('code' => $station['code'], 'name' => $station['name'], 'latitude' => $station['latitude'], 'longitude' => $station['longitude'], 'insee' => $station['insee']);
        if (is_null($lastRefresh)) {
            $objStation['etat'] = null;
        } else {
            if ((new DateTime($lastRefresh)) < $repere) //Pas de mise à jour depuis 6h
            {
                $objStation['etat'] = false;
            } else {
                $objStation['etat'] = true;
            }

        }
        $stations[] = $objStation;
    }

    return $stations;
}

function getEtatCarte()
{
    global $pdo;

    $stations = getDataEtat();

    $liste_communes = array();
    $objets = array();

    define('ETAT_INCONNU', 0);
    define('ETAT_FONCTIONNE', 1);
    define('ETAT_NON_FONCTIONNE', 2);

    $couleur_etat = array(
        ETAT_INCONNU => 'lightgray',
        ETAT_FONCTIONNE => 'ForestGreen',
        ETAT_NON_FONCTIONNE => 'FireBrick',
    );

    $nombre_etat = array(
        ETAT_INCONNU => 0,
        ETAT_FONCTIONNE => 0,
        ETAT_NON_FONCTIONNE => 0,
    );

    foreach ($stations as $station) {
        $code = $station['code'];
        if (is_null($station['etat'])) {
            $etat = ETAT_INCONNU;
            $s_info1 = 'Pas d\'information sur l\'état des données';
        } elseif ($station['etat']) {
            $etat = ETAT_FONCTIONNE;
            $s_info1 = 'Données actualisés récemment';
        } else {
            $etat = ETAT_NON_FONCTIONNE;
            $s_info1 = 'Données non reçus depuis un moment';
        }

        //forme en fonction de l'état annoncé
        if ($etat == ETAT_INCONNU) {
            $s_point = 'rond';
            $s_taille = 6;
            $s_angle = 0;
        } else {
            $s_point = 'carre';
            $s_taille = 6;
            $s_angle = 45;
        }

        //comptage du nombre de stations par etat
        $nombre_etat[$etat]++;

        //récupération de la commune correspondant à la station (d'après son numéro)
        $inseeCommune = $station['insee'];

        //ajout à la liste des communes
        if (!is_null($inseeCommune) && !in_array($inseeCommune, $liste_communes)) {
            $liste_communes[] = $inseeCommune;
        }

        $objets[] = array(
            'nature' => 'point',
            'point' => $s_point,
            'taille' => $s_taille,
            'angle' => $s_angle,
            'couleur' => $couleur_etat[$etat],
            'lon' => $station['longitude'],
            'lat' => $station['latitude'],
            'lien' => 'station.php?code=' . $code,
            'info' => 'Station ' . displayCodeStation($code) . "\n" . $station['name'] . "\n" . $s_info1,
        );

    }

    //légende (point et texte, affiché uniquement pour les états ayant au moins une station
    if ($nombre_etat[ETAT_INCONNU]) {
        $objets[] = array('point' => 'rond', 'x' => -58, 'y' => 10, 'taille' => 6, 'angle' => 0,
            'couleur' => $couleur_etat[ETAT_INCONNU]);
        $objets[] = array('nature' => 'texte', 'x' => -60, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'État des données inconnu (' . $nombre_etat[ETAT_INCONNU] . ')');
    }
    if ($nombre_etat[ETAT_NON_FONCTIONNE]) {
        $objets[] = array('point' => 'carre', 'x' => -18, 'y' => 210, 'taille' => 6, 'angle' => 45,
            'couleur' => $couleur_etat[ETAT_NON_FONCTIONNE]);
        $objets[] = array('nature' => 'texte', 'x' => -20, 'y' => 220, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Données vieilles (' . $nombre_etat[ETAT_NON_FONCTIONNE] . ')');
    }
    if ($nombre_etat[ETAT_FONCTIONNE]) {
        $objets[] = array('point' => 'carre', 'x' => -18, 'y' => 10, 'taille' => 6, 'angle' => 45,
            'couleur' => $couleur_etat[ETAT_FONCTIONNE]);
        $objets[] = array('nature' => 'texte', 'x' => -20, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Données fraîches (' . $nombre_etat[ETAT_FONCTIONNE] . ')');
    }

    $svg = genererCarteSVG(800, 500, $liste_communes, 'commune.php?insee=', '', '', $objets, COMMUNE_AVM_VELIB);

    return $svg;
}
