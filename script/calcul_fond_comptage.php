<?php
require_once '../config.php';
include_once '../functions.php';

$requete = $pdo->query('SELECT MAX(nbStation) as max_nbStation FROM `comptage_statusConso` WHERE date >= DATE_SUB(CURDATE(), INTERVAL 2 YEAR)');
$donneesMax = $requete->fetch();
$maxStation = $donneesMax['max_nbStation'];

$cleJours = ['job', 'sam', 'djf'];

$pdo->beginTransaction();
$type = 1;
$pdo->exec('DELETE FROM fond WHERE type = ' . $type);
for ($heure = 0; $heure <= 23; $heure++) {
    $requete = $pdo->query('SELECT *, round(nombre + ((' . $maxStation . ' - nbStation) * (nombre / nbStation))) As valeurCorrige
    FROM `comptage_statusConso`
    WHERE HOUR(date) = ' . $heure . ' AND date >= DATE_SUB(CURDATE(), INTERVAL 2 YEAR)
    ORDER BY `valeurCorrige` DESC');
    $donnees = $requete->fetchAll();

    $valeur = array(
        'job' => [],
        'sam' => [],
        'djf' => [],
    );
    $record = array(
        'job' => 0,
        'sam' => 0,
        'djf' => 0,
    );
    foreach ($donnees as $donnee) {
        $cle = getTypeJour($donnee['date']);
        $valeur[$cle][] = $donnee['valeurCorrige'];
        if ($donnee['nombre'] > $record[$cle]) {
            $record[$cle] = $donnee['nombre'];
        }
    }
    foreach ($cleJours as $cle) {
        $valeursJour = $valeur[$cle];
        $nbValeurs = count($valeursJour);

        //echo $heure . 'h - ' . $cle . ' - 20% : ' . $valeursJour[floor(0.8 * ($nbValeurs - 1))] . ' - 80% : ' . $valeursJour[floor(0.2 * ($nbValeurs - 1))] . ' - 90% : ' . $valeursJour[floor(0.1 * ($nbValeurs - 1))] . ' - 100% : ' . $valeursJour[0] . "<br />\n";

        $centile20 = $valeursJour[floor(0.85 * ($nbValeurs - 1))];
        $centile50 = $valeursJour[floor(0.5 * ($nbValeurs - 1))];
        $centile80 = $valeursJour[floor(0.20 * ($nbValeurs - 1))];
        $centile90 = $valeursJour[floor(0.08 * ($nbValeurs - 1))];
        $centile95 = $valeursJour[floor(0.05 * ($nbValeurs - 1))];
        $centile100 = $record[$cle];

        $pdo->exec('INSERT INTO `fond` (`type`, `periode`, heure, `centile20`, `centile50`, `centile80`, `centile90`, `centile95`, `centile100`) VALUES (' . $type . ', "' . $cle . '", "' . $heure . ':00:00", ' . $centile20 . ', ' . $centile50 . ', ' . $centile80 . ', ' . $centile90 . ', ' . $centile95 . ', ' . $centile100 . ')');
    }
}

$pdo->commit();
