from resumeLib_comptage import calculerToutResume
from comptageLib import nettoyerResume
import pymysql
import datetime
from config import getMysqlConnection

# On commence par avoir la date min et max
mysql = getMysqlConnection()
requete = mysql.cursor()
requete.execute("SELECT MIN(date), MAX(date) FROM `comptage_statusConso`")
bornesDate = requete.fetchone()
dateMin = bornesDate[0].replace(microsecond=0, second=0)
dateMax = bornesDate[1].replace(microsecond=0, second=0)
dateMax = dateMax + datetime.timedelta(days=1)

print("******* Bornes *******")
print("Début = "+str(dateMin))
print("Fin = "+str(dateMax))
print("******* Bornes *******")

# On nettoie
nettoyerResume(dateMin, dateMax)

# Périodes à calculer
calculerToutResume(dateMin, dateMax)
