<div id="credits">
    Ce site n'est pas un site officiel de vélib métropole. Les données utilisées proviennent de 
    <a href="https://www.velib-metropole.fr">www.velib-metropole.fr</a> et appartiennent à leur propriétaire. - 
    <a href="https://framagit.org/JonathanMM/velibstats">Site du projet et sources</a> - 
    Auteur : JonathanMM (<a href="https://twitter.com/Jonamaths">@Jonamaths</a>)
</div>