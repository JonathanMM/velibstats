<?php
require_once '../config.php';
include_once '../functions.php';

$cleJours = ['job', 'sam', 'djf'];

$pdo->beginTransaction();
$type = 2;
$pdo->exec('DELETE FROM fond WHERE type = ' . $type);
for ($heure = 0; $heure <= 23; $heure++) {
    for ($minute = 0; $minute < 60; $minute += 5) {
        $requete = $pdo->query('SELECT date, LEAST(SUM(nbBikePris + nbEBikePris), SUM(nbBikeRendu + nbEBikeRendu)) AS nbLocations
    FROM `resumeStatus`
    WHERE HOUR(date) = ' . $heure . ' AND MINUTE(date) = ' . $minute . ' AND duree = 5 AND date >= DATE_SUB(CURDATE(), INTERVAL 2 YEAR)
    GROUP BY date
    ORDER BY `nbLocations` DESC');
        $donnees = $requete->fetchAll();

        $valeur = array(
            'job' => [],
            'sam' => [],
            'djf' => [],
        );
        $record = array(
            'job' => 0,
            'sam' => 0,
            'djf' => 0,
        );
        foreach ($donnees as $donnee) {
            $cle = getTypeJour($donnee['date']);
            $valeur[$cle][] = $donnee['nbLocations'];
            if ($donnee['nbLocations'] > $record[$cle]) {
                $record[$cle] = $donnee['nbLocations'];
            }
        }
        foreach ($cleJours as $cle) {
            $valeursJour = $valeur[$cle];
            $nbValeurs = count($valeursJour);

            if ($nbValeurs == 0) {
                continue;
            }

            //echo $heure . 'h - ' . $cle . ' - 20% : ' . $valeursJour[floor(0.8 * ($nbValeurs - 1))] . ' - 80% : ' . $valeursJour[floor(0.2 * ($nbValeurs - 1))] . ' - 90% : ' . $valeursJour[floor(0.1 * ($nbValeurs - 1))] . ' - 100% : ' . $valeursJour[0] . "<br />\n";

            $centile20 = $valeursJour[floor(0.85 * ($nbValeurs - 1))];
            $centile50 = $valeursJour[floor(0.5 * ($nbValeurs - 1))];
            $centile80 = $valeursJour[floor(0.20 * ($nbValeurs - 1))];
            $centile90 = $valeursJour[floor(0.08 * ($nbValeurs - 1))];
            $centile95 = $valeursJour[floor(0.05 * ($nbValeurs - 1))];
            $centile100 = $record[$cle];

            $pdo->exec('INSERT INTO `fond` (`type`, `periode`, heure, `centile20`, `centile50`, `centile80`, `centile90`, `centile95`, `centile100`) VALUES (' . $type . ', "' . $cle . '", "' . $heure . ':' . $minute . ':00", ' . $centile20 . ', ' . $centile50 . ', ' . $centile80 . ', ' . $centile90 . ', ' . $centile95 . ', ' . $centile100 . ')');
        }
    }
}

$pdo->commit();
