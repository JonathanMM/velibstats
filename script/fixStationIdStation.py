import pymysql, urllib.request, json, os
from config import getMysqlConnection

mysql = getMysqlConnection()

#On récupère toutes les stations sans insee
requete = mysql.cursor()
requete.execute('SELECT code, latitude, longitude, name FROM stations WHERE stationId IS NULL')
stations = requete.fetchall()
tmpFileName = 'gbfsStation.json'
urllib.request.urlretrieve("https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_information.json", tmpFileName)
data = json.load(open(tmpFileName))

for station in stations:
    codeStation = station[0]
    latitude = float(station[1])
    longitude = float(station[2])
    for stat in data['data']['stations']:
        if abs(stat['lat'] - latitude) < 0.00002 and abs(stat['lon'] - longitude) < 0.00002:
            requete = mysql.cursor()
            requete.execute('UPDATE stations SET stationId = '+str(stat['station_id'])+' WHERE code = '+str(codeStation))

mysql.close()
os.remove(tmpFileName)