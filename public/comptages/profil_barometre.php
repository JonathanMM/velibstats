<?php
require_once 'config.php';
include_once 'functions.php';

function estJF($date)
{
    $jour = intval($date->format('d'));
    $mois = intval($date->format('m'));

    if (
        ($jour == 1 && $mois == 1) ||
        ($jour == 1 && $mois == 5) ||
        ($jour == 8 && $mois == 5) ||
        ($jour == 14 && $mois == 7) ||
        ($jour == 15 && $mois == 8) ||
        ($jour == 1 && $mois == 11) ||
        ($jour == 11 && $mois == 11) ||
        ($jour == 25 && $mois == 12)
    ) {
        return true;
    }

    $an = intval($date->format('Y'));
    $mars21 = new DateTimeImmutable($an . '-03-21', new DateTimeZone('Europe/Paris'));
    $paques = $mars21->add(new DateInterval('P' . easter_days($an) . 'D'));
    $lundiPaques = $paques->add(new DateInterval('P1D'));
    $ascemption = $paques->add(new DateInterval('P39D'));
    $pentecote = $paques->add(new DateInterval('P50D'));
    $lundiPentecote = $paques->add(new DateInterval('P51D'));
    if (
        ($jour == intval($paques->format('d')) && $mois == intval($paques->format('m'))) ||
        ($jour == intval($lundiPaques->format('d')) && $mois == intval($lundiPaques->format('m'))) ||
        ($jour == intval($ascemption->format('d')) && $mois == intval($ascemption->format('m'))) ||
        ($jour == intval($pentecote->format('d')) && $mois == intval($pentecote->format('m'))) ||
        false//($jour == intval($lundiPentecote->format('d')) && $mois == intval($lundiPentecote->format('m')))
    ) {
        return true;
    }

    return false;
}

function estVS($date)
{
    $jour = intval($date->format('d'));
    $mois = intval($date->format('m'));
    $an = intval($date->format('Y'));

    switch ($an) {
        case 2018:
            return (
                (($mois == 1 && $jour < 8)) ||
                (($mois == 2 && $jour >= 17) || ($mois == 3 && $jour < 5)) ||
                (($mois == 4 && ($jour >= 14 && $jour < 30))) ||
                (($mois == 7 && $jour >= 7) || ($mois == 8) || ($mois == 9 && $jour < 3)) ||
                (($mois == 10 && $jour >= 20) || ($mois == 11 && $jour < 5)) ||
                (($mois == 12 && $jour >= 22))
            );
        case 2019:
            return (
                (($mois == 1 && $jour < 7)) ||
                (($mois == 2 && $jour >= 23) || ($mois == 3 && $jour < 11)) ||
                (($mois == 4 && $jour >= 20) || ($mois == 5 && $jour < 6)) ||
                (($mois == 5 && $jour >= 29) || ($mois == 6 && $jour < 3)) ||
                (($mois == 7 && $jour >= 6) || ($mois == 8) || ($mois == 9 && $jour < 2)) ||
                (($mois == 10 && $jour >= 19) || ($mois == 11 && $jour < 4)) ||
                (($mois == 12 && $jour >= 21))
            );
        case 2020:
            return (
                (($mois == 1 && $jour < 6)) ||
                (($mois == 2 && ($jour >= 8 && $jour < 24))) ||
                (($mois == 4 && ($jour >= 4 && $jour < 20))) ||
                (($mois == 5 && ($jour >= 20 && $jour < 25))) ||
                (($mois == 7 && $jour >= 4) || ($mois == 8) || ($mois == 9 && $jour < 1)) ||
                (($mois == 10 && $jour >= 17) || ($mois == 11 && $jour < 2)) ||
                (($mois == 12 && $jour >= 19))
            );
    }

    return false;
}

$requete = $pdo->query('SELECT MAX(nbStation) as max_nbStation FROM `comptage_statusConso` WHERE date >= DATE_SUB(CURDATE(), INTERVAL 2 YEAR)');
$donneesMax = $requete->fetch();
$maxStation = $donneesMax['max_nbStation'];

$cleJours = ['job', 'sam', 'djf', 'jvs'];
$charts = array(
    'job' => [
        20 => [],
        50 => [],
        80 => [],
        90 => [],
        100 => [],
        101 => [],
    ],
    'sam' => [
        20 => [],
        50 => [],
        80 => [],
        90 => [],
        100 => [],
        101 => []],
    'djf' => [
        20 => [],
        50 => [],
        80 => [],
        90 => [],
        100 => [],
        101 => []],
    'jvs' => [
        20 => [],
        50 => [],
        80 => [],
        90 => [],
        100 => [],
        101 => []]
);

for ($heure = 0; $heure <= 23; $heure++) {
    $requete = $pdo->query('SELECT *, round(nombre + ((' . $maxStation . ' - nbStation) * (nombre / nbStation))) As valeurCorrige
    FROM `comptage_statusConso`
    WHERE HOUR(date) = ' . $heure . ' AND date >= DATE_SUB(CURDATE(), INTERVAL 2 YEAR)
    ORDER BY `valeurCorrige` DESC');
    $donnees = $requete->fetchAll();

    $valeur = array(
        'job' => [],
        'sam' => [],
        'djf' => [],
        'jvs' => [],
    );
    $record = array(
        'job' => 0,
        'sam' => 0,
        'djf' => 0,
        'jvs' => 0,
    );
    foreach ($donnees as $donnee) {
        $jour = new DateTime($donnee['date'], new \DateTimeZone('Europe/Paris'));
        $dayOfWeek = intval($jour->format('N'));
        $cle = 'job';
        if ($dayOfWeek == 7 || estJF($jour)) //Dimanche ou JF
        {
            $cle = 'djf';
        } elseif ($dayOfWeek == 6) //Samedi
        {
            $cle = 'sam';
        } elseif (estVS($jour)) {
            $cle = 'jvs';
        }
        $valeur[$cle][] = $donnee['valeurCorrige'];
        if ($donnee['nombre'] > $record[$cle]) {
            $record[$cle] = $donnee['nombre'];
        }
    }
    foreach ($cleJours as $cle) {
        $valeursJour = $valeur[$cle];
        $nbValeurs = count($valeursJour);

        //echo $heure . 'h - ' . $cle . ' - 20% : ' . $valeursJour[floor(0.8 * ($nbValeurs - 1))] . ' - 80% : ' . $valeursJour[floor(0.2 * ($nbValeurs - 1))] . ' - 90% : ' . $valeursJour[floor(0.1 * ($nbValeurs - 1))] . ' - 100% : ' . $valeursJour[0] . "<br />\n";

        $charts[$cle][20][] = $valeursJour[floor(0.85 * ($nbValeurs - 1))];
        $charts[$cle][50][] = $valeursJour[floor(0.5 * ($nbValeurs - 1))];
        $charts[$cle][80][] = $valeursJour[floor(0.20 * ($nbValeurs - 1))];
        $charts[$cle][90][] = $valeursJour[floor(0.08 * ($nbValeurs - 1))];
        $charts[$cle][100][] = $valeursJour[floor(0.05 * ($nbValeurs - 1))];
        $charts[$cle][101][] = $record[$cle];
    }
}

$hier = [];
$requete = $pdo->query('SELECT date, nombre
FROM `comptage_statusConso`
WHERE date >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND date < CURDATE()
ORDER BY date ASC');
$donnees = $requete->fetchAll();

foreach ($donnees as $donnee) {
    $jour = new DateTime($donnee['date'], new \DateTimeZone('Europe/Paris'));
    $heure = intval($jour->format('H'));
    $cle = 'job';
    if ($dayOfWeek == 7 || estJF($jour)) //Dimanche ou JF
    {
        $cle = 'djf';
    } elseif ($dayOfWeek == 6) //Samedi
    {
        $cle = 'sam';
    } elseif (estVS($jour)) {
        $cle = 'jvs';
    }
    $hier[$heure] = $donnee['nombre'];
}
?>

<script type="application/javascript" src="js/Chart.min.js"></script>
    <script type="application/javascript" src="js/jquery-3.2.1.min.js"></script>
<canvas id="chartjob" height="400px" width="700px"></canvas>
<canvas id="chartsam" height="400px" width="700px"></canvas>
<canvas id="chartdjf" height="400px" width="700px"></canvas>
<canvas id="chartjvs" height="400px" width="700px"></canvas>
<script>
$(document).ready( function () {
		var inputs = {
			min: -100,
			max: 100,
			count: 8,
			decimals: 2,
			continuity: 1
		};

		function generateLabels(config) {
			return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ,17 ,18 ,19 ,20, 21, 22, 23].map(h => h + 'h');
		}

		var options = {
			maintainAspectRatio: false,
			spanGaps: false,
			elements: {
				line: {
					tension: 0.4
				}
			},
			plugins: {
				filler: {
					propagate: false
				}
			},
			scales: {
				xAxes: [{
					ticks: {
						autoSkip: false,
						maxRotation: 0
					}
				}],
                yAxes: {
                    stacked: false
                }
            }
		};

        new Chart('chartjob', {
            type: 'line',
            data: {
                labels: generateLabels(),
                datasets: [{
                    backgroundColor: '#b3ee96',
                    data: [<?=implode($charts['job'][20], ', ');?>],
                    label: 'Faible',
                    fill: 'start',
                    order: 70
                },{
                    borderColor: 'rgb(76, 213, 233)',
                    data: [<?=implode($charts['job'][50], ', ');?>],
                    label: 'Médiane',
                    order: 10,
                    fill: false
                },{
                    backgroundColor: '#eee096',
                    data: [<?=implode($charts['job'][80], ', ');?>],
                    label: 'Normal',
                    fill: 'start',
                    order: 80
                },{
                    backgroundColor: '#eec396',
                    data: [<?=implode($charts['job'][90], ', ');?>],
                    label: 'Élevé',
                    fill: 'start',
                    order: 90
                },{
                    backgroundColor: '#ee9696',
                    data: [<?=implode($charts['job'][100], ', ');?>],
                    label: 'Exceptionnel',
                    fill: 'start',
                    order: 100
                },{
                    borderColor: 'rgb(255, 0, 0)',
                    data: [<?=implode($charts['job'][101], ', ');?>],
                    label: 'Record',
                    fill: false,
                    order: 20
                },{
                    borderColor: 'rgb(0, 0, 0)',
                    data: [<?=implode($hier, ', ');?>],
                    label: 'Hier',
                    fill: false,
                    order: 15
                }]
            },
            options: Chart.helpers.merge(options, {
                title: {
                    text: 'Baromètre de la circulation - Semaine',
                    display: true
                }
            })
        });

        new Chart('chartsam', {
            type: 'line',
            data: {
                labels: generateLabels(),
                datasets: [{
                    backgroundColor: '#b3ee96',
                    data: [<?=implode($charts['sam'][20], ', ');?>],
                    label: 'Faible',
                    fill: 'start',
                    order: 70
                },{
                    borderColor: 'rgb(76, 213, 233)',
                    data: [<?=implode($charts['sam'][50], ', ');?>],
                    label: 'Médiane',
                    order: 10,
                    fill: false
                },{
                    backgroundColor: '#eee096',
                    data: [<?=implode($charts['sam'][80], ', ');?>],
                    label: 'Normal',
                    fill: 'start',
                    order: 80
                },{
                    backgroundColor: '#eec396',
                    data: [<?=implode($charts['sam'][90], ', ');?>],
                    label: 'Élevé',
                    fill: 'start',
                    order: 90
                },{
                    backgroundColor: '#ee9696',
                    data: [<?=implode($charts['sam'][100], ', ');?>],
                    label: 'Exceptionnel',
                    fill: 'start',
                    order: 100
                },{
                    borderColor: 'rgb(255, 0, 0)',
                    data: [<?=implode($charts['sam'][101], ', ');?>],
                    label: 'Record',
                    fill: false,
                    order: 20
                }]
            },
            options: Chart.helpers.merge(options, {
                title: {
                    text: 'Baromètre de la circulation - Samedi',
                    display: true
                }
            })
        });

        new Chart('chartdjf', {
            type: 'line',
            data: {
                labels: generateLabels(),
                datasets: [{
                    backgroundColor: '#b3ee96',
                    data: [<?=implode($charts['djf'][20], ', ');?>],
                    label: 'Faible',
                    fill: 'start',
                    order: 70
                },{
                    borderColor: 'rgb(76, 213, 233)',
                    data: [<?=implode($charts['djf'][50], ', ');?>],
                    label: 'Médiane',
                    order: 10,
                    fill: false
                },{
                    backgroundColor: '#eee096',
                    data: [<?=implode($charts['djf'][80], ', ');?>],
                    label: 'Normal',
                    fill: 'start',
                    order: 80
                },{
                    backgroundColor: '#eec396',
                    data: [<?=implode($charts['djf'][90], ', ');?>],
                    label: 'Élevé',
                    fill: 'start',
                    order: 90
                },{
                    backgroundColor: '#ee9696',
                    data: [<?=implode($charts['djf'][100], ', ');?>],
                    label: 'Exceptionnel',
                    fill: 'start',
                    order: 100
                },{
                    borderColor: 'rgb(255, 0, 0)',
                    data: [<?=implode($charts['djf'][101], ', ');?>],
                    label: 'Record',
                    fill: false,
                    order: 20
                }]
            },
            options: Chart.helpers.merge(options, {
                title: {
                    text: 'Baromètre de la circulation - Dimanche et JF',
                    display: true
                }
            })
        });


        new Chart('chartjvs', {
            type: 'line',
            data: {
                labels: generateLabels(),
                datasets: [{
                    backgroundColor: '#b3ee96',
                    data: [<?=implode($charts['jvs'][20], ', ');?>],
                    label: 'Faible',
                    fill: 'start',
                    order: 70
                },{
                    borderColor: 'rgb(76, 213, 233)',
                    data: [<?=implode($charts['jvs'][50], ', ');?>],
                    label: 'Médiane',
                    order: 10,
                    fill: false
                },{
                    backgroundColor: '#eee096',
                    data: [<?=implode($charts['jvs'][80], ', ');?>],
                    label: 'Normal',
                    fill: 'start',
                    order: 80
                },{
                    backgroundColor: '#eec396',
                    data: [<?=implode($charts['jvs'][90], ', ');?>],
                    label: 'Élevé',
                    fill: 'start',
                    order: 90
                },{
                    backgroundColor: '#ee9696',
                    data: [<?=implode($charts['jvs'][100], ', ');?>],
                    label: 'Exceptionnel',
                    fill: 'start',
                    order: 100
                },{
                    borderColor: 'rgb(255, 0, 0)',
                    data: [<?=implode($charts['jvs'][101], ', ');?>],
                    label: 'Record',
                    fill: false,
                    order: 20
                }]
            },
            options: Chart.helpers.merge(options, {
                title: {
                    text: 'Baromètre de la circulation - Vacances Scolaires',
                    display: true
                }
            })
        });
});
</script>
