import pymysql
import datetime
from config import getVlsMysqlConnection
import serviceLib
from toolsLib import val


def calculerResume(slug, dateConsoDT, dureeConso):
    service = serviceLib.getConfigService(slug)

    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    minuteAvant = dateConsoDT - datetime.timedelta(minutes=1)
    dateConsoAvant = minuteAvant.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConso)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    proprietes = ['nbBike', 'nbFreeEDock', 'nbEDock']

    if service.config.eBike:
        proprietes.append('nbEBike')
    if service.config.overflow:
        proprietes.append('nbBikeOverflow')
        proprietes.append('maxBikeOverflow')
        if service.config.eBike:
            proprietes.append('nbEBikeOverflow')

    # On récupère la minute d'avant pour les bases de conso
    mysql = getVlsMysqlConnection()
    requete = mysql.cursor()
    requete.execute("SELECT s.code, s."+', s.'.join(proprietes)+" \
    FROM status s \
    INNER JOIN `statusConso` c ON c.id = s.idConso \
    WHERE s.service = " + str(service.id) + " AND (c.`date` >= '"+dateConsoAvant+"' AND c.`date` < '"+dateConso+"') \
    ORDER BY c.id DESC")
    statusConsoPrecedent = requete.fetchall()
    precedents = {}
    for row in statusConsoPrecedent:
        codeStation = int(row[0])
        for i, cle in enumerate(proprietes):
            if cle in ['nbBike', 'nbEBike']:
                valeurProp = int(row[i+1])
                if not codeStation in precedents:
                    precedents[codeStation] = {}
                precedents[codeStation][cle] = valeurProp

    requete = mysql.cursor()
    requete.execute("SELECT s.code, s."+', s.'.join(proprietes)+" \
    FROM status s \
    INNER JOIN `statusConso` c ON c.id = s.idConso \
    WHERE s.service = " + str(service.id) + " AND (c.`date` >= '"+dateConso+"' AND c.`date` < '"+dateConsoFin+"') \
    ORDER BY c.id ASC")
    statusConso = requete.fetchall()
    bikeList = {}

    # On insert ici les valeurs que l'on va calculer
    proprietes.append('nbEDockPerdus')

    for row in statusConso:
        codeStation = int(row[0])
        for i, cle in enumerate(proprietes):
            if cle == 'nbEDockPerdus':
                valeurProp = int(row[proprietes.index('nbEDock') + 1]) - int(
                    row[proprietes.index('nbBike') + 1]) - int(row[proprietes.index('nbFreeEDock') + 1])
                if service.config.eBike:
                    valeurProp -= int(row[proprietes.index('nbEBike') + 1])
            else:
                valeurProp = int(row[i+1])
            if cle in ['nbEDock', 'maxBikeOverflow']:
                bikeList[codeStation][cle] = valeurProp
            else:
                if codeStation not in bikeList:
                    bikeList[codeStation] = {
                        cle: {'data': [], 'min': valeurProp, 'max': valeurProp}}
                    if cle in ['nbBike', 'nbEBike']:
                        bikeList[codeStation][cle]['pris'] = 0
                        bikeList[codeStation][cle]['remis'] = 0
                elif cle not in bikeList[codeStation]:
                    bikeList[codeStation][cle] = {
                        'data': [], 'min': valeurProp, 'max': valeurProp}
                    if cle in ['nbBike', 'nbEBike']:
                        bikeList[codeStation][cle]['pris'] = 0
                        bikeList[codeStation][cle]['remis'] = 0
                else:
                    bikeList[codeStation][cle]['max'] = max(
                        bikeList[codeStation][cle]['max'], valeurProp)
                    bikeList[codeStation][cle]['min'] = min(
                        bikeList[codeStation][cle]['min'], valeurProp)

                if cle in ['nbBike', 'nbEBike']:
                    if codeStation not in precedents:
                        precedents[codeStation] = {}

                    if cle in precedents[codeStation]:
                        delta = valeurProp - precedents[codeStation][cle]
                        if delta > 0:
                            bikeList[codeStation][cle]['remis'] += delta
                        if delta < 0:
                            bikeList[codeStation][cle]['pris'] -= delta
                    precedents[codeStation][cle] = valeurProp

                bikeList[codeStation][cle]['data'].append(valeurProp)

    for codeStation in bikeList:
        for cle in bikeList[codeStation]:
            info = bikeList[codeStation][cle]
            if type(info) is dict:
                data = info['data']
                bikeList[codeStation][cle]['moyenne'] = sum(
                    data) / max(len(data), 1)

    for codeStation in bikeList:
        valeurs = bikeList[codeStation]
        requete = mysql.cursor()
        champsKey = ['id', 'service', 'code', 'date', 'duree', 'nbBikeMin', 'nbBikeMax', 'nbBikeMoyenne', 'nbBikePris', 'nbBikeRendu',
                     'nbFreeEDockMin', 'nbFreeEDockMax', 'nbFreeEDockMoyenne', 'nbEDock', 'nbEDockPerdusMin', 'nbEDockPerdusMax', 'nbEDockPerdusMoyenne']
        champsValues = [None, str(service.id), codeStation, dateConso, dureeConso, valeurs['nbBike']['min'], valeurs['nbBike']['max'], valeurs['nbBike']['moyenne'], valeurs['nbBike']['pris'], valeurs['nbBike']['remis'],
                        valeurs['nbFreeEDock']['min'], valeurs['nbFreeEDock']['max'], valeurs['nbFreeEDock']['moyenne'], valeurs['nbEDock'], valeurs['nbEDockPerdus']['min'], valeurs['nbEDockPerdus']['max'], valeurs['nbEDockPerdus']['moyenne']]

        if service.config.eBike:
            champsKey.append('nbEBikeMin')
            champsValues.append(valeurs['nbEBike']['min'])
            champsKey.append('nbEBikeMax')
            champsValues.append(valeurs['nbEBike']['max'])
            champsKey.append('nbEBikeMoyenne')
            champsValues.append(valeurs['nbEBike']['moyenne'])
            champsKey.append('nbEBikePris')
            champsValues.append(valeurs['nbEBike']['pris'])
            champsKey.append('nbEBikeRendu')
            champsValues.append(valeurs['nbEBike']['remis'])

        if service.config.overflow:
            champsKey.append('nbBikeOverflowMin')
            champsValues.append(valeurs['nbBikeOverflow']['min'])
            champsKey.append('nbBikeOverflowMax')
            champsValues.append(valeurs['nbBikeOverflow']['max'])
            champsKey.append('nbBikeOverflowMoyenne')
            champsValues.append(valeurs['nbBikeOverflow']['moyenne'])
            if service.config.eBike:
                champsKey.append('nbEBikeOverflowMin')
                champsValues.append(valeurs['nbEBikeOverflow']['min'])
                champsKey.append('nbEBikeOverflowMax')
                champsValues.append(valeurs['nbEBikeOverflow']['max'])
                champsKey.append('nbEBikeOverflowMoyenne')
                champsValues.append(valeurs['nbEBikeOverflow']['moyenne'])
            champsKey.append('maxBikeOverflow')
            champsValues.append(valeurs['maxBikeOverflow'])

        requete.execute('INSERT INTO `resumeStatus` (' + ', '.join(champsKey) +
                        ') VALUES (' + ', '.join(map(val, champsValues)) + ')')


def calculerResumeOfResume(slug, dateConsoDT, dureeConsoOrigine, dureeConsoFinale):
    service = serviceLib.getConfigService(slug)

    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConsoFinale)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    # On récupère les conso
    proprietes = ['nbBikeMin', 'nbBikeMax', 'nbBikeMoyenne', 'nbBikePris', 'nbBikeRendu',
                  'nbFreeEDockMin', 'nbFreeEDockMax', 'nbFreeEDockMoyenne', 'nbEDock', 'nbEDockPerdusMin', 'nbEDockPerdusMax', 'nbEDockPerdusMoyenne']
    if service.config.eBike:
        proprietes.append('nbEBikeMin')
        proprietes.append('nbEBikeMax')
        proprietes.append('nbEBikeMoyenne')
        proprietes.append('nbEBikePris')
        proprietes.append('nbEBikeRendu')

    if service.config.overflow:
        proprietes.append('nbBikeOverflowMin')
        proprietes.append('nbBikeOverflowMax')
        proprietes.append('nbBikeOverflowMoyenne')
        if service.config.eBike:
            proprietes.append('nbEBikeOverflowMin')
            proprietes.append('nbEBikeOverflowMax')
            proprietes.append('nbEBikeOverflowMoyenne')
        proprietes.append('maxBikeOverflow')

    mysql = getVlsMysqlConnection()
    requete = mysql.cursor()
    requete.execute("SELECT code, "+', '.join(proprietes)+" FROM `resumeStatus` WHERE service = " + str(service.id) + " AND duree = "+str(
        dureeConsoOrigine)+" and date >= '"+dateConso+"' and date < '"+dateConsoFin+"' order by code")
    consos = requete.fetchall()
    precedentCode = None
    infosCourantes = {}
    for row in consos:
        codeStation = int(row[0])

        if precedentCode != None and precedentCode != codeStation:  # On change de station
            # On enregistre la conso
            enregistrerConso(mysql, service, proprietes, infosCourantes,
                             precedentCode, dateConso, dureeConsoFinale)
            infosCourantes = {}

        for i, cle in enumerate(proprietes):
            if cle[-7:] == 'Moyenne':
                valeurProp = float(row[i+1])
            else:
                valeurProp = int(row[i+1])

            if cle in infosCourantes:
                if cle[-7:] == 'Moyenne':
                    infosCourantes[cle].append(valeurProp)
                elif cle[-3:] == 'Min':
                    infosCourantes[cle] = min(infosCourantes[cle], valeurProp)
                elif cle[-3:] == 'Max' or cle == 'maxBikeOverflow' or cle == 'nbEDock':
                    infosCourantes[cle] = max(infosCourantes[cle], valeurProp)
                else:
                    infosCourantes[cle] += valeurProp
            else:
                if cle[-7:] == 'Moyenne':
                    infosCourantes[cle] = [valeurProp]
                else:
                    infosCourantes[cle] = valeurProp

        precedentCode = codeStation
    # Et on oublie pas le dernier !
    enregistrerConso(mysql, service, proprietes, infosCourantes,
                     precedentCode, dateConso, dureeConsoFinale)


def enregistrerConso(mysql, service, proprietes, infosCourantes, codeStation, dateConso, dureeConso):
    # On traite les moyennes et on prépare la requête
    if infosCourantes != {}:
        strValeurs = ""
        for i, cle in enumerate(proprietes):
            if cle[-7:] == 'Moyenne':
                data = infosCourantes[cle]
                moyenne = sum(data) / max(len(data), 1)
                infosCourantes[cle] = moyenne
            if i != 0:
                strValeurs += ', '
            strValeurs += val(infosCourantes[cle])

        requete = mysql.cursor()
        requete.execute('INSERT INTO `resumeStatus` (`id`, service, `code`, `date`, `duree`, '+', '.join(proprietes)+') VALUES \
        (NULL, ' + val(str(service.id)) + ', '+val(codeStation)+', '+val(dateConso)+', '+val(dureeConso)+', '+strValeurs+')')


def debuterCalculResume(slug, periode, dateCourante=datetime.datetime.now()):
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periode)
    debutPeriodeMinute = dateConsoDT.minute % periode
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0, minute=dateConsoDT.minute - debutPeriodeMinute)
    calculerResume(slug, dateConsoDT, periode)


def debuterCalculResumeOfResume(slug, periodeOrigine, periodeFinale, dateCourante=datetime.datetime.now()):
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periodeFinale)
    debutPeriodeMinute = (dateConsoDT.hour * 60 +
                          dateConsoDT.minute) % periodeFinale
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0) - datetime.timedelta(minutes=debutPeriodeMinute)
    calculerResumeOfResume(slug, dateConsoDT, periodeOrigine, periodeFinale)

# StatutConso


def calculerResumeConso(slug, dateConsoDT, dureeConso):
    service = serviceLib.getConfigService(slug)

    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConso)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    proprietes = ['nbStation', 'nbStationDetecte',
                  'nbBike', 'nbFreeEDock', 'nbEDock']

    if service.config.eBike:
        proprietes.append('nbEBike')

    mysql = getVlsMysqlConnection()

    requete = mysql.cursor()
    requete.execute("SELECT "+', '.join(proprietes)+" \
    FROM statusConso \
    WHERE service = " + str(service.id) + " AND (`date` >= '"+dateConso+"' AND `date` < '"+dateConsoFin+"') \
    ORDER BY id ASC")
    statusConso = requete.fetchall()
    bikeList = {}
    for row in statusConso:
        for i, cle in enumerate(proprietes):
            if not row[i] is None:
                valeurProp = int(row[i])
                if cle in ['nbStation', 'nbStationDetecte', 'nbEDock']:
                    if not cle in bikeList:
                        bikeList[cle] = valeurProp
                    else:
                        bikeList[cle] = max(bikeList[cle], valeurProp)
                else:
                    if not cle in bikeList:
                        bikeList[cle] = {'data': [],
                                         'min': valeurProp, 'max': valeurProp}
                    else:
                        bikeList[cle]['max'] = max(
                            bikeList[cle]['max'], valeurProp)
                        bikeList[cle]['min'] = min(
                            bikeList[cle]['min'], valeurProp)

                    bikeList[cle]['data'].append(valeurProp)

    for cle in bikeList:
        info = bikeList[cle]
        if type(info) is dict:
            data = info['data']
            bikeList[cle]['moyenne'] = sum(data) / max(len(data), 1)

    if len(bikeList) > 2:  # S'il y a plus que simplement le nombre de stations
        valeurs = bikeList
        requete = mysql.cursor()
        # nbEDock peut être null
        nbEDock = 0
        if nbEDock in valeurs:
            nbEDock = valeurs['nbEDock']

        champsKey = ['id', 'service', 'date', 'duree', 'nbStation', 'nbStationDetecte', 'nbBikeMin',
                     'nbBikeMax', 'nbBikeMoyenne', 'nbFreeEDockMin', 'nbFreeEDockMax', 'nbFreeEDockMoyenne', 'nbEDock']
        champsValues = [None, str(service.id), dateConso, dureeConso, valeurs['nbStation'], valeurs['nbStationDetecte'], valeurs['nbBike']['min'], valeurs['nbBike']
                        ['max'], valeurs['nbBike']['moyenne'], valeurs['nbFreeEDock']['min'], valeurs['nbFreeEDock']['max'], valeurs['nbFreeEDock']['moyenne'], nbEDock]

        if service.config.eBike:
            champsKey.append('nbEBikeMin')
            champsValues.append(valeurs['nbEBike']['min'])
            champsKey.append('nbEBikeMax')
            champsValues.append(valeurs['nbEBike']['max'])
            champsKey.append('nbEBikeMoyenne')
            champsValues.append(valeurs['nbEBike']['moyenne'])

        requete.execute('INSERT INTO `resumeConso` (' + ', '.join(champsKey) +
                        ') VALUES (' + ', '.join(map(val, champsValues)) + ')')


def calculerResumeOfResumeConso(slug, dateConsoDT, dureeConsoOrigine, dureeConsoFinale):
    service = serviceLib.getConfigService(slug)

    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConsoFinale)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    # On récupère les conso
    proprietes = ['nbStation', 'nbStationDetecte', 'nbBikeMin', 'nbBikeMax',
                  'nbBikeMoyenne', 'nbFreeEDockMin', 'nbFreeEDockMax', 'nbFreeEDockMoyenne', 'nbEDock']

    if service.config.eBike:
        proprietes.append('nbEBikeMin')
        proprietes.append('nbEBikeMax')
        proprietes.append('nbEBikeMoyenne')

    mysql = getVlsMysqlConnection()
    requete = mysql.cursor()
    requete.execute("SELECT "+', '.join(proprietes)+" FROM `resumeConso` WHERE service = " + str(service.id) + " AND duree = "+str(
        dureeConsoOrigine)+" and date >= '"+dateConso+"' and date < '"+dateConsoFin+"' order by date")
    consos = requete.fetchall()
    infosCourantes = {}
    for row in consos:
        for i, cle in enumerate(proprietes):
            if cle[-7:] == 'Moyenne':
                valeurProp = float(row[i])
            else:
                valeurProp = int(row[i])

            if cle in infosCourantes:
                if cle[-7:] == 'Moyenne':
                    infosCourantes[cle].append(valeurProp)
                elif cle[-3:] == 'Min':
                    infosCourantes[cle] = min(infosCourantes[cle], valeurProp)
                else:
                    infosCourantes[cle] = max(infosCourantes[cle], valeurProp)
            else:
                if cle[-7:] == 'Moyenne':
                    infosCourantes[cle] = [valeurProp]
                else:
                    infosCourantes[cle] = valeurProp

    # Et on enregistre
    if infosCourantes != {}:
        strValeurs = ""
        for i, cle in enumerate(proprietes):
            if cle[-7:] == 'Moyenne':
                data = infosCourantes[cle]
                moyenne = sum(data) / max(len(data), 1)
                infosCourantes[cle] = moyenne
            if i != 0:
                strValeurs += ', '
            strValeurs += val(infosCourantes[cle])

        requete = mysql.cursor()
        requete.execute('INSERT INTO `resumeConso` (`id`, service, `date`, `duree`, '+', '.join(proprietes)+') VALUES \
        (NULL, ' + val(str(service.id)) + ', '+val(dateConso)+', '+val(dureeConsoFinale)+', '+strValeurs+')')


def debuterCalculResumeConso(slug, periode, dateCourante=datetime.datetime.now()):
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periode)
    debutPeriodeMinute = dateConsoDT.minute % periode
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0, minute=dateConsoDT.minute - debutPeriodeMinute)
    calculerResumeConso(slug, dateConsoDT, periode)


def debuterCalculResumeOfResumeConso(slug, periodeOrigine, periodeFinale, dateCourante=datetime.datetime.now()):
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periodeFinale)
    debutPeriodeMinute = (dateConsoDT.hour * 60 +
                          dateConsoDT.minute) % periodeFinale
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0) - datetime.timedelta(minutes=debutPeriodeMinute)
    calculerResumeOfResumeConso(
        slug, dateConsoDT, periodeOrigine, periodeFinale)


def calculerToutResume(slug, dateMin, dateMax):
    periodes = [5, 15, 60, 360, 1440]
    periodePrecedente = 0
    for periode in periodes:
        dateCourante = dateMin
        print("-> Début période " + str(periode))
        while dateCourante + datetime.timedelta(minutes=periode) <= dateMax:
            print("Période " + str(periode)+'; date = '+str(dateCourante))
            # on met la date courante à la fin de la période, car, ça calcule à rebours
            dateCourante = dateCourante + datetime.timedelta(minutes=periode)
            if periode == 5:
                debuterCalculResume(slug, periode, dateCourante)
                debuterCalculResumeConso(slug, periode, dateCourante)
            else:
                debuterCalculResumeOfResume(
                    slug, periodePrecedente, periode, dateCourante)
                debuterCalculResumeOfResumeConso(
                    slug, periodePrecedente, periode, dateCourante)
        periodePrecedente = periode
        print("-> Fin période " + str(periode))
