import urllib.request
import os
import pymysql
from datetime import datetime
from config import getVlsMysqlConnection
from adresseLib import getAdresse, getInsee
from gbfsLib import getStationIdByLatLong
import hashlib
import time
import ssl
import csv
from toolsLib import val
import serviceLib


def getCSVUrl():
    return "https://data.iledefrance-mobilites.fr/explore/dataset/jcdecaux-bike-stations-data/download/?format=csv&timezone=Europe/Paris&lang=fr&use_labels_for_header=true&csv_separator=%3B"


def getAllStation(slug):
    mysql = getVlsMysqlConnection()

    service = serviceLib.getConfigService(slug)

    # On récupère la liste des stations déjà en base
    requete = mysql.cursor()
    requete.execute(
        'SELECT code' + (', dateOuverture' if service.config.dateOuverture else '') + ' FROM stations')
    rep = requete.fetchall()
    requete.close()
    aujourdhui = datetime.today().date()
    stations = []
    stationsFutur = None
    if service.config.dateOuverture:
        stationsFutur = []
    for row in rep:
        stations.append(row[0])
        if service.config.dateOuverture and (row[1] is None or row[1] > aujourdhui):
            stationsFutur.append(row[0])

    nbTotalBike = 0
    nbTotalEBike = 0
    nbTotalFreeEDock = 0
    nbTotalEDock = 0
    nbTotalBikeOverflow = 0
    nbTotalEBikeOverflow = 0

    # On créer une conso pour avoir son id
    requete = mysql.cursor()
    requete.execute(
        'INSERT INTO statusConso (date, service) VALUES (NOW(), '+val(service.id) + ')')
    mysql.commit()
    idConso = requete.lastrowid
    requete.close()
    strIdConso = str(idConso)
    nbStationsOuvertes = 0
    nbStationsOuvertesDetectees = 0

    urlDataStations = getCSVUrl()
    tmpFileName = 'detailsStations.csv'

    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    with urllib.request.urlopen(urlDataStations, context=ctx) as u, \
            open(tmpFileName, 'wb') as f:
        f.write(u.read())

    with open(tmpFileName, newline='') as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        pos = None
        for infoStation in data:
            if pos is None:
                print("> Lecture de l'entête")
                # Entête
                i = 0
                pos = {}
                for valeur in infoStation:
                    pos[valeur] = i
                    i += 1
                del i
                del valeur
                continue

            if infoStation[pos['Contract Name']] != slug:
                continue

            try:
                codeStation = int(infoStation[pos['number']])
            except Exception:
                codeStation = 0

            if codeStation > 0:
                if codeStation not in stations:
                    position = infoStation[pos['position']]
                    infoPosition = position.split(',')
                    latitude = infoPosition[0]
                    longitude = infoPosition[1]
                    if service.config.dateOuverture:
                        if infoStation[pos['state']] != 'Operative':
                            strDateOuverture = 'NULL'
                        else:
                            strDateOuverture = 'CURDATE()'
                    # On récupère également le stationId des données GTFS
                    # stationId = getStationIdByLatLong(latitude, longitude)

                    requete = mysql.cursor()
                    requete.execute('INSERT INTO stations (code, service, name, longitude, latitude, type' + (', dateOuverture' if service.config.dateOuverture else '') + ', adresse, stationId) VALUES \
                    ('+val(codeStation)+', '+val(service.id)+', '+val(infoStation[pos['name']])+', '+val(longitude)+', '+val(latitude)+', '+val(infoStation[pos['banking']])+(', '+strDateOuverture if service.config.dateOuverture else '')+', '+val(infoStation[pos['address']])+', '+val(None)+')')
                    mysql.commit()
                    requete.close()
                    stations.append(codeStation)

                nbBike = int(infoStation[pos['Available Bikes']])
                nbEbike = 0
                if service.config.eBike:
                    nbEbike = int(infoStation[pos['nbEbike']])
                nbFreeEDock = int(
                    infoStation[pos['Available Bike Stands']]) + (int(infoStation[pos['nbFreeEDock']]) if service.config.eBike else 0)
                nbEDock = int(
                    infoStation[pos['Bike Stands']])+(int(infoStation[pos['nbEDock']]) if service.config.eBike else 0)
                nbBikeOverflow = 0
                nbEBikeOverflow = 0
                if service.config.overflow:
                    nbBikeOverflow = int(infoStation[pos['nbBikeOverflow']])
                    if service.config.eBike:
                        nbEBikeOverflow = int(
                            infoStation[pos['nbEBikeOverflow']])
                requete = mysql.cursor()
                requete.execute('INSERT INTO status (code, service, idConso, state, nbBike' + (', nbEBike' if service.config.eBike else '') + ', nbFreeEDock, nbEDock' + (', nbBikeOverflow' + (', nbEBikeOverflow' if service.config.eBike else '') + ', maxBikeOverflow, overflow, overflowActivation' if service.config.overflow else '') + ') VALUES \
                ('+val(codeStation)+', '+val(service.id)+', '+val(strIdConso)+', '+val(infoStation[pos['status']])+', '+val(nbBike)+(', '+val(nbEbike) if service.config.eBike else '')+', '+val(nbFreeEDock)+', '+val(nbEDock)+((', '+val(nbBikeOverflow)+((', '+val(nbEBikeOverflow)) if service.config.eBike else '')+', '+val(infoStation[pos['maxBikeOverflow']])+', '+val(infoStation[pos['overflow']])+', '+val(infoStation[pos['overflowActivation']])) if service.config.overflow else '')+')')
                mysql.commit()
                requete.close()

                # On met à jour la station au besoin
                if service.config.dateOuverture and codeStation in stationsFutur and nbEDock > 0:
                    requete = mysql.cursor()
                    requete.execute('UPDATE stations \
                    SET dateOuverture = CURDATE() WHERE code = '+val(codeStation))
                    mysql.commit()
                    requete.close()

                # On ajoute a la conso
                nbTotalBike += nbBike
                nbTotalEBike += nbEbike
                nbTotalFreeEDock += nbFreeEDock
                nbTotalEDock += nbEDock
                nbTotalBikeOverflow += nbBikeOverflow
                nbTotalEBikeOverflow += nbEBikeOverflow
                if infoStation[pos['status']] == "OPEN":
                    nbStationsOuvertes += 1
                if nbEDock > 0 and nbBike + nbEbike + nbFreeEDock > 0:
                    nbStationsOuvertesDetectees += 1
    os.remove(tmpFileName)

    # On insert tout dans le statut
    requete = mysql.cursor()
    requete.execute('UPDATE statusConso SET \
    nbStation = '+str(nbStationsOuvertes)+', \
    nbStationDetecte = '+str(nbStationsOuvertesDetectees)+' ,\
    nbBike = '+str(nbTotalBike)+((', \
    nbEbike = '+str(nbTotalEBike)) if service.config.eBike else '')+((', \
    nbBikeOverflow = '+str(nbTotalBikeOverflow)) if service.config.overflow else '')+((', \
    nbEbikeOverflow = '+str(nbTotalEBikeOverflow)) if service.config.overflow and service.config.eBike else '')+', \
    nbEDock = '+str(nbTotalEDock)+', \
    nbFreeEDock = '+str(nbTotalFreeEDock)+' \
    WHERE id = '+strIdConso)
    mysql.commit()
    requete.close()
    mysql.close()
