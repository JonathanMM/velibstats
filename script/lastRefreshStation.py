import pymysql
from config import getMysqlConnection
from gbfsLib import getGBFSData
import datetime

mysql = getMysqlConnection()
data = getGBFSData()
for station in data:
    lastRefresh = datetime.datetime.fromtimestamp(int(station['last_reported']), datetime.timezone.utc)
    stationId = station['station_id']
    requete = mysql.cursor()
    requete.execute('UPDATE stations SET lastRefresh = "'+lastRefresh.strftime("%Y-%m-%d %H:%M:%S")+'" WHERE stationId = '+str(stationId))