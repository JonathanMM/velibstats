{include file='./header.tpl'}
<header class="header">
    <h1>Comptages Nocle</h1> 
    <h2>site non officiel</h2>
</header>
<div id="content">
<form action="barometre.php" method="get">
    Date : <input name="date" type="date" /> <input type="submit" value="Filtrer" />
</form>
<p>
    <a href="barometre.php?date={$dateVeille}">&lt; Jour précédent</a>
    {if not $isLive} − <a href="barometre.php?date={$dateLendemain}">Jour suivant &gt;</a>{/if}
</p>
<p>
Baromètre issu du site comptages.nocle.fr
</p>
<script type="application/javascript" src="js/Moment.min.js"></script>
<script type="application/javascript" src="js/Chart.min.js"></script>
<script type="application/javascript" src="js/jquery-3.2.1.min.js"></script>

<canvas id="chart" height="700px" width="1000px"></canvas>
<script>
$(document).ready( function () {
		var inputs = {
			min: -100,
			max: 100,
			count: 8,
			decimals: 2,
			continuity: 1
		};

		function generateLabels(config) {
			return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ,17 ,18 ,19 ,20, 21, 22, 23].map(h => h + 'h');
		}

		var options = {
            responsive: false,
			maintainAspectRatio: false,
			spanGaps: false,
			elements: {
				line: {
					tension: 0.4
				}
			},
			plugins: {
				filler: {
					propagate: false
				}
			},
			scales: {
				xAxes: [{
                    type: 'time',
                    time: {
                        parser: 'HH:mm:ss',
                        tooltipFormat: 'HH\\h mm',
                        unit: 'hour',
                        displayFormats: {
                            hour: 'HH\\h'
                        }
                    },
					ticks: {
						autoSkip: false,
						maxRotation: 0
					}
				}],
                yAxes: {
                    stacked: false
                }
            }
		};

        new Chart('chart', {
            type: 'line',
            data: {
                datasets: [{
                    backgroundColor: '#b3ee96',
                    data: {$centile20},
                    label: 'Faible',
                    fill: 'start',
                    order: 70
                },{
                    borderColor: 'rgb(130, 200, 230)',
                    data: {$centile50},
                    label: 'Médiane',
                    order: 10,
                    fill: false
                },{
                    backgroundColor: '#eee096',
                    data: {$centile80},
                    label: 'Normal',
                    fill: 'start',
                    order: 80
                },{
                    backgroundColor: '#eec396',
                    data: {$centile90},
                    label: 'Élevé',
                    fill: 'start',
                    order: 90
                },{
                    backgroundColor: '#ee9696',
                    data: {$centile95},
                    label: 'Exceptionnel',
                    fill: 'start',
                    order: 100
                },{
                    borderColor: 'rgb(255, 20, 0)',
                    data: {$centile100},
                    label: 'Record',
                    fill: false,
                    order: 20
                },{if not $isLive}{
                    borderColor: 'rgb(0, 0, 0)',
                    data: {$hier},
                    label: 'Passages',
                    fill: false,
                    order: 15
                },{/if}{
                    borderColor: 'rgb(100, 0, 130)',
                    data: {$estimation},
                    label: 'Estimation{if $isLive} du jour{/if}',
                    borderDash: [3],
                    fill: false,
                    order: 10
                }]
            },
            options: Chart.helpers.merge(options, {
                title: {
                    text: 'Baromètre de la circulation − {$dateJour}',
                    display: true
                }
            })
        });
});
</script>
<p>
    Les estimations sont réalisées le jour même en utilisant les données disponible. Le fond de graphique est recalculé chaque semaine, à partir de données corrigés (comparaisons sur un même nombre de stations). La courbe record correspond au maximum atteint heure par heure.<br />La légende est interactive, vous pouvez cliquer sur une courbe pour la faire disparaître. 
</p>
    {include file="./credits.tpl"}
</div>
{include file="./footer.tpl"}