{include file='./header.tpl'}
<header class="header">
    <h1>Comptages Nocle</h1> 
    <h2>site non officiel</h2>
</header>
<div id="content">
    <div id="statsCarteArea">
        <div id="statsConsoArea">
            {include file='./index_statsConso.tpl'}
        </div>
        <div id="carteArea"></div>
    </div>
    <select id="typeGraphiqueSelect" style="display: none">
        <option value="_double">Conso</option>
    </select>
    <select id="dureeGraphiqueSelect">
        <option value="septJours" selected>Une semaine - Période d'une heure</option>
        <option value="unMois">Un mois - Période de six heures</option>
        <option value="unMoisParJour">Un mois - Période d'un jour</option>
        <option value="unTrimestre">Un trimestre - Période d'un jour</option>
        <option value="unAn">Un an - Période d'un jour</option>
        <option value="cinqAns">Cinq ans - Période d'un jour</option>
    </select> - Graphique issu du site comptages.nocle.fr
    <div id="chartArea">
        <canvas id="chartBikes" height="500" width="1200"></canvas>
    </div>
    {include file="./credits.tpl"}
    <h2>Points de comptage</h2>
    <table id="stations">
        <thead>
            <tr>
                <th>Code</th>
                <th>Nom</th>
                <th>Date d'ouverture</th>
                <th>Nombre de passage</th>
            </tr>
        </thead>
    </table>
    <script type="text/javascript">
    var codeStation = -1;
    </script>
    <script type="application/javascript">
        $(document).ready( function () {
            var dt = $('#stations').DataTable({
                ajax: 'api.php?action=getDataConso&dateResume={$dateResume}',
                columns: [{
                    data: 'codeStr',
                    render: function(data, type, row, meta)
                    {
                        return '<a href="station.php?code='+row.code+'">'+data+'</a>';
                    }
                },{
                    data: 'name',
                    render: function(data, type, row, meta)
                    {
                        return '<a href="station.php?code='+row.code+'">'+data+'</a>';
                    }
                },{
                    data: 'dateOuverture',
                    render: function(data, type, row, meta)
                    {
                        if(data == 'Non ouvert')
                            return data;
                        var date = new Date(data);
                        if(type == 'sort') //Pour le tri, on fait en sorte que la valeur soit triable
                            return date.getFullYear() + '-' + putZero(date.getMonth()+1) + '-' + putZero(date.getDate());
                        
                        return putZero(date.getDate()) + '/' + putZero(date.getMonth()+1) + '/' + date.getFullYear();
                    }
                },{
                    data: 'nombre'
                }],
                language: dtTraduction
            });
            voirCarteEtat();
        });

        function recupererCarte(url)
        {
            getData(url).then(function(svg) {
                $("#carteArea").html(svg);
            });
        }

        function voirCarteEtat()
        {
            recupererCarte('api.php?action=getCommunesCarte&dateResume={$dateResume}');
        }
    </script>
</div>
{include file="./footer.tpl"}