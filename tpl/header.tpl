<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Vélib Nocle, différentes statistiques sur l'utilisation de velib 2. Site non officiel">
    <title>Vélib Nocle (site non officiel)</title>
    <script type="application/javascript" src="js/Chart.min.js"></script>
    <script type="application/javascript" src="js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

    <script type="text/javascript" src="js/datatables.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script> 
    <link rel="stylesheet" href="css/pure-min.css">
    
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/side-menu-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
            <link rel="stylesheet" href="css/layouts/side-menu.css">
        <!--<![endif]-->
</head>
<body>

<div id="layout">
    <!-- Menu toggle -->
    <a href="#menu" id="menuLink" class="menu-link">
        <!-- Hamburger icon -->
        <span></span>
    </a>

    <div id="menu">
        <div class="pure-menu">
            <a class="pure-menu-heading" href="index.php">Vélib Nocle</a>

            <ul class="pure-menu-list">
                <li class="pure-menu-item {if $pageId eq 'accueil'}pure-menu-selected{/if}"><a href="index.php" class="pure-menu-link">Accueil</a></li>
                <li class="pure-menu-item {if $pageId eq 'stats'}pure-menu-selected{/if}"><a href="stats.php" class="pure-menu-link">Plus de stats</a></li>
                <li class="pure-menu-item {if $pageId eq 'communes'}pure-menu-selected{/if}"><a href="communes.php" class="pure-menu-link">Par commune</a></li>
                <li class="pure-menu-item {if $pageId eq 'comptage'}pure-menu-selected{/if}"><a href="https://comptages.nocle.fr" class="pure-menu-link">Comptages Nocle</a></li>
                <li class="pure-menu-item"><a href="https://cristolib.nocle.fr" class="pure-menu-link">CristoLib Nocle</a></li>
                <li class="pure-menu-item"><a href="https://velo2.nocle.fr" class="pure-menu-link">Vél'O2 Nocle</a></li>

                {* <li class="pure-menu-item menu-item-divided pure-menu-selected">
                    <a href="#" class="pure-menu-link">Services</a>
                </li>

                <li class="pure-menu-item"><a href="#" class="pure-menu-link">Contact</a></li> *}
            </ul>
        </div>
    </div>

    <div id="main">