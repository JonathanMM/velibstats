import pymysql
from config import getMysqlConnection

def nettoyerInstantanne(date):
    dateStr = date.strftime("%Y-%m-%d %H:%M:%S")
    mysql = getMysqlConnection()
    requete = mysql.cursor()
    requete.execute('DELETE FROM status WHERE idConso IN ( \
SELECT id FROM statusConso \
WHERE date < "'+dateStr+'")')
    mysql.commit()
    requete.close()
    requete = mysql.cursor()
    requete.execute('DELETE FROM statusConso WHERE date < "'+dateStr+'"')
    mysql.commit()
    requete.close()
    mysql.close()

def nettoyerConso(date, filtre):
    dateStr = date.strftime("%Y-%m-%d %H:%M:%S")
    mysql = getMysqlConnection()
    requete = mysql.cursor()
    requete.execute('DELETE FROM resumeStatus WHERE date < "'+dateStr+'" AND ('+filtre+')')
    mysql.commit()
    requete.close()
    requete = mysql.cursor()
    requete.execute('DELETE FROM resumeConso WHERE date < "'+dateStr+'" AND ('+filtre+')')
    mysql.commit()
    requete.close()
    mysql.close()

def optimiserBDD():
    mysql = getMysqlConnection()
    requete = mysql.cursor()
    requete.execute('OPTIMIZE TABLE resumeConso, resumeStatus, status, statusConso')
    mysql.commit()
    requete.close()
    mysql.close()