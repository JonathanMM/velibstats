import dumpLib
import datetime
import resumeLib
import os
import nettoyageLib

jour = datetime.datetime(2020, 9, 14)
dateMax = datetime.datetime(2020, 9, 18)
while jour <= dateMax:
    print("--- Jour traité : " + str(jour.strftime("%Y-%m-%d")))
    print("Nettoyage de la table station")
    dumpLib.cleanStationTable()
    print("Téléchargement du dump")
    nomFichier = dumpLib.downloadDumpConso(jour)
    print("Importation du dump")
    dumpLib.importerDumpConso(nomFichier)
    demain = jour + datetime.timedelta(days=1)
    print("Suppression du fichier dump")
    os.remove(nomFichier)
    # print("Optimisation")
    # nettoyageLib.optimiserBDD()

    jour += datetime.timedelta(days=1)
