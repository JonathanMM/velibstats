<div id="credits">
    Ce site n'est pas un site officiel d'un opérateur. Les données utilisées proviennent du jeu de données 
    <a href="https://data.iledefrance-mobilites.fr/explore/dataset/jcdecaux-bike-stations-data/table/?location=13,49.03719,2.08551&basemap=jawg.streets">Stations de vélo en libre service (VélO2 et Cristolib) - Disponibilités en temps réel</a>
    disponible sur le site <a href="https://data.iledefrance-mobilites.fr/">Île-de-France Mobilités Open Data</a> sous licence Open License v1.0, et appartiennent à leur propriétaire. - 
    <a href="https://framagit.org/JonathanMM/velibstats">Site du projet et sources</a> - 
    Auteur : JonathanMM (<a href="https://twitter.com/Jonamaths">@Jonamaths</a>)
</div>