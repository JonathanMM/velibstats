<?php
require_once 'config.php';
include_once 'functions.php';
require_once 'libs/Smarty.class.php';
include_once 'includes/constantes.php';

$smarty = new Smarty();

$smarty->assign(array(
    'pageId' => 'communes',
));

//On récupère les communes adhérentes Vélib au Syndicat Autolib'Vélib'Métropole
$requete = $pdo->query('SELECT insee, nom_complet, dept FROM commune WHERE avm&' . COMMUNE_AVM_VELIB . '!=0 ORDER BY insee');
$communes = $requete->fetchAll(PDO::FETCH_ASSOC);

$departements = array();

foreach ($communes as $commune) {
    $dept = $commune['dept'];
    if (!isset($departements[$dept])) {
        $departements[$dept] = array('communes' => array(), 'numero' => $dept, 'nom' => $nomDept[$dept]);
    }

    $departements[$dept]['communes'][] = $commune;
}

$smarty->assign(array(
    'departements' => $departements,
));

$smarty->display('tpl/communes.tpl');
exit();
