<?php
class Service
{
    public $name;
    public $id;
    public $slug;
    public $config;

    public function __construct(array $configToml)
    {
        $this->name = $configToml['name'];
        $this->id = $configToml['id'];
        $this->slug = $configToml['slug'];
        $this->config = new ServiceConfig($configToml['config']);
    }
}
