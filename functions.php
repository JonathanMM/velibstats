<?php
include_once 'carte.php';

function displayCodeStation($code)
{
    if ($code < 10000) {
        return '0' . $code;
    }

    return $code;
}

function getCommuneStation($codeStation)
{
    global $pdo;

    $res = $pdo->query('SELECT insee FROM tranche WHERE debut <= ' . $codeStation . ' AND fin >= ' . $codeStation);
    $ligne = $res->fetch();

    if (is_null($ligne)) {
        return null;
    }

    return $ligne['insee'];
}

function getStatusByIdConso($idConso, $filtreWhere = null)
{
    global $pdo;
    $requete = $pdo->query('SELECT * FROM status INNER JOIN `stations` ON stations.code = status.code WHERE idConso = ' . $idConso .
        (is_null($filtreWhere) ? '' : ' AND (' . $filtreWhere . ')') . ' ORDER BY status.code ASC');
    if ($requete === false) {
        return null;
    }

    $data = $requete->fetchAll(PDO::FETCH_ASSOC);
    $retour = [];
    foreach ($data as $station) {
        $retour[] = array(
            'code' => $station['code'],
            'codeStr' => displayCodeStation($station['code']),
            'name' => $station['name'],
            'dateOuverture' => is_null($station['dateOuverture']) ? 'Non ouvert' : $station['dateOuverture'],
            'state' => $station['state'],
            'nbBike' => $station['nbBike'],
            'nbEbike' => $station['nbEBike'],
            'nbBikeOverflow' => $station['nbBikeOverflow'],
            'nbEbikeOverflow' => $station['nbEBikeOverflow'],
            'nbFreeEDock' => $station['nbFreeEDock'],
            'nbEDock' => $station['nbEDock'],
            'latitude' => $station['latitude'],
            'longitude' => $station['longitude'],
            'overflow' => $station['overflow'],
            'overflowActivation' => $station['overflowActivation'],
        );
    }
    return $retour;
}

function estJF($date)
{
    $jour = intval($date->format('d'));
    $mois = intval($date->format('m'));

    if (
        ($jour == 1 && $mois == 1) ||
        ($jour == 1 && $mois == 5) ||
        ($jour == 8 && $mois == 5) ||
        ($jour == 14 && $mois == 7) ||
        ($jour == 15 && $mois == 8) ||
        ($jour == 1 && $mois == 11) ||
        ($jour == 11 && $mois == 11) ||
        ($jour == 25 && $mois == 12)
    ) {
        return true;
    }

    $an = intval($date->format('Y'));
    $mars21 = new DateTimeImmutable($an . '-03-21', new DateTimeZone('Europe/Paris'));
    $paques = $mars21->add(new DateInterval('P' . easter_days($an) . 'D'));
    $lundiPaques = $paques->add(new DateInterval('P1D'));
    $ascemption = $paques->add(new DateInterval('P39D'));
    $pentecote = $paques->add(new DateInterval('P50D'));
    $lundiPentecote = $paques->add(new DateInterval('P51D'));
    if (
        ($jour == intval($paques->format('d')) && $mois == intval($paques->format('m'))) ||
        ($jour == intval($lundiPaques->format('d')) && $mois == intval($lundiPaques->format('m'))) ||
        ($jour == intval($ascemption->format('d')) && $mois == intval($ascemption->format('m'))) ||
        ($jour == intval($pentecote->format('d')) && $mois == intval($pentecote->format('m'))) ||
        false//($jour == intval($lundiPentecote->format('d')) && $mois == intval($lundiPentecote->format('m')))
    ) {
        return true;
    }

    return false;
}

function getTypeJour(string $date): string
{
    $jour = new DateTime($date, new \DateTimeZone('Europe/Paris'));
    $dayOfWeek = intval($jour->format('N'));
    $cle = 'job';
    if ($dayOfWeek == 7 || estJF($jour)) //Dimanche ou JF
    {
        $cle = 'djf';
    } elseif ($dayOfWeek == 6) //Samedi
    {
        $cle = 'sam';
    }

    return $cle;
}
