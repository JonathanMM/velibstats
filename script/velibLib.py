import urllib.request
import json
import os
import pymysql
from datetime import datetime
from config import getMysqlConnection
from adresseLib import getAdresse, getInsee
from gbfsLib import getStationIdByLatLong
import hashlib
import time
import ssl
import requests


def val(valeur):
    if valeur is None:
        return 'NULL'
    if valeur is True:
        return '1'
    if valeur is False:
        return '0'
    return '"'+str(valeur)+'"'


def fichierEnBinaire(file):
    with file:
        return file.read()


def downloadData():
    url = "https://velib-metropole-opendata.smovengo.cloud/opendata/Velib_Metropole/station_status.json"

    results = requests.get(url,
                           headers={'Content-Type': 'application/json'})

    return results.json()


def getContentDataStation():
    url = "https://velib-metropole-opendata.smovengo.cloud/opendata/Velib_Metropole/station_information.json"

    results = requests.get(url,
                           headers={'Content-Type': 'application/json'})

    return results.json()


def getAllStation():
    data = downloadData()

    # if fileName is None:  # On n'a trouvé de valeurs correctes :(
    #     print("Impossible de récupérer les données")
    #     return False

    mysql = getMysqlConnection()

    # On récupère la liste des stations déjà en base
    requete = mysql.cursor()
    requete.execute('SELECT code, dateOuverture FROM stations')
    rep = requete.fetchall()
    requete.close()
    aujourdhui = datetime.today().date()
    stations = []
    stationsFutur = []
    for row in rep:
        stations.append(row[0])
        if(row[1] is None or row[1] > aujourdhui):
            stationsFutur.append(row[0])

    nbTotalBike = 0
    nbTotalEBike = 0
    nbTotalFreeEDock = 0
    nbTotalEDock = 0
    nbTotalBikeOverflow = 0
    nbTotalEBikeOverflow = 0

    # On créer une conso pour avoir son id
    requete = mysql.cursor()
    requete.execute('INSERT INTO statusConso (date) VALUES (NOW())')
    mysql.commit()
    idConso = requete.lastrowid
    requete.close()
    strIdConso = str(idConso)
    nbStationsOuvertes = 0
    nbStationsOuvertesDetectees = 0

    dataStationFromFile = getContentDataStation()

    for etatStation in data['data']['stations']:
        try:
            codeStation = int(etatStation['stationCode'])
        except Exception:
            codeStation = 0

        print("Station : " + str(codeStation))

        if codeStation > 100:
            stationCherche = None
            for stationData in dataStationFromFile['data']['stations']:
                codeStationFile = 0
                try:
                    codeStationFile = int(stationData['stationCode'])
                except Exception:
                    continue

                if codeStationFile > 0 and codeStationFile == codeStation:
                    stationCherche = stationData
                    break

            if codeStation not in stations and stationCherche != None:
                longitude = stationCherche['lon']
                latitude = stationCherche['lat']
                strDateOuverture = 'CURDATE()'
                # On récupère également le stationId des données GTBS
                stationId = stationCherche['station_id']

                requete = mysql.cursor()
                requete.execute('INSERT INTO stations (code, name, longitude, latitude, type, dateOuverture, adresse, insee, stationId) VALUES \
                    ('+val(codeStation)+', '+val(stationCherche['name'])+', '+val(longitude)+', '+val(latitude)+', '+val("Unknown")+', '+strDateOuverture+', '+val(getAdresse(latitude, longitude))+', '+val(getInsee(codeStation))+', '+val(stationId)+')')
                mysql.commit()
                requete.close()
                stations.append(codeStation)

            bikesAvailable = etatStation['num_bikes_available_types']
            nbBike = 0
            nbEbike = 0
            for etatBikesAvailable in bikesAvailable:
                if 'mechanical' in etatBikesAvailable:
                    nbBike = int(etatBikesAvailable['mechanical'])
                if 'ebike' in etatBikesAvailable:
                    nbEbike = int(etatBikesAvailable['ebike'])
            nbFreeEDock = int(etatStation['num_docks_available'])
            # nbEDock = int(etatStation['numDocksAvailable'])
            if stationCherche != None:
                nbEDock = int(stationCherche['capacity'])
            else:
                nbEDock = nbFreeEDock
            nbBikeOverflow = 0
            nbEBikeOverflow = 0
            state = "Operative"
            requete = mysql.cursor()
            requete.execute('INSERT INTO status (code, idConso, state, nbBike, nbEBike, nbFreeEDock, nbEDock, nbBikeOverflow, nbEBikeOverflow, maxBikeOverflow, overflow, overflowActivation) VALUES \
            ('+val(codeStation)+', '+val(strIdConso)+', '+val(state)+', '+val(nbBike)+', '+val(nbEbike)+', '+val(nbFreeEDock)+', '+val(nbEDock)+', '+val(nbBikeOverflow)+', '+val(nbEBikeOverflow)+', '+val(0)+', '+val(0)+', '+val(0)+')')
            mysql.commit()
            requete.close()

            # On met à jour la station au besoin
            if codeStation in stationsFutur and nbEDock > 0:
                requete = mysql.cursor()
                requete.execute('UPDATE stations \
                SET dateOuverture = CURDATE() WHERE code = '+val(codeStation))
                mysql.commit()
                requete.close()

            # On ajoute a la conso
            nbTotalBike += nbBike
            nbTotalEBike += nbEbike
            nbTotalFreeEDock += nbFreeEDock
            nbTotalEDock += nbEDock
            nbTotalBikeOverflow += nbBikeOverflow
            nbTotalEBikeOverflow += nbEBikeOverflow
            if state == "Operative":
                nbStationsOuvertes += 1
            if nbEDock > 0 and nbBike + nbEbike + nbFreeEDock > 0:
                nbStationsOuvertesDetectees += 1

    # On insert tout dans le statut
    requete = mysql.cursor()
    requete.execute('UPDATE statusConso SET \
    nbStation = '+str(nbStationsOuvertes)+', \
    nbStationDetecte = '+str(nbStationsOuvertesDetectees)+' ,\
    nbBike = '+str(nbTotalBike)+', \
    nbEbike = '+str(nbTotalEBike)+', \
    nbBikeOverflow = '+str(nbTotalBikeOverflow)+', \
    nbEbikeOverflow = '+str(nbTotalEBikeOverflow)+', \
    nbEDock = '+str(nbTotalEDock)+', \
    nbFreeEDock = '+str(nbTotalFreeEDock)+' \
    WHERE id = '+strIdConso)
    mysql.commit()
    requete.close()
    mysql.close()
