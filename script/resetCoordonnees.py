from adresseLib import getAdresse
import pymysql
import urllib
import hashlib
import os
import time
import json
from config import getMysqlConnection


def val(valeur):
    if valeur is None:
        return 'NULL'
    return '"'+str(valeur)+'"'


def fichierEnBinaire(file):
    with file:
        return file.read()


def resetCoordonnees():
    mysql = getMysqlConnection()
    requete = mysql.cursor()

    urlVelib = ""
    tmpFileName = 'detailsStations.json'
    nbEssai = 0
    fichierOk = False
    while nbEssai < 3 and not fichierOk:
        urllib.request.urlretrieve(urlVelib, tmpFileName)
        dataBrut = open(tmpFileName)

        # On regarde si ce n'est pas le md5 corrompu !
        valeurMD5 = hashlib.md5(fichierEnBinaire(
            open(tmpFileName, 'rb'))).hexdigest()
        fichierOk = valeurMD5 != 'c746fec461216aa3a67602b879dafa6c'
        nbEssai += 1
        if not fichierOk:
            os.remove(tmpFileName)
            time.sleep(10)

    if not fichierOk:  # On n'a trouvé de valeurs correctes :(
        return False

    data = json.load(dataBrut)
    for etatStation in data:
        infoStation = etatStation['station']

        try:
            codeStation = int(infoStation['code'])
        except Exception:
            codeStation = 0

        if codeStation > 100:
            longitude = infoStation['gps']['longitude']
            latitude = infoStation['gps']['latitude']
            adresse = getAdresse(latitude, longitude)
            requete = mysql.cursor()
            requete.execute('UPDATE stations SET longitude = '+val(longitude)+', latitude = '+val(
                latitude)+', adresse = '+val(adresse)+' WHERE code = '+val(codeStation))

    mysql.close()


resetCoordonnees()
