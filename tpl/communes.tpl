{include file='tpl/header.tpl'}
<header class="header">
    <h1>Vélib Nocle</h1> 
    <h2>site non officiel</h2>
</header>
<div id="content">
    <h2 class="content-subhead">Filtrer par commune</h2>
        <div class="liste-ville-area">
        {foreach $departements as $dept}
            <div class="liste-ville-dept">
                <h3>{$dept.nom} ({$dept.numero})</h3>
                <ul>
                {foreach $dept.communes as $commune}
                    <li><a href="commune.php?insee={$commune.insee}">{$commune.nom_complet}</a></li>
                {/foreach}
                </ul>
            </div>
        {/foreach}
        </div>
    {include file="tpl/credits.tpl"}
</div>
{include file="tpl/footer.tpl"}