import pymysql
from datetime import datetime, timedelta, date
from config import getMysqlConnection


def nettoyerBarometreJour():

    mysql = getMysqlConnection()

    print("Début du nettoyage du baromètre")

    date = datetime.today()
    dateLimite = date.replace(microsecond=0, second=0, minute=0, hour=0)
    dateStr = dateLimite.strftime("%Y-%m-%d %H:%M:%S")

    requete = mysql.cursor()
    requete.execute(
        'DELETE FROM comptage_barometre WHERE date >= "'+dateStr+'"')
    mysql.commit()
    requete.close()

    print("Fin du nettoyage du baromètre")

    mysql.close()
