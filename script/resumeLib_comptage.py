import pymysql
import datetime
from config import getMysqlConnection
from toolsLib import val


def calculerResume(dateConsoDT, dureeConso, where=None):
    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConso)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    proprietes = ['nombre']
    mysql = getMysqlConnection()

    innerJoin = None

    if where is not None:
        innerJoin = 'INNER JOIN comptage_stations t ON t.code = s.code'

    requete = mysql.cursor()
    requete.execute("SELECT s.code, s."+', s.'.join(proprietes)+" \
    FROM comptage_status s \
    INNER JOIN `comptage_statusConso` c ON c.id = s.idConso " + ('' if innerJoin is None else innerJoin) + " \
    WHERE (c.`date` >= '"+dateConso+"' AND c.`date` < '"+dateConsoFin+"' " + ('' if where is None else ' AND ' + where) + ") \
    ORDER BY c.id ASC")
    statusConso = requete.fetchall()
    bikeList = {}

    for row in statusConso:
        codeStation = row[0]
        for i, cle in enumerate(proprietes):
            valeurProp = int(row[i+1])

            if codeStation not in bikeList:
                bikeList[codeStation] = {cle: valeurProp}
            else:
                bikeList[codeStation][cle] += valeurProp

    for codeStation in bikeList:
        valeurs = bikeList[codeStation]
        requete = mysql.cursor()
        requete.execute('INSERT INTO `comptage_resumeStatus` (`id`, `code`, `date`, `duree`, `nombre`) VALUES \
        (NULL, '+val(codeStation)+', '+val(dateConso)+', '+val(dureeConso)+', '+val(valeurs['nombre'])+')')


def calculerResumeOfResume(dateConsoDT, dureeConsoOrigine, dureeConsoFinale, where=None):
    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConsoFinale)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    # On récupère les conso
    proprietes = ['nombre']

    innerJoin = None

    if where is not None:
        innerJoin = 'INNER JOIN comptage_stations t ON t.code = s.code'

    mysql = getMysqlConnection()
    requete = mysql.cursor()
    requete.execute("SELECT s.code, s."+', s.'.join(proprietes)+" FROM comptage_resumeStatus s " + ('' if innerJoin is None else innerJoin) + " \
        WHERE s.duree = "+str(
        dureeConsoOrigine)+" and s.date >= '"+dateConso+"' and s.date < '"+dateConsoFin+"' " + ('' if where is None else ' AND ' + where) + " order by s.code")
    consos = requete.fetchall()
    precedentCode = None
    infosCourantes = {}
    for row in consos:
        codeStation = row[0]

        if precedentCode != None and precedentCode != codeStation:  # On change de station
            # On enregistre la conso
            enregistrerConso(mysql, proprietes, infosCourantes,
                             precedentCode, dateConso, dureeConsoFinale)
            infosCourantes = {}

        for i, cle in enumerate(proprietes):
            valeurProp = int(row[i+1])
            if cle not in infosCourantes:
                infosCourantes[cle] = valeurProp
            else:
                infosCourantes[cle] += valeurProp

        precedentCode = codeStation
    # Et on oublie pas le dernier !
    enregistrerConso(mysql, proprietes, infosCourantes,
                     precedentCode, dateConso, dureeConsoFinale)


def enregistrerConso(mysql, proprietes, infosCourantes, codeStation, dateConso, dureeConso):
    # On traite les moyennes et on prépare la requête
    if infosCourantes != {}:
        strValeurs = ""
        for i, cle in enumerate(proprietes):
            if i != 0:
                strValeurs += ', '
            strValeurs += val(infosCourantes[cle])

        requete = mysql.cursor()
        requete.execute('INSERT INTO `comptage_resumeStatus` (`id`, `code`, `date`, `duree`, '+', '.join(proprietes)+') VALUES \
        (NULL, '+val(codeStation)+', '+val(dateConso)+', '+val(dureeConso)+', '+strValeurs+')')


def debuterCalculResume(periode, dateCourante=datetime.datetime.now(), where=None):
    print("Début de calcul du résumé : " +
          str(dateCourante) + " | Période : " + str(periode))
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periode)
    debutPeriodeMinute = dateConsoDT.minute % periode
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0, minute=dateConsoDT.minute - debutPeriodeMinute)
    calculerResume(dateConsoDT, periode, where)


def debuterCalculResumeOfResume(periodeOrigine, periodeFinale, dateCourante=datetime.datetime.now(), where=None):
    print("Début de calcul du résumé du résumé : " + str(dateCourante) +
          " | Période : " + str(periodeOrigine) + " > " + str(periodeFinale))
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periodeFinale)
    debutPeriodeMinute = (dateConsoDT.hour * 60 +
                          dateConsoDT.minute) % periodeFinale
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0) - datetime.timedelta(minutes=debutPeriodeMinute)
    calculerResumeOfResume(dateConsoDT, periodeOrigine, periodeFinale, where)

# StatutConso


def calculerResumeConso(dateConsoDT, dureeConso):
    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConso)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    proprietes = ['nbStation', 'nombre']
    mysql = getMysqlConnection()

    requete = mysql.cursor()
    requete.execute("SELECT "+', '.join(proprietes)+" \
    FROM comptage_statusConso \
    WHERE (`date` >= '"+dateConso+"' AND `date` < '"+dateConsoFin+"') \
    ORDER BY id ASC")
    statusConso = requete.fetchall()
    bikeList = {}
    for row in statusConso:
        for i, cle in enumerate(proprietes):
            if not row[i] is None:
                valeurProp = int(row[i])
                if cle in ['nbStation']:
                    if not cle in bikeList:
                        bikeList[cle] = valeurProp
                    else:
                        bikeList[cle] = max(bikeList[cle], valeurProp)
                else:
                    if not cle in bikeList:
                        bikeList[cle] = valeurProp
                    else:
                        bikeList[cle] += valeurProp

    if len(bikeList) > 1:  # S'il y a plus que simplement le nombre de stations
        valeurs = bikeList
        requete = mysql.cursor()
        requete.execute('INSERT INTO `comptage_resumeConso` (`id`, `date`, `duree`, `nbStation`, nombre) VALUES \
        (NULL, '+val(dateConso)+', '+val(dureeConso)+', '+val(valeurs['nbStation'])+', '+val(valeurs['nombre'])+')')


def calculerResumeOfResumeConso(dateConsoDT, dureeConsoOrigine, dureeConsoFinale):
    dateConso = dateConsoDT.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateConsoDT + datetime.timedelta(minutes=dureeConsoFinale)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    # On récupère les conso
    proprietes = ['nbStation', 'nombre']

    mysql = getMysqlConnection()
    requete = mysql.cursor()
    requete.execute("SELECT "+', '.join(proprietes)+" FROM `comptage_resumeConso` WHERE duree = "+str(
        dureeConsoOrigine)+" and date >= '"+dateConso+"' and date < '"+dateConsoFin+"' order by date")
    consos = requete.fetchall()
    infosCourantes = {}
    for row in consos:
        for i, cle in enumerate(proprietes):
            valeurProp = int(row[i])

            if cle in ['nbStation']:
                if cle in infosCourantes:
                    infosCourantes[cle] = max(infosCourantes[cle], valeurProp)
                else:
                    infosCourantes[cle] = valeurProp
            else:
                if cle in infosCourantes:
                    infosCourantes[cle] += valeurProp
                else:
                    infosCourantes[cle] = valeurProp

    # Et on enregistre
    if infosCourantes != {}:
        strValeurs = ""
        for i, cle in enumerate(proprietes):
            if i != 0:
                strValeurs += ', '
            strValeurs += val(infosCourantes[cle])

        requete = mysql.cursor()
        requete.execute('INSERT INTO `comptage_resumeConso` (`id`, `date`, `duree`, '+', '.join(proprietes)+') VALUES \
        (NULL, '+val(dateConso)+', '+val(dureeConsoFinale)+', '+strValeurs+')')


def debuterCalculResumeConso(periode, dateCourante=datetime.datetime.now()):
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periode)
    debutPeriodeMinute = dateConsoDT.minute % periode
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0, minute=dateConsoDT.minute - debutPeriodeMinute)
    calculerResumeConso(dateConsoDT, periode)


def debuterCalculResumeOfResumeConso(periodeOrigine, periodeFinale, dateCourante=datetime.datetime.now(), where=None):
    dateConsoDT = dateCourante - datetime.timedelta(minutes=periodeFinale)
    debutPeriodeMinute = (dateConsoDT.hour * 60 +
                          dateConsoDT.minute) % periodeFinale
    dateConsoDT = dateConsoDT.replace(
        microsecond=0, second=0) - datetime.timedelta(minutes=debutPeriodeMinute)
    calculerResumeOfResumeConso(
        dateConsoDT, periodeOrigine, periodeFinale)


def calculerToutResume(dateMin, dateMax, where=None, consoACalculer=True):
    periodes = [360, 1440]
    periodePrecedente = 0
    for periode in periodes:
        dateCourante = dateMin
        print("-> Début période " + str(periode))
        while dateCourante + datetime.timedelta(minutes=periode) <= dateMax:
            print("Période " + str(periode)+'; date = '+str(dateCourante))
            # on met la date courante à la fin de la période, car, ça calcule à rebours
            dateCourante = dateCourante + datetime.timedelta(minutes=periode)
            if periode == 360:
                debuterCalculResume(periode, dateCourante, where)
                if consoACalculer:
                    debuterCalculResumeConso(periode, dateCourante)
            else:
                debuterCalculResumeOfResume(
                    periodePrecedente, periode, dateCourante, where)
                if consoACalculer:
                    debuterCalculResumeOfResumeConso(
                        periodePrecedente, periode, dateCourante)
        periodePrecedente = periode
        print("-> Fin période " + str(periode))
