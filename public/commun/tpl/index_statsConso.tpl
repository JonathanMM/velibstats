<div id="statsConso">
    <div class="statsBox nombreStations">
        <span class="nombre">{$nbStation}</span><br />
        stations ouvertes
    </div>
    <div class="statsBox veloMecanique">
        <span class="nombre">{$nbBike}</span><br />
        vélos mécaniques
    </div>
    {if $hasEbike}
    <div class="statsBox veloElectrique">
        <span class="nombre">{$nbEbike}</span><br />
        vélos électriques
    </div>
    {/if}
</div>
<ul id="statsPlus">
    <li>Stations détectées : {$nbStationDetecte}</li>
    {if $hasOverflow}<li>Vélos en overflow : {$nbOverflow}</li>{/if}
    <li>Bornes libres : {$nbFreeEDock}</li>
    <li>Bornes totales : {$nbEDock}</li>
</ul>
{* <div id="statsJournaliere">
    <div class="statsBox">
        Au moins<br />
        <span class="nombre">{$statsNombre}</span><br />
        locations le {$statsDate}
    </div>
</div> *}
<i>Dernière mise à jour : {$dateDerniereConso}</i>