from statsLib import calculerStatsJournaliere
import datetime

date = datetime.datetime.today().replace(hour=0, minute=0, second=0,
                                         microsecond=0) - datetime.timedelta(days=1)
calculerStatsJournaliere(date)
