<?php
require_once 'config.php';
include_once 'functions.php';
require_once 'libs/Smarty.class.php';

$smarty = new Smarty();

$smarty->assign(array(
    'pageId' => 'accueil',
));

//Filtre 24 heures
$hier = new DateTime("-1day");
$filtreDate = $hier->format('Y-m-d H:i:s');

//Dernière conso
$requete = $pdo->query('SELECT * FROM `statusConso` where nbStation Is not null and date >= "' . $filtreDate . '" Order by id desc limit 0,1');
$conso = $requete->fetch();

//Filtre 7 jours
$semaineDerniere = new DateTime("-7day");
$filtreDateSemaineDerniere = $semaineDerniere->format('Y-m-d H:i:s');

//Stats journalières
$requete = $pdo->query('SELECT * FROM statsJournaliere WHERE date >= "' . $filtreDateSemaineDerniere . '" AND nbJour = 1 ORDER BY date DESC');
$derniereStat = $requete->fetch();

$smarty->assign(array(
    'idConso' => $conso['id'],
    'nbStation' => $conso['nbStation'],
    'nbStationDetecte' => $conso['nbStationDetecte'],
    'nbBike' => $conso['nbBike'],
    'nbEbike' => $conso['nbEbike'],
    'nbOverflow' => $conso['nbBikeOverflow'] + $conso['nbEbikeOverflow'],
    'nbEDock' => $conso['nbEDock'],
    'nbFreeEDock' => $conso['nbFreeEDock'],
    'dateDerniereConso' => (new DateTime($conso['date']))->format('d/m/Y à H:i'),
    'statsDate' => (new DateTime($derniereStat['date']))->format('d/m/Y'),
    'statsNombre' => min($derniereStat['nbBikePris'] + $derniereStat['nbEBikePris'], $derniereStat['nbBikeRendu'] + $derniereStat['nbEBikeRendu']),
));

$smarty->display('tpl/index.tpl');
exit();
