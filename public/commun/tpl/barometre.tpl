{include file='./header.tpl'}
<header class="header">
    <h1>Vélib Nocle</h1> 
    <h2>site non officiel</h2>
</header>
<div id="content">
    Baromètre issu du site velib.nocle.fr

<script type="application/javascript" src="js/Moment.min.js"></script>
<script type="application/javascript" src="js/Chart.min.js"></script>
    <script type="application/javascript" src="js/jquery-3.2.1.min.js"></script>
<canvas id="chart" height="700px" width="1000px"></canvas>
<script>
$(document).ready( function () {
		var inputs = {
			min: -100,
			max: 100,
			count: 8,
			decimals: 2,
			continuity: 1
		};

		function generateLabels(config) {
			return [].concat(...[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ,17 ,18 ,19 ,20, 21, 22, 23].map(h => ["00", "05", 10, 15, 20, 25, 30, 35, 40, 45, 50, 55].map(m => h + 'h' + m)));
		}

		var options = {
            responsive: false,
			maintainAspectRatio: false,
			spanGaps: true,
			elements: {
				line: {
					tension: 0.4
				}
			},
			plugins: {
				filler: {
					propagate: false
				}
			},
			scales: {
				xAxes: [{
					ticks: {
						autoSkip: true,
						maxRotation: 0,
                        autoSkipPadding: 8
					}
				}],
                yAxes: {
                    stacked: false
                }
            }
		};

        new Chart('chart', {
            type: 'line',
            data: {
                labels: generateLabels(),
                datasets: [{
                    backgroundColor: '#b3ee96',
                    data: {$centile20},
                    label: 'Faible',
                    fill: 'start',
                    order: 70
                },{
                    borderColor: 'rgb(76, 213, 233)',
                    data: {$centile50},
                    label: 'Médiane',
                    order: 10,
                    fill: false
                },{
                    backgroundColor: '#eee096',
                    data: {$centile80},
                    label: 'Normal',
                    fill: 'start',
                    order: 80
                },{
                    backgroundColor: '#eec396',
                    data: {$centile90},
                    label: 'Élevé',
                    fill: 'start',
                    order: 90
                },{
                    backgroundColor: '#ee9696',
                    data: {$centile95},
                    label: 'Exceptionnel',
                    fill: 'start',
                    order: 100
                },{
                    borderColor: 'rgb(255, 0, 0)',
                    data: {$centile100},
                    label: 'Record',
                    fill: false,
                    order: 20
                }]
            },
            options: Chart.helpers.merge(options, {
                title: {
                    text: 'Baromètre de la circulation',
                    display: true
                }
            })
        });
});
</script>
    {include file="./credits.tpl"}
</div>
{include file="./footer.tpl"}