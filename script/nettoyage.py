import datetime
from nettoyageLib import nettoyerInstantanne, nettoyerConso, optimiserBDD

date = datetime.datetime.today() - datetime.timedelta(days=2)  # Temporalité 15 Min
dateLimite = date.replace(microsecond=0, second=0, minute=0, hour=0)
nettoyerInstantanne(dateLimite)
nettoyerConso(dateLimite, "duree = 15")

date = datetime.datetime.today() - datetime.timedelta(days=10)  # Temporalité 5 Min
dateLimite = date.replace(microsecond=0, second=0,
                          minute=0, hour=0)  # 10 jours car baromètre
nettoyerConso(dateLimite, "duree = 5")

dateHuitJours = datetime.datetime.today() - datetime.timedelta(days=8)
dateLimiteHuitJours = dateHuitJours.replace(
    microsecond=0, second=0, minute=0, hour=0)
nettoyerConso(dateLimiteHuitJours, "duree = 60")
optimiserBDD()
