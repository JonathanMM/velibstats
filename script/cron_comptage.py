import comptageLib
from resumeLib_comptage import debuterCalculResume, debuterCalculResumeConso, debuterCalculResumeOfResume, debuterCalculResumeOfResumeConso
import datetime
import barometreLib

# Est-ce qu'on doit faire un truc ?
if not comptageLib.ifHasUpdate():
    print("Pas besoin de mise à jour")
    quit()

print("Mise à jour nécessaire")

# On nettoie sur les trois derniers jours
comptageLib.nettoyerRecent()

# On gère la table de comptage
comptageLib.getAllStation()

# Et les résumes, sur trois jours
auj = datetime.datetime.combine(
    datetime.date.today(), datetime.datetime.min.time())
date = auj - datetime.timedelta(days=3)
while date < auj:
    date += datetime.timedelta(hours=6)
    for i in range(0, 4):
        debuterCalculResume(360, date)
        debuterCalculResumeConso(360, date)
        date += datetime.timedelta(hours=6)

    date -= datetime.timedelta(hours=6)
    debuterCalculResumeOfResume(360, 1440, date)
    debuterCalculResumeOfResumeConso(360, 1440, date)

comptageLib.mettreAJourRecord(auj - datetime.timedelta(days=3))

barometreLib.nettoyerBarometreJour()
