import urllib.request
import json
import os
import pymysql
from datetime import datetime, timedelta, date
from config import getMysqlConnection
import hashlib
import time
import ssl
import csv
import pytz
from toolsLib import val

cleDateDerniereMaj = "comptage-derniere-maj"


def fichierEnBinaire(file):
    with file:
        return file.read()


def getAllStation(urlComptage=None):
    mysql = getMysqlConnection()

    # On récupère la liste des stations déjà en base
    requete = mysql.cursor()
    requete.execute(
        'SELECT code, dateOuverture FROM comptage_stations')
    rep = requete.fetchall()
    requete.close()
    aujourdhui = datetime.today().date()
    stations = []
    stationsFutur = []
    checkMaxConso = True
    for row in rep:
        codeStation = int(row[0])
        stations.append(codeStation)
        if(row[1] is None or row[1] > aujourdhui):
            stationsFutur.append(codeStation)

    if urlComptage == None:
        # urlComptage = "https://parisdata.opendatasoft.com/explore/dataset/comptage-velo-donnees-compteurs/download/?format=csv&timezone=Europe/Paris&use_labels_for_header=true&csv_separator=%3B" # Tout, sans filtre
        urlDebut = "https://opendata.paris.fr/explore/dataset/comptage-velo-donnees-compteurs/download/?format=csv&disjunctive.id=true&disjunctive.name=true&timezone=Europe/Paris&use_labels_for_header=true&csv_separator=%3B"
        borneBasse = datetime.combine(
            date.today(), datetime.min.time()) - timedelta(days=12)
        dateDebut = borneBasse.strftime("%Y-%m-%dT%H:%M:%SZ")
        urlComptage = urlDebut + "&q=date%3E%3D%22" + dateDebut + "%22"
    else:  # On a donné une URL, c'est qu'on sait ce qu'on fait
        checkMaxConso = False

    # On aura aussi besoin de la date de dernière conso en base, pour n'importer que les nouvelles
    maxConso = None
    if checkMaxConso:
        requete = mysql.cursor()
        requete.execute('SELECT MAX(date) FROM `comptage_statusConso`')
        rep = requete.fetchone()
        requete.close()
        if rep[0] != None:
            timezone = pytz.timezone("Europe/Paris")
            maxConso = timezone.localize(rep[0])
            print("Dernière conso en base : " +
                  maxConso.strftime("%Y-%m-%d %Hh"))

    listeConso = {}
    passageComptes = []

    print("URL : " + urlComptage)
    print("Téléchargement du fichier de comptage")
    tmpFileName = 'comptage.csv'

    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    with urllib.request.urlopen(urlComptage, context=ctx) as u, \
            open(tmpFileName, 'wb') as f:
        f.write(u.read())

    print("Téléchargement du fichier de comptage terminé")

    print("Début de l'importation")

    with open(tmpFileName, newline='') as csvfile:
        data = csv.reader(csvfile, delimiter=';')
        pos = None
        stationsFusionData = []
        for etatStation in data:
            if pos is None:
                print("> Lecture de l'entête")
                # Entête
                i = 0
                pos = {}
                for valeur in etatStation:
                    pos[valeur] = i
                    i += 1
                del i
                del valeur
                continue

            print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>")
            print("> Nouvelle ligne")

            nbPassage = None
            try:
                nbPassage = int(float(etatStation[pos['Comptage horaire']]))
            except Exception:
                print(">> Impossible de convertir le nombre de passage en nombre")
                continue

            # Pour éviter les bugs, si la date fini par +01:00, on remplace par +0100
            dateConsoStr = etatStation[pos['Date et heure de comptage']].replace(
                "+01:00", "+0100").replace("+02:00", "+0200")
            dateConso = datetime.strptime(dateConsoStr, "%Y-%m-%dT%H:%M:%S%z")
            if maxConso != None and dateConso <= maxConso:
                print(">> Relevé de comptage hors périmètre")
                continue

            codeStation = None
            codeParent = None
            nomStation = None
            nomParent = None
            try:
                codeCompteur = etatStation[pos['Identifiant du compteur']]
                codeInfos = codeCompteur.split('-')
                codeSiteComptage = codeInfos[0]
                codeChannel = codeInfos[1]
                if codeChannel == "SC":  # Pas de regroupement utile, on utilise l'identifiant du site de comptage
                    codeStation = int(codeSiteComptage)
                    nomStation = etatStation[pos['Nom du site de comptage']]
                else:  # Regroupement utile
                    codeStation = int(codeChannel)
                    nomStation = etatStation[pos['Nom du compteur']]
                    codeParent = int(codeSiteComptage)
                    nomParent = etatStation[pos['Nom du site de comptage']]
            except Exception:
                print(
                    ">> Impossible de récupérer l'identifiant du compteur")
                continue

            print("> Station : " + str(codeStation) + " (" + nomStation + ")")

            if codeStation not in stations:
                print("> Nouveau compteur détecté")
                if len(nomStation) == 0:
                    print(">> Le nom du compteur est vide")
                    continue
                coordonnees = etatStation[pos['Coordonnées géographiques']].split(
                    ',')
                latitude = float(coordonnees[0])
                longitude = float(coordonnees[1])
                strDateOuverture = str(
                    etatStation[pos['Date d\'installation du site de comptage']])

                plusInsert = ""
                plusValues = ""
                if codeParent != None:  # Il a un parent
                    plusInsert = ", parent"
                    plusValues = ", "+val(codeParent)

                requete = mysql.cursor()
                requete.execute('INSERT INTO comptage_stations (code, nom, longitude, latitude, dateOuverture'+plusInsert+') VALUES \
                                ('+val(codeStation)+', '+val(nomStation)+', '+val(longitude)+', '+val(latitude)+', '+val(strDateOuverture)+plusValues+')')
                mysql.commit()
                requete.close()
                stations.append(codeStation)
                print("> compteur ajouté")

            # Il nous faut la conso
            strDateConso = dateConso.strftime("%Y-%m-%d %H:%M:%S")

            identifiantStatut = strDateConso + "#" + str(codeStation)

            if identifiantStatut in passageComptes:  # On a déjà pris en compte ce comptage
                print(">> Ligne déjà traitée")
                continue
            else:
                passageComptes.append(identifiantStatut)

            idConso = None

            if strDateConso not in listeConso.keys():
                print("> Nouvelle conso")
                requete = mysql.cursor()
                requete.execute(
                    'INSERT INTO comptage_statusConso (date) VALUES ('+val(strDateConso)+')')
                mysql.commit()
                idConso = requete.lastrowid
                requete.close()
                listeConso[strDateConso] = idConso
            else:
                idConso = listeConso[strDateConso]

            requete = mysql.cursor()
            requete.execute('INSERT INTO comptage_status (code, idConso, nombre) VALUES \
            ('+val(codeStation)+', '+val(idConso)+', '+val(nbPassage)+')')
            mysql.commit()
            requete.close()

            # On va gérer les stations fusions
            if codeParent != None:
                print("> Station faisant partie d'une station fusion")
                print("> Parent : " + str(codeParent) + " (" + nomParent + ")")

                requete = mysql.cursor()

                if codeParent not in stations:  # Nouveau parent
                    requete.execute('INSERT INTO comptage_stations (code, nom, longitude, latitude, dateOuverture, type) VALUES \
                    ('+val(codeParent)+', '+val(nomParent)+', '+val(longitude)+', '+val(latitude)+', '+val(strDateOuverture)+', 2)')
                    stations.append(codeParent)

                # On va quand même référencer le parent dans la station (utile uniquemnet en cas de mise à jour)
                requete.execute('UPDATE comptage_stations SET parent = ' +
                                val(codeParent)+' WHERE code = '+val(codeStation))

                idConsoStationFusion = str(idConso) + '-' + str(codeParent)

                if idConsoStationFusion not in stationsFusionData:
                    stationsFusionData.append(idConsoStationFusion)
                    requete.execute('INSERT INTO comptage_status (code, idConso, nombre) VALUES \
                            ('+val(codeParent)+', '+val(idConso)+', '+val(0)+')')

                requete.execute('UPDATE comptage_status \
                SET nombre = nombre + '+val(nbPassage)+' WHERE idConso = '+val(idConso)+' and code = '+val(codeParent))
                mysql.commit()
                requete.close()

            # On met à jour la conso
            requete = mysql.cursor()
            requete.execute('UPDATE comptage_statusConso \
            SET nbStation = nbStation + 1, nombre = nombre + '+str(nbPassage)+' WHERE id = '+str(idConso))
            mysql.commit()
            requete.close()
            print("> Ligne bien prise en compte")

    print("Fin de l'importation")
    os.remove(tmpFileName)
    mysql.close()


def ifHasUpdate():
    mysql = getMysqlConnection()

    # On regarde la date de dernière mise à jour
    requete = mysql.cursor()
    requete.execute('SELECT valeur FROM `config` WHERE cle="' +
                    cleDateDerniereMaj+'"')
    rep = requete.fetchone()
    requete.close()

    dateDerniereMaj = None

    if rep != None:  # Si on a déjà une clé
        dateDerniereMaj = rep[0]

    print("Date de dernière mise à jour ici : " + str(dateDerniereMaj))

    urlApiMetaData = "https://opendata.paris.fr/api/v2/catalog/datasets/comptage-velo-donnees-compteurs?pretty=false&timezone=UTC&include_app_metas=false"

    # On regarde la date dans l'API
    ctx = ssl.create_default_context()
    ctx.check_hostname = False
    ctx.verify_mode = ssl.CERT_NONE

    needUpdate = False

    with urllib.request.urlopen(urlApiMetaData, context=ctx) as url:
        data = json.loads(url.read().decode())

        dateMetaData = str(data['dataset']['metas']
                           ['default']['data_processed'])

        print("Date de dernière mise à jour de l'API : " + dateMetaData)

        if dateDerniereMaj == None:  # On créé le record dans la table de config
            needUpdate = True
            requete = mysql.cursor()
            requete.execute(
                'INSERT INTO config (cle, valeur) VALUES ("'+cleDateDerniereMaj+'", "'+dateMetaData+'")')
            mysql.commit()
            requete.close()
        else:
            needUpdate = dateMetaData != dateDerniereMaj
            if needUpdate:
                requete = mysql.cursor()
                requete.execute(
                    'UPDATE config SET valeur = "'+dateMetaData+'" WHERE cle = "'+cleDateDerniereMaj+'"')
                mysql.commit()
                requete.close()

    mysql.close()
    return needUpdate


def nettoyerRecent():
    mysql = getMysqlConnection()

    print("Début du nettoyage")

    date = datetime.today() - timedelta(days=3)
    dateLimite = date.replace(microsecond=0, second=0, minute=0, hour=0)
    dateStr = dateLimite.strftime("%Y-%m-%d %H:%M:%S")

    requete = mysql.cursor()
    requete.execute(
        'DELETE FROM comptage_resumeConso WHERE date >= "'+dateStr+'"')
    mysql.commit()
    requete.close()

    requete = mysql.cursor()
    requete.execute(
        'DELETE FROM comptage_resumeStatus WHERE date >= "'+dateStr+'"')
    mysql.commit()
    requete.close()

    requete = mysql.cursor()
    requete.execute('DELETE FROM comptage_status WHERE idConso IN ( \
SELECT id FROM comptage_statusConso \
WHERE date >= "'+dateStr+'")')
    mysql.commit()
    requete.close()

    requete = mysql.cursor()
    requete.execute(
        'DELETE FROM comptage_statusConso WHERE date >= "'+dateStr+'"')
    mysql.commit()
    requete.close()

    print("Fin du nettoyage")

    mysql.close()


def nettoyerStatut(dateMin, dateMax, where=None):
    dateMinStr = dateMin.strftime("%Y-%m-%d %H:%M:%S")
    dateMaxStr = dateMax.strftime("%Y-%m-%d %H:%M:%S")

    mysql = getMysqlConnection()

    conditionWhere = 'date >= "'+dateMinStr+'" AND date < "' + dateMaxStr+'"'

    print("Début du nettoyage des statuts")

    requete = mysql.cursor()
    requete.execute('DELETE FROM comptage_status WHERE idConso IN ( \
SELECT id FROM comptage_statusConso \
WHERE '+conditionWhere+') ' + ('' if where is None else ' AND (' + where + ')'))
    mysql.commit()
    requete.close()

    requete = mysql.cursor()
    requete.execute(
        'DELETE FROM comptage_statusConso WHERE '+conditionWhere)
    mysql.commit()
    requete.close()

    print("Fin du nettoyage des statuts")


def nettoyerResume(dateMin, dateMax, where=None):
    dateMinStr = dateMin.strftime("%Y-%m-%d %H:%M:%S")
    dateMaxStr = dateMax.strftime("%Y-%m-%d %H:%M:%S")

    mysql = getMysqlConnection()

    conditionWhere = 'date >= "'+dateMinStr+'" AND date < "' + \
        dateMaxStr+'"' + ('' if where is None else ' AND (' + where + ')')

    print("Début du nettoyage des résumés")

    requete = mysql.cursor()
    requete.execute(
        'DELETE FROM comptage_resumeConso WHERE ' + conditionWhere)
    mysql.commit()
    requete.close()

    requete = mysql.cursor()
    requete.execute(
        'DELETE FROM comptage_resumeStatus WHERE ' + conditionWhere)
    mysql.commit()
    requete.close()

    print("Fin du nettoyage des résumés")

    mysql.close()


def calculerFusion(dateMin, dateMax):
    dateMinStr = dateMin.strftime("%Y-%m-%d %H:%M:%S")
    dateMaxStr = dateMax.strftime("%Y-%m-%d %H:%M:%S")

    mysql = getMysqlConnection()

    print("Début du calcul des fusions")

    stationsEnfant = {}

    # On récupère la liste des stations en base
    requete = mysql.cursor()
    requete.execute(
        'SELECT code, parent FROM comptage_stations WHERE type = 1 AND parent IS NOT NULL')
    rep = requete.fetchall()
    requete.close()
    stationsEnfant = {}
    for row in rep:
        codeStation = row[0]
        parent = row[1]
        stationsEnfant[codeStation] = parent

    # Puis, on va parcourir les conso qui vont bien, pour calculer nos statuts
    requete = mysql.cursor()
    requete.execute(
        'SELECT s.code, s.idConso, s.nombre FROM comptage_status s \
        INNER JOIN comptage_statusConso c ON c.id = s.idConso \
        WHERE s.code IN ('+",".join(str(c) for c in stationsEnfant.keys())+') AND c.date >= "' + dateMinStr + '" AND c.date <= "' + dateMaxStr + '" \
        ORDER BY s.idConso DESC, s.code ASC')
    statuts = requete.fetchall()
    requete.close()

    precedentIdConso = None
    currentStatus = []

    for statut in statuts:
        code = statut[0]
        parent = stationsEnfant[code]
        idConso = statut[1]
        nbPassage = statut[2]

        if precedentIdConso != idConso:  # On vide les currentStatuts
            print("Conso actuelle : "+str(idConso))
            precedentIdConso = idConso
            currentStatus = []

        requete = mysql.cursor()

        if parent not in currentStatus:
            currentStatus.append(parent)
            requete.execute('INSERT INTO comptage_status (code, idConso, nombre) VALUES \
                    ('+val(parent)+', '+val(idConso)+', '+val(0)+')')

        requete.execute('UPDATE comptage_status \
        SET nombre = nombre + '+val(nbPassage)+' WHERE idConso = '+val(idConso)+' and code = '+val(parent))
        mysql.commit()
        requete.close()

    print("Fin du calcul des fusions")

    mysql.close()


def mettreAJourRecord(dateMin):
    dateMinStr = dateMin.strftime("%Y-%m-%d")

    mysql = getMysqlConnection()
    # On récupère la liste des stations déjà en base
    requete = mysql.cursor()
    requete.execute('SELECT code FROM comptage_stations')
    rep = requete.fetchall()
    requete.close()
    stations = []
    for row in rep:
        codeStation = int(row[0])
        stations.append(codeStation)
    # Ainsi que les records. En faisant deux requêtes séparés, on pourra ainsi avoir les nouvelles stations
    requete = mysql.cursor()
    requete.execute('SELECT code, date, nombre FROM comptage_record')
    rep = requete.fetchall()
    requete.close()
    records = {}
    for row in rep:
        codeStation = int(row[0])
        #date = str(row[1])
        nombre = int(row[2])
        records[codeStation] = nombre

    # Pour chaque station, on va calculer son max
    for code in stations:
        requete = mysql.cursor()
        requete.execute(
            'SELECT date, nombre FROM comptage_resumeStatus r JOIN (SELECT MAX(nombre) AS nb FROM comptage_resumeStatus where code = '+str(code)+' AND date >= "'+dateMinStr+'" AND duree = 1440 order by date asc limit 0,1) max ON r.nombre = max.nb')
        rep = requete.fetchone()
        requete.close()
        if rep == None:
            continue
        dateRecord = str(rep[0])
        nombreRecord = rep[1]

        if code in records.keys():
            precedentRecord = records[code]
            if nombreRecord > precedentRecord:
                requete = mysql.cursor()
                requete.execute('UPDATE comptage_record SET date = '+val(dateRecord)+', nombre = '+val(nombreRecord)+' WHERE code = ' +
                                val(code))
                requete.close()
        else:  # Nouvelle station, nouveau record !
            requete = mysql.cursor()
            requete.execute('INSERT INTO comptage_record (code, date, nombre) VALUES (' +
                            val(code)+', '+val(dateRecord)+', '+val(nombreRecord)+')')
            requete.close()

    mysql.commit()
    mysql.close()
