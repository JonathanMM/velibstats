<?php
/**
 *    arguments :
 *    largeur_carte         :    nombre de pixels de largeur de l'image SVG à générer
 *    hauteur_carte         :    nombre de pixels de hauteur de l'image SVG à générer
 *    filtre                :    communes affichées :
 *                                        0            = tout (métropole gd Paris)
 *                                        01..99        = numéro de département
 *                                        100..999    = partie de dept (epic+100)
 *                                        1000..99999    = commune (numéro insee)
 *                                        tableau        = liste de numéros INSEE
 *                            le code insee et le nom sont ajoutés à la fin
 *    liens                :    début des liens des tracés de commune
 *    info                :    tableau associatif : numéro INSEE => texte infobulle
 *                                        (ne fonctionne que si lien existe)
 *    couleur                :    tableau associatif : numéro INSEE => couleur
 *    objets                :    liste d'objets ajoutés (tableaux associatifs)
 *
 *    arguments communs aux objets :
 *            nature        :    type d'objet à ajouter : point, texte, ...
 *            x            :    position horizontale en pixels
 *                                        >=0            = depuis le bord gauche
 *                                        <0            = depuis le bord droit
 *            y            :    position verticale en pixels
 *                                        >=0            = depuis le bord supérieur
 *                                        <            = depuis le bord inférieur
 *            lon            :    longitude de l'objet en degrés (ignoré si x existe)
 *                                        <0            = Ouest
 *                                        >0            = Est
 *            lat            :    latitude de l'objet en degrés (ignoré si y existe)
 *                                        <0            = Sud
 *                                        >0            = Nord
 *            taille        :    dimension de l'objet en pixels
 *            angle        :    rotation ed l'objet en degrés
 *            couleur        :    couleur de l'objet [#FF0000, red, hsl(0,100%,50%)
 *            lien        :    lien auquel en cas de clic sur l'objet
 *            info        :    texte infobulle (ne fonctionne que si lien existe)
 *
 *    arguments spécifiques aux objets de type "point" :
 *            point        :    forme un point : rond, carre, triangle
 *
 *    arguments spécifiques aux objets de type "texte" :
 *            texte        :    texte à afficher
 *            align        :    alignement du texte
 *                                        l            = gauche, par défaut)
 *                                        m            = milieu
 *                                        r            = droite
 *
 */

/*ÉTAT DE LA COMMUNE DANS LE SYNDICAT AUTOLIB VELIB METROPOLE*/
define('COMMUNE_AVM_NON', 0);
define('COMMUNE_AVM_AUTOLIB', 1);
define('COMMUNE_AVM_VELIB', 2);
define('COMMUNE_AVM_AUTOLIB_VELIB', 3);

function genererCarteSVG($largeur_carte = 800, $hauteur_carte = 600, $filtre = 0,
    $liens = '', $info = array(), $couleur = array(), $objets = array(), $avm = 0) {
    global $pdo;

    /****** FILTRAGE DES COMMUNES DEMANDEES ***************************************/

    //détermination de la liste des communes en fonction de l'argument "filtre"
    if (is_array($filtre)) {
        if (count($filtre)) {
            $liste_des_communes = implode(',', array_map('intval', $filtre));
            $where = ' WHERE insee IN (' . $liste_des_communes . ')';
        } else {
            $filtre == 0;
            $where = '';
        }
    } else if ($filtre == 0) {
        $where = '';
    } else if ($filtre < 100) {
        $where = ' WHERE dept="' . $filtre . '"';
    } else if ($filtre < 1000) {
        $where = ' WHERE etab+100="' . $filtre . '"';
    } else {
        $where = ' WHERE insee="' . $filtre . '"';
    }

    //récupération des coordonnées extremes de l'ensemble communes demandées
    //entiers en 1/10 000 000 °de degré (précision d'environ 1cm)
    //longitude : 180°W = -1 800 000 000 ; 180°E = 1 800 000 000
    //latitude : 90°S = -900 000 000 ; 90°N = 900 000 000
    $requete = $pdo->query('SELECT MIN(ouest) AS W, MAX(est) AS E, MAX(nord) AS N,
                            MIN(sud) AS S FROM commune' . $where);
    $ligne = $requete->fetch(PDO::FETCH_ASSOC);

    //si le filtre ne renvoie aucune commune, suppression du filtre
    if (!$ligne['W']) {
        $filtre = 0;
        $requete = $pdo->query('SELECT MIN(ouest) AS W, MAX(est) AS E,
                            MAX(nord) AS N,    MIN(sud) AS S FROM commune');
        $ligne = $requete->fetch(PDO::FETCH_ASSOC);
    }

    //récupération des coordonnées extremes des communes (en 10^-7°)
    $data_W = $ligne['W'];
    $data_E = $ligne['E'];
    $data_N = $ligne['N'];
    $data_S = $ligne['S'];

    /****** CALCUL DES COORDONNEES DE LA CARTE ET DE LA TRANSFORMATION EN PIXELS **/

    //calculs en degrés
    $carte_W = $data_W / 1e7;
    $carte_E = $data_E / 1e7;
    $carte_N = $data_N / 1e7;
    $carte_S = $data_S / 1e7;

    //détermination du centre de la carte (en degrés)
    $centreOE = ($carte_W + $carte_E) / 2;
    $centreNS = ($carte_N + $carte_S) / 2;

    //calcul de la taille d'un degré (en km) au centre de la carte
    $km_lat = 20003.932 / 180;
    $km_lon = $km_lat * cos(deg2rad($centreNS));

    //calcul des dimensions minimales de la carte (en degrés)
    $largeur_d = ($carte_E - $carte_W);
    $hauteur_d = ($carte_N - $carte_S);

    //calcul des dimensions minimales de la carte (en km)
    $largeur_k = $largeur_d * $km_lon;
    $hauteur_k = $hauteur_d * $km_lat;

    //calcul du nombre de pixels utilisés pour afficher un km
    $pixels_par_km_lon = $largeur_carte / $largeur_k;
    $pixels_par_km_lat = $hauteur_carte / $hauteur_k;

    //nombre retenu : coordonnée la moins précise + marge de 2,5% autour de la carte
    $pixels_par_km = min($pixels_par_km_lon, $pixels_par_km_lat) / 1.05;

    //calcul des dimensions réelles de la carte (en km)
    $largeur_k = $largeur_carte / $pixels_par_km;
    $hauteur_k = $hauteur_carte / $pixels_par_km;

    //calcul des dimensions réelles de la carte (en degrés)
    $largeur_d = $largeur_k / $km_lon;
    $hauteur_d = $hauteur_k / $km_lat;
    //calcul des coordonnées extrèmes de la carte (en degrés)
    $carte_W = $centreOE - $largeur_d / 2;
    $carte_E = $centreOE + $largeur_d / 2;
    $carte_N = $centreNS + $hauteur_d / 2;
    $carte_S = $centreNS - $hauteur_d / 2;

    //valeurs extrèmes des points de la carte (en 10^-7°)
    $data_W = $carte_W * 1e7;
    $data_E = $carte_E * 1e7;
    $data_N = $carte_N * 1e7;
    $data_S = $carte_S * 1e7;

    //les tracés des communes sont en 1/1000 de degré pour des raisons techniques
    //en degré, les points sont trop rapprochés pour le moteur SVG
    //en 1/10 000 000 de degré, les nombres sont trop grands pour SVG

    //facteurs de mise à l'échelle entre données des communes et carte
    //le facteur comprend un retournnement vertical pour que le Nord soit en haut
    $largeur_f = $largeur_carte / $largeur_d / 1e3;
    $hauteur_f = -$hauteur_carte / $hauteur_d / 1e3;

    //translation de la carte (coordonnées du coin supérieur gauche)
    $dec_lon = -$carte_W * 1000;
    $dec_lat = -$carte_N * 1000;

    //épaisseur des traits (en fonction du zoom)
    $stroke = round(pow($hauteur_f * $hauteur_f, -0.4) / 2, 4);

    /****** RECUPERATION DE LA LISTE DES COMMUNES VISIBLES SUR LA CARTE ***********/

    //récupération des communes au moins partiellement sur la carte
    if ($avm) {
        $requete = $pdo->query('SELECT * FROM commune WHERE avm&' . $avm . '!=0
            AND ouest<=' . $data_E . ' AND est>=' . $data_W . '
            AND nord>=' . $data_S . ' AND sud<=' . $data_N);
    } else {
        $requete = $pdo->query('SELECT * FROM commune WHERE
            ouest<=' . $data_E . ' AND est>=' . $data_W . '
            AND nord>=' . $data_S . ' AND sud<=' . $data_N);
    }
    $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);

    //détermination si les communes affichées sont dans la sélection ou pas (in)
    foreach ($resultat as $res => $ligne) {
        $resultat[$res]['in'] = (
            ($filtre == 0) ||
            ($filtre <= 100 && $ligne['dept'] == $filtre) ||
            ($filtre <= 1000 && $ligne['etab'] + 100 == $filtre) ||
            ($ligne['insee'] == $filtre) ||
            (is_array($filtre) && in_array($ligne['insee'], $filtre))
        );
    }

    /****** INITIALISATION DE L'AFFICHAGE SVG *************************************/

    $svg = "\n\n";
    $svg .= '<svg id="carte" xmlns="http://www.w3.org/2000/svg" version="1.1" width="' . $largeur_carte . '" height="' . $hauteur_carte . '" viewBox="0 0 ' . $largeur_carte . ' ' . $hauteur_carte . '">' . "\n";

    //liste des styles
    $svg .= '<style type="text/css">' . "\n";
    $svg .= "\t" . 'a {stroke:inherit;}' . "\n";
    $svg .= "\t" . 'a:focus {outline-style:none;}' . "\n";

    //la commune survolée est en surbrillance
    $svg .= "\t" . 'path:hover {fill-opacity:1;}' . "\n";

    //couleurs des départememts
    $svg .= "\t" . '.d75, .d95 {fill:Khaki}' . "\n";
    $svg .= "\t" . '.d77, .d92 {fill:SkyBlue}' . "\n";
    $svg .= "\t" . '.d78, .d94 {fill:GreenYellow}' . "\n";
    $svg .= "\t" . '.d91, .d93 {fill:LightCoral}' . "\n";

    //couleurs des zones
    $svg .= "\t" . '.zone_bois {fill:Tan; stroke:none;}' . "\n";

    $svg .= '</style>' . "\n";

    //fond de carte
    $svg .= '<rect x="0" y="0" width="' . $largeur_carte . '" height="' . $hauteur_carte . '" fill="white"/>' . "\n";

    /****** TRACÉ DES COMMUNES ****************************************************/

    //tracés des communes (transformation de 1/1000 de degré en pixels)
    $svg .= '<g id="communes" transform="scale(' . $largeur_f . ',' . $hauteur_f . ') translate(' . $dec_lon . ',' . $dec_lat . ')">' . "\n";

    //style commun à tous les tracés de communes
    $svg .= "\t" . '<g id="contour" style="stroke-width:' . $stroke . '; fill:darkgray;">' . "\n";

    //affichage des communes
    foreach (array(false, true) as $INOUT) {
        if ($INOUT == false) {
            $svg .= "\t\t" . '<g id="out" style="stroke:white; fill-opacity:0.25;">' . "\n";
        } else {
            $svg .= "\t\t" . '<g id="in" style="stroke:black; fill-opacity:0.5;">' . "\n";
        }

        foreach ($resultat as $ligne) {
            if ($ligne['in'] == $INOUT) {
                //ajout du lien s'il est défini
                if ($liens != '') {
                    $svg .= "\t\t\t" . '<a xlink:href="' . $liens . $ligne['insee'] . '">' . "\n";
                    //ajout de l'info-bulle si elle est définie (il faut que le lien soit défini)
                    $svg .= "\t\t\t" . '<title>' . $ligne['nom_complet'];
                    if (isset($info[$ligne['insee']])) {
                        $svg .= ' ' . $info[$ligne['insee']];
                    }

                    $svg .= '</title>' . "\n";
                }
                //ajout du tracé de la commune
                $svg .= "\t\t\t" . '<path ';

                //couleur spécifiquement définie à défaut couleur du département
                if (isset($couleur[$ligne['insee']])) {
                    $svg .= 'style="fill:' . $couleur[$ligne['insee']] . ';" ';
                } else {
                    $svg .= 'class="d' . floor($ligne['insee'] / 1000) . '" ';
                }

                $svg .= 'd="' . $ligne['contour'] . '"/>' . "\n";

                //ajout des zones de la commune (bois, eau)
                if ($ligne['zones'] != '') {
                    //la liste est sous forme "style|contour¶style|contour¶…"
                    $liste = explode('¶', $ligne['zones']);
                    foreach ($liste as $element) {
                        $el = explode('|', $element);
                        $svg .= "\t\t\t" . '<path class="zone_' . $el[0] . '" d="' . $el[1] . '"/>' . "\n";
                    }
                }

                //fermeture du lien s'il a été ouvert
                if ($liens != '') {
                    $svg .= "\t\t\t" . '</a>' . "\n";
                }

            }
        }
        $svg .= "\t\t" . '</g>' . "\n";
    }

    //affichage en dernier (avant-plan) des communes en sélection (in=true)

    $svg .= "\t" . '</g>' . "\n";

    $svg .= '</g>' . "\n";

    /****** AFFICHAGE DES OBJETS : CARACTERISTIQUES COMMUNES **********************/

    if (is_array($objets) && count($objets) > 0) {
        $svg .= '<g id="objets" style="stroke:none;">' . "\n";

        foreach ($objets as $objet) {
            //valeurs par défaut des attributs de l'objet
            if (!isset($objet['nature'])) {
                $objet['nature'] = 'point';
            }

            if (!isset($objet['taille'])) {
                $objet['taille'] = 8;
            }

            if (!isset($objet['angle'])) {
                $objet['angle'] = 0;
            }

            if (!isset($objet['couleur'])) {
                $objet['couleur'] = '#000000';
            }

            if (!isset($objet['lien'])) {
                $objet['lien'] = '';
            }

            if (!isset($objet['info'])) {
                $objet['info'] = '';
            }

            //calcul de la coordonnée x (en fonction de l'argument x ou de la longitude)
            if (isset($objet['x'])) {
                //coordonnée négative : affichage par rapport au bord droit de la carte
                if ($objet['x'] < 0) {
                    $x = $largeur_carte + $objet['x'];
                } else {
                    $x = $objet['x'];
                }

            } else if (isset($objet['lon'])) {
                //transformation de la longitude en coordonnée
                $x = round(($objet['lon'] * 1e3 + $dec_lon) * $largeur_f, 3);
            } else //par défaut : milieu de la carte
            {
                $x = $largeur_carte / 2;
            }

            //calcul de la coordonnée y (en fonction de l'argument y ou de la latitude)
            if (isset($objet['y'])) {
                //coordonnée négative : affichage par rapport au bord inférieur de la carte
                if ($objet['y'] < 0) {
                    $y = $hauteur_carte + $objet['y'];
                } else {
                    $y = $objet['y'];
                }

            } else if (isset($objet['lat'])) {
                //transformation de la latitude en coordonnée
                $y = round(($objet['lat'] * 1e3 + $dec_lat) * $hauteur_f, 3);
            } else //par défaut : milieu de la carte
            {
                $y = $hauteur_carte / 2;
            }

            //ajout du lien s'il est défini
            if ($objet['lien'] != '') {
                $svg .= "\t" . '<a xlink:href="' . $objet['lien'] . '">' . "\n";
                //ajout de l'info-bulle si elle est définie (il faut que le lien soit défini)
                if ($objet['info'] != '') {
                    $svg .= "\t" . '<title>' . $objet['info'] . '</title>' . "\n";
                }

            }

            switch ($objet['nature']) {
                /****** AFFICHAGE DES OBJETS TEXTE ********************************************/
                case 'texte':
                    //valeurs par défaut des attributs des objets texte
                    if (!isset($objet['texte'])) {
                        $objet['texte'] = 'Hello!';
                    }

                    if (!isset($objet['align'])) {
                        $objet['align'] = 'l';
                    }

                    $svg .= "\t" . '<text x="' . $x . '" y="' . $y . '" style="font-family:Arial; font-size:' . $objet['taille'] . 'px;" fill="' . $objet['couleur'] . '"';
                    //alignement du texte
                    switch ($objet['align']) {
                        case 'm':$svg .= ' text-anchor="middle"';
                            break;
                        case 'r':$svg .= ' text-anchor="end"';
                            break;
                    }
                    //angle d'inclinaison du texte
                    if ($objet['angle'] != 0) {
                        $svg .= ' transform="rotate(' . $objet['angle'] . ',' . $x . ',' . $y . ')"';
                    }

                    $svg .= '>' . $objet['texte'] . '</text>' . "\n";
                    break;

                /****** AFFICHAGE DES OBJETS CONTOUR (référentiel géographique)****************/
                case 'contour':
                    if (!isset($objet['contour'])) {
                        $objet['contour'] = 'MZ';
                    }

                    if (!isset($objet['stroke'])) {
                        $objet['stroke'] = 'lightgray';
                    }

                    if (!isset($objet['stroke-width'])) {
                        $objet['stroke-width'] = 'inherit';
                    }

                    $svg .= '<g id="communes" transform="scale(' . $largeur_f . ',' . $hauteur_f . ') translate(' . $dec_lon . ',' . $dec_lat . ')" style="fill-opacity:0.5; stroke-width:' . $stroke . ';">' . "\n";
                    $svg .= "\t" . '<path d="' . $objet['contour'] . '" style="fill:' . $objet['couleur'] . '; stroke:' . $objet['stroke'] . '; stroke-width:' . $objet['stroke-width'] . ';"';
                    $svg .= '/>' . "\n";
                    $svg .= '</g>' . "\n";
                    break;

                case 'poly':
                    if (!isset($objet['contour'])) {
                        $objet['contour'] = 'MZ';
                    }

                    if (!isset($objet['stroke'])) {
                        $objet['stroke'] = 'lightgray';
                    }

                    if (!isset($objet['stroke-width'])) {
                        $objet['stroke-width'] = 1;
                    }

                    $svg .= "\t" . '<path d="' . $objet['contour'] . '" style="fill:' . $objet['couleur'] . '; stroke:' . $objet['stroke'] . '; stroke-width:' . $objet['stroke-width'] . ';"';
                    $svg .= '/>' . "\n";
                    break;

                /****** AFFICHAGE DES OBJETS POINT (objet par défaut)**************************/
                default:
                    //valeurs par défaut des attributs des objets point
                    if (!isset($objet['point'])) {
                        $objet['point'] = 'rond';
                    }

                    switch ($objet['point']) {
                        case 'triangle':
                            $y1 = $y - $objet['taille'] * 0.5774;
                            $y2 = $y + $objet['taille'] * 0.2887;
                            $x2 = $x + $objet['taille'] * 0.5;
                            $x3 = $x - $objet['taille'] * 0.5;
                            $svg .= "\t" . '<polygon points="' . $x . ' ' . $y1 . ',' . $x2 . ' ' . $y2 . ',' . $x3 . ' ' . $y2 . '" style="stroke:black; stroke-width:0.4; fill:' . $objet['couleur'] . ';"';
                            if ($objet['angle'] != 0) {
                                $svg .= ' transform="rotate(' . $objet['angle'] . ',' . $x . ',' . $y . ')"';
                            }

                            $svg .= '/>' . "\n";
                            break;
                        case 'carre':
                            $y1 = $y - $objet['taille'] * 0.5;
                            $x1 = $x - $objet['taille'] * 0.5;
                            $svg .= "\t" . '<rect x="' . $x1 . '" y="' . $y1 . '" width="' . $objet['taille'] . '" height="' . $objet['taille'] . '" style="stroke:black; stroke-width:0.4; fill:' . $objet['couleur'] . ';"';
                            if ($objet['angle'] != 0) {
                                $svg .= ' transform="rotate(' . $objet['angle'] . ',' . $x . ',' . $y . ')"';
                            }

                            $svg .= '/>' . "\n";
                            break;
                        //point de type diagramme en camembert
                        case 'pie':
                            //si le diagramme n'a pas le format attendu, diagramme par défaut
                            if (!isset($objet['pie']) || count($objet['pie']) != 4 || $objet['pie'][0] == 0) {
                                $objet_pie = array(3, 1, 1, 1);
                            }

                            $couleur = array(1 => 'DodgerBlue', 'LimeGreen', 'chocolate');
                            //initialisation
                            $r = $objet['taille'] * 0.5;
                            $a1 = deg2rad(90);
                            $x1 = $x;
                            $y1 = $y - $r;

                            //pour chaque partie du diagramme (non vide)
                            for ($i = 1; $i <= 3; $i++) {
                                if ($objet['pie'][$i]) {
                                    $prop = $objet['pie'][$i] / $objet['pie'][0];
                                    $a2 = $a1 - deg2rad(360 * $prop);
                                    $x2 = $x + cos($a2) * $r;
                                    $y2 = $y - sin($a2) * $r;
                                    //angle inférieur ou égal à 180°
                                    if ($prop < 0.5) {
                                        $svg .= "\t" . '<path d="M' . $x . ' ' . $y . 'L' . $x1 . ' ' . $y1 . 'A' . $r . ' ' . $r . ' 0 0 1 ' . $x2 . ' ' . $y2 . 'Z" style="stroke:black; stroke-width:0.25; fill:' . $couleur[$i] . ';"/>' . "\n";
                                    }
                                    //angle entre 180° et 360°
                                    else if ($prop < 1) {
                                        $svg .= "\t" . '<path d="M' . $x . ' ' . $y . 'L' . $x1 . ' ' . $y1 . 'A' . $r . ' ' . $r . ' 0 1 1 ' . $x2 . ' ' . $y2 . 'Z" style="stroke:black; stroke-width:0.25; fill:' . $couleur[$i] . ';"/>' . "\n";
                                    }
                                    //cercle complet
                                    else {
                                        $svg .= "\t" . '<circle cx="' . $x . '" cy="' . $y . '" r="' . $r . '" style="stroke:black; stroke-width:0.25; fill:' . $couleur[$i] . ';"/>' . "\n";
                                    }

                                    $a1 = $a2;
                                    $x1 = $x2;
                                    $y1 = $y2;
                                }
                            }
                            break;

                        default:$r = $objet['taille'] * 0.5;
                            $svg .= "\t" . '<circle cx="' . $x . '" cy="' . $y . '" r="' . $r . '" style="stroke:black; stroke-width:0.4; fill:' . $objet['couleur'] . ';"/>' . "\n";
                            break;
                    }
                    break;
            }

            /****** FIN DE L'AFFICHAGE DES OBJETS *****************************************/
            if ($objet['lien'] != '') {
                $svg .= "\t" . '</a>' . "\n";
            }
        }
        $svg .= '</g>' . "\n";
    }
    /****** FIN DU SVG ET SORTIE DE LA FONCTION ***********************************/
    $svg .= '</svg>' . "\n\n";

    return ($svg);
}

function genererObjetsCarteSVG($statusStation, $inseeCommuneFixe = null)
{
    $liste_communes = array();
    $objets = array();

    define('ETAT_INCONNU', 0);
    define('ETAT_TRAVAUX', 1);
    define('ETAT_OUVERTE', 2);
    define('ETAT_FERMEE', 3);
    define('ETAT_NEUTRALISEE', 4);

    define('BORNES_NON', 10);
    define('BORNES_OUI', 20);

    $couleur_etat = array(
        BORNES_NON + ETAT_INCONNU => 'DodgerBlue',
        BORNES_NON + ETAT_TRAVAUX => 'FireBrick',
        BORNES_NON + ETAT_OUVERTE => 'Coral',
        BORNES_NON + ETAT_FERMEE => 'Brown',
        BORNES_NON + ETAT_NEUTRALISEE => 'DarkGoldenrod',
        BORNES_OUI + ETAT_INCONNU => 'MediumOrchid',
        BORNES_OUI + ETAT_TRAVAUX => 'DarkTurquoise',
        BORNES_OUI + ETAT_OUVERTE => 'Green',
        BORNES_OUI + ETAT_FERMEE => 'Brown',
        BORNES_OUI + ETAT_NEUTRALISEE => 'DarkGoldenrod',
    );

    $nombre_etat = array(
        BORNES_NON + ETAT_INCONNU => 0,
        BORNES_NON + ETAT_TRAVAUX => 0,
        BORNES_NON + ETAT_OUVERTE => 0,
        BORNES_NON + ETAT_FERMEE => 0,
        BORNES_NON + ETAT_NEUTRALISEE => 0,
        BORNES_OUI + ETAT_INCONNU => 0,
        BORNES_OUI + ETAT_TRAVAUX => 0,
        BORNES_OUI + ETAT_OUVERTE => 0,
        BORNES_OUI + ETAT_FERMEE => 0,
        BORNES_OUI + ETAT_NEUTRALISEE => 0,
    );

    // Si l'insee est fixé, c'est qu'il n'y en a qu'une
    if (!is_null($inseeCommuneFixe)) {
        $liste_communes[] = $inseeCommuneFixe;
    }

    if (!is_null($statusStation)) {
        foreach ($statusStation as $station) {
            //interprétation de l'état de la station 0=état non prévu
            switch ($station['state']) {
                case 'En travaux':
                case 'Work in progress':
                    $etat = ETAT_TRAVAUX;
                    break;
                case 'Operative':
                    $etat = ETAT_OUVERTE;
                    break;
                case 'Close':
                    $etat = ETAT_FERMEE;
                    break;
                case 'Neutralised':
                    $etat = ETAT_NEUTRALISEE;
                    break;
                default:
                    $etat = ETAT_INCONNU;
            }

            //forme en fonction de l'état annoncé
            if ($etat == ETAT_INCONNU) {
                $s_point = 'rond';
                $s_taille = 6;
                $s_angle = 0;
                $s_info1 = 'État inconnu';
            } else if ($etat == ETAT_TRAVAUX) {
                $s_point = 'triangle';
                $s_taille = 8;
                $s_angle = 0;
                $s_info1 = 'En travaux';
            } else if ($etat == ETAT_OUVERTE) {
                $s_point = 'carre';
                $s_taille = 6;
                $s_angle = 45;
                $s_info1 = 'En service';
            } else if ($etat == ETAT_NEUTRALISEE || $etat == ETAT_FERMEE) {
                $s_point = 'carre';
                $s_taille = 6;
                $s_angle = 0;
                $s_info1 = 'Hors service';
            }

            //présence de bornes actives ?
            if ($station['nbEDock'] != 0) {
                $etat += BORNES_OUI;
            } else {
                $etat += BORNES_NON;
            }

            //comptage du nombre de stations par etat
            $nombre_etat[$etat]++;

            if (is_null($inseeCommuneFixe)) {
                //récupération de la commune correspondant à la station (d'après son numéro)
                $inseeCommune = getCommuneStation($station['code']);

                //ajout à la liste des communes
                if (!is_null($inseeCommune) && !in_array($inseeCommune, $liste_communes)) {
                    $liste_communes[] = $inseeCommune;
                }
            } else {
                $inseeCommune = $inseeCommuneFixe;
            }

            //texte en fonction du nombre de bornes
            switch ($station['nbEDock']) {
                case 0:$s_info2 = 'aucune borne';
                    break;
                case 1:$s_info2 = '1 borne';
                    break;
                default:$s_info2 = $station['nbEDock'] . ' bornes';
                    break;
            }

            $objets[] = array(
                'nature' => 'point',
                'point' => $s_point,
                'taille' => $s_taille,
                'angle' => $s_angle,
                'couleur' => $couleur_etat[$etat],
                'lon' => $station['longitude'],
                'lat' => $station['latitude'],
                'lien' => 'station.php?code=' . $station['code'],
                'info' => 'Station ' . displayCodeStation($station['code']) . "\n" . $station['name'] . "\n" . $s_info1 . ' - ' . $s_info2,
            );

        }
    }

    //légende (point et texte, affiché uniquement pour les états ayant au moins une station
    if ($nombre_etat[BORNES_NON + ETAT_INCONNU]) {
        $objets[] = array('point' => 'rond', 'x' => -78, 'y' => 10, 'taille' => 6, 'angle' => 0,
            'couleur' => $couleur_etat[BORNES_NON + ETAT_INCONNU]);
        $objets[] = array('nature' => 'texte', 'x' => -80, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Inconnu sans borne (' . $nombre_etat[BORNES_NON + ETAT_INCONNU] . ')');
    }
    if ($nombre_etat[BORNES_NON + ETAT_TRAVAUX]) {
        $objets[] = array('point' => 'triangle', 'x' => -38, 'y' => 10, 'taille' => 8, 'angle' => 90,
            'couleur' => $couleur_etat[BORNES_NON + ETAT_TRAVAUX]);
        $objets[] = array('nature' => 'texte', 'x' => -40, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Stations en travaux (' . $nombre_etat[BORNES_NON + ETAT_TRAVAUX] . ')');
    }
    if ($nombre_etat[BORNES_NON + ETAT_OUVERTE]) {
        $objets[] = array('point' => 'carre', 'x' => -18, 'y' => 210, 'taille' => 6, 'angle' => 45,
            'couleur' => $couleur_etat[BORNES_NON + ETAT_OUVERTE]);
        $objets[] = array('nature' => 'texte', 'x' => -20, 'y' => 220, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'En service sans borne (' . $nombre_etat[BORNES_NON + ETAT_OUVERTE] . ')');
    }
    if ($nombre_etat[BORNES_OUI + ETAT_INCONNU]) {
        $objets[] = array('point' => 'rond', 'x' => -78, 'y' => 210, 'taille' => 6, 'angle' => 0,
            'couleur' => $couleur_etat[BORNES_OUI + ETAT_INCONNU]);
        $objets[] = array('nature' => 'texte', 'x' => -80, 'y' => 220, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Inconnu avec bornes (' . $nombre_etat[BORNES_OUI + ETAT_INCONNU] . ')');
    }
    if ($nombre_etat[BORNES_OUI + ETAT_FERMEE] || $nombre_etat[BORNES_NON + ETAT_FERMEE]) {
        $objets[] = array('point' => 'carre', 'x' => -58, 'y' => 10, 'taille' => 6, 'angle' => 0,
            'couleur' => $couleur_etat[BORNES_OUI + ETAT_FERMEE]);
        $objets[] = array('nature' => 'texte', 'x' => -60, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Stations fermées (' . (($nombre_etat[BORNES_OUI + ETAT_FERMEE] || 0) + ($nombre_etat[BORNES_OUI + ETAT_FERMEE] || 0)) . ')');
    }
    if ($nombre_etat[BORNES_NON + ETAT_NEUTRALISEE] || $nombre_etat[BORNES_OUI + ETAT_NEUTRALISEE]) {
        $objets[] = array('point' => 'carre', 'x' => -58, 'y' => 210, 'taille' => 6, 'angle' => 0,
            'couleur' => $couleur_etat[BORNES_NON + ETAT_NEUTRALISEE]);
        $objets[] = array('nature' => 'texte', 'x' => -60, 'y' => 220, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Stations neutralisées (' . (($nombre_etat[BORNES_NON + ETAT_NEUTRALISEE] || 0) + ($nombre_etat[BORNES_OUI + ETAT_NEUTRALISEE] || 0)) . ')');
    }
    if ($nombre_etat[BORNES_OUI + ETAT_TRAVAUX]) {
        $objets[] = array('point' => 'triangle', 'x' => -38, 'y' => 210, 'taille' => 8, 'angle' => 90,
            'couleur' => $couleur_etat[BORNES_OUI + ETAT_TRAVAUX]);
        $objets[] = array('nature' => 'texte', 'x' => -40, 'y' => 220, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'En travaux avec bornes (' . $nombre_etat[BORNES_OUI + ETAT_TRAVAUX] . ')');
    }
    if ($nombre_etat[BORNES_OUI + ETAT_OUVERTE]) {
        $objets[] = array('point' => 'carre', 'x' => -18, 'y' => 10, 'taille' => 6, 'angle' => 45,
            'couleur' => $couleur_etat[BORNES_OUI + ETAT_OUVERTE]);
        $objets[] = array('nature' => 'texte', 'x' => -20, 'y' => 20, 'taille' => 12, 'angle' => 90, 'align' => 'l',
            'texte' => 'Stations en service (' . $nombre_etat[BORNES_OUI + ETAT_OUVERTE] . ')');
    }

    return array('obj' => $objets, 'communes' => $liste_communes);
}
