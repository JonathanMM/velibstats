<?php
require_once '../../libs/Smarty.class.php';

$smarty = new Smarty();

$smarty->assign(array(
    'pageId' => 'accueil',
    'titre' => $service->name,
    'adresseSiteWeb' => $adresseSiteWeb,
));

//Filtre 24 heures
$hier = new DateTime("-1day");
$filtreDate = $hier->format('Y-m-d H:i:s');

//Dernière conso
$requete = $pdo->query('SELECT * FROM `statusConso` where service = ' . $service->id . ' and nbStation Is not null and date >= "' . $filtreDate . '" Order by id desc limit 0,1');
$conso = $requete->fetch();

//Filtre 7 jours
$semaineDerniere = new DateTime("-7day");
$filtreDateSemaineDerniere = $semaineDerniere->format('Y-m-d H:i:s');

//Stats journalières
$requete = $pdo->query('SELECT * FROM statsJournaliere WHERE service = ' . $service->id . ' and date >= "' . $filtreDateSemaineDerniere . '" AND nbJour = 1 ORDER BY date DESC');
$derniereStat = $requete->fetch();

$smarty->assign(array(
    'idConso' => $conso['id'],
    'nbStation' => $conso['nbStation'],
    'nbStationDetecte' => $conso['nbStationDetecte'],
    'nbBike' => $conso['nbBike'],
    'nbEDock' => $conso['nbEDock'],
    'nbFreeEDock' => $conso['nbFreeEDock'],
    'dateDerniereConso' => (new DateTime($conso['date']))->format('d/m/Y à H:i'),
    'statsDate' => (new DateTime($derniereStat['date']))->format('d/m/Y'),
    'statsNombre' => min($derniereStat['nbBikePris'] + $derniereStat['nbEBikePris'], $derniereStat['nbBikeRendu'] + $derniereStat['nbEBikeRendu']),
    'hasEbike' => $service->config->eBike,
    'hasOverflow' => $service->config->overflow,
    'hasDateOuverture' => $service->config->dateOuverture,
));

if ($service->config->eBike) {
    $smarty->assign(array(
        'nbEbike' => $conso['nbEbike'],
    ));
}

if ($service->config->overflow) {
    $smarty->assign(array(
        'nbOverflow' => $conso['nbBikeOverflow'] + $conso['nbEbikeOverflow'],
    ));
}

$smarty->display(__DIR__ . '/tpl/index.tpl');
exit();
