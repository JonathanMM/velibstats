function getData(url) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onload = function() {
      if (xhr.status === 200) resolve(JSON.parse(xhr.response));
      else reject(xhr.statusText);
    };

    xhr.send();
  });
}

function getDataTotem(codeStation) {
  getData("api.php?action=getDataTotem&codeStation=" + codeStation).then(
    function(data) {
      // Pour celui du jour, plusieurs choses à faire :
      // On cumule les comptages qui sont avant l'heure courante
      var currentDateTime = new Date();
      var currentHour = currentDateTime.getHours();
      var currentMinutes = currentDateTime.getMinutes();

      var totalJour = 0;
      for (var heure in data.compteurHeure) {
        if (heure < currentHour)
          totalJour += parseInt(data.compteurHeure[heure]);
        else if (heure == currentHour) {
          var nbPassage = parseInt(data.compteurHeure[heure]);

          //On va simuler un nombre de cycliste déjà passé
          var incertitude = Math.random() * 0.2 - 0.1;
          var dejaPasse = Math.floor(
            (currentMinutes / 60 + incertitude) * nbPassage
          );
          totalJour += dejaPasse;

          var reste = nbPassage - dejaPasse;
          // Et là, on va faire genre ça passe de façon random
          var maxTimeout = (60 - currentMinutes - 1) * 60000;
          for (var i = 0; i < reste; i++) {
            var timeout = Math.floor(Math.random() * maxTimeout);
            setTimeout(function() {
              var compteurJour = document.getElementById("compteur-jour");
              var passageCourant = parseInt(compteurJour.innerHTML);
              compteurJour.innerHTML = passageCourant + 1;

              var compteurAn = document.getElementById("compteur-an");
              passageCourant = parseInt(compteurAn.innerHTML);
              compteurAn.innerHTML = passageCourant + 1;
            }, timeout);
          }
        }
      }

      // Et si on change d'heure, bah, on a qu'à rappeler la fonction :p
      // Prochaine heure dans
      var prochaineHeureDans =
        ((60 - currentDateTime.getMinutes() - 1) * 60 +
          (60 - currentDateTime.getSeconds() + 1)) *
        1000;

      setTimeout(function() {
        return getDataTotem(codeStation);
      }, prochaineHeureDans);

      // Et on randomise pour l'heure courante
      var compteurJour = document.getElementById("compteur-jour");
      compteurJour.innerHTML = totalJour;

      // On rempli le compteur des années
      var compteurAn = document.getElementById("compteur-an");
      compteurAn.innerHTML = data.totalAn + totalJour;
    }
  );
}
