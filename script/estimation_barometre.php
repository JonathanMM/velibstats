<?php
require_once '../config.php';
include_once '../functions.php';

// On commence à regarder la date max dans baromètre
$auj = new DateTime('now', new DateTimeZone('Europe/Paris'));
$dateAuj = $auj->format('Y-m-d');

// Pour déterminer la veille, on va considérer la dernière conso (utile quand la conso de la veille n'est pas encore dispo)
$typeJour = getTypeJour($dateAuj);
$hier = new DateTime("-10day");
$filtreDate = $hier->format('Y-m-d H:i:s');

//Dernière conso
$requete = $pdo->query('SELECT * FROM `comptage_resumeConso` where nbStation Is not null and date >= "' . $filtreDate . '" and duree = 1440 ' . ($typeJour == 'sam' ? ' AND DAYOFWEEK(date) = 7' : ($typeJour == 'djf' ? ' AND DAYOFWEEK(date) = 1' : ' AND DAYOFWEEK(date) >= 2 AND DAYOFWEEK(date) <= 6')) . ' Order by date desc limit 0,1');
$conso = $requete->fetch();

$dateVeilleObj = new DateTime($conso['date'], new DateTimeZone('Europe/Paris'));
$dateVeille = $dateVeilleObj->format('Y-m-d');

$requeteHeureMax = $pdo->query('SELECT TIME(MAX(date)) As heureMax from comptage_barometre WHERE date >= "' . $dateAuj . '"');
$donneesHeureMax = $requeteHeureMax->fetch();

if ($donneesHeureMax === false || !isset($donneesHeureMax['heureMax'])) {
    $heureMax = '00:00:00';
} else {
    $heureMax = $donneesHeureMax['heureMax'];
}

// De cette heure max, on obtient la borne inf de la boucle
$borneMin = DateTime::createFromFormat('H:m:s', $heureMax, new DateTimeZone('Europe/Paris'));
$borneMinHeure = intval($borneMin->format('H'));
$borneMinMinutes = intdiv(intval($borneMin->format('m')), 5) * 5;

//La borne max correspond à l'heure actuelle
$borneMaxHeure = intval($auj->format('H'));
$borneMaxMinutes = intdiv(intval($auj->format('m')), 5) * 5;

for ($heure = $borneMinHeure; $heure <= $borneMaxHeure; $heure++) {

    $requete = $pdo->query('SELECT date, nombre
FROM `comptage_statusConso`
WHERE date = "' . $dateVeille . ' ' . $heure . ':00:00"
ORDER BY date ASC');
    $donnee = $requete->fetch();
    $nbComptage = $donnee['nombre'];

    $estimation2Veille = 0;
    $estimation2Auj = 0;

    $borneMinLocMinutes = $heure == $borneMinHeure ? $borneMinMinutes : 0;
    $borneMaxLocMinutes = $heure == $borneMaxHeure ? $borneMaxMinutes : 60;

    for ($minute = $borneMinLocMinutes; $minute < $borneMaxLocMinutes; $minute += 5) {
        $requete = $pdo->query('SELECT date, LEAST(SUM(nbBikePris + nbEBikePris), SUM(nbBikeRendu + nbEBikeRendu)) AS nbLocations
    FROM `resumeStatus`
    WHERE duree = 5 AND date >= "' . $dateVeille . ' ' . $heure . ':00:00" AND date <= "' . $dateVeille . ' ' . $heure . ':' . $minute . ':00"
    GROUP BY date
    ORDER BY date ASC');
        $donnees = $requete->fetchAll();

        if ($donnees === false || count($donnees) != (($minute / 5) + 1)) {
            echo 'Pas assez de données de la veille (' . count($donnees) . ' au lieu de ' . (($minute / 5) + 1) . ')';
            continue;
        }

        $requete2 = $pdo->query('SELECT date, LEAST(SUM(nbBikePris + nbEBikePris), SUM(nbBikeRendu + nbEBikeRendu)) AS nbLocations
    FROM `resumeStatus`
    WHERE duree = 5 AND date >= "' . $dateAuj . ' ' . $heure . ':00:00" AND date <= "' . $dateAuj . ' ' . $heure . ':' . $minute . ':00"
    GROUP BY date
    ORDER BY date ASC');
        $donnees2 = $requete2->fetchAll();

        if ($donnees2 === false || count($donnees2) != (($minute / 5) + 1)) {
            echo 'Pas assez de données du jour';
            continue;
        }

        // On calcule les estimations
        $estimationVeille = 0;
        foreach ($donnees as $donnee) {
            $estimationVeille += $donnee['nbLocations'];
        }

        $estimationAuj = 0;
        foreach ($donnees2 as $donnee) {
            $estimationAuj += $donnee['nbLocations'];
        }

        $estimation = $estimationVeille == 0 ? 0 : ($estimationAuj * $nbComptage) / $estimationVeille;

        $pdo->exec('INSERT INTO comptage_barometre (date, estimation) VALUES ("' . $dateAuj . ' ' . $heure . ':' . $minute . ':00", ' . floor($estimation) . ')');
    }
}
