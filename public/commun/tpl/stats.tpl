{include file='./header.tpl'}
<header class="header">
    <h1>Cristalib Nocle</h1> 
    <h2>site non officiel</h2>
</header>
<div id="content">
    <form method="GET" action="stats.php">
        Date : <input name="date" type="date" /> <input type="submit" value="Filtrer" />
    </form>
    <p>Sur la dernière journée ({$minDate} → {$maxDate}) :</p>
    <ul>
        <li>Au moins {$totalPris} vélos ont été retirés (dont {$totalPrisMecanique} mécaniques et {$totalPrisElectrique} électriques);</li>
        <li>Au moins {$totalRendu} vélos ont été remis (dont {$totalRenduMecanique} mécaniques et {$totalRenduElectrique} électriques);</li>
        <li>Nombre moyen de bornes inutilisables : {$bornesPerduesMoyenne} (maximum de la période : {$bornesPerduesMax}).</li>
        <li>Nombre de stations sans mouvement : {$stationsSansMouvement}, sur un total de {$totalStation} stations.</li>
    </ul>
    <p>Nombre de stations par départements :</p>
    <ul>
        {foreach $departements as $code => $departement}
            <li>{$code} {$departement.nom} : {$departement.nb}
        {/foreach}
    </ul>
    <p>Liste des stations sans mouvement : {foreach $listeStationSansMouvement as $i => $code}{if $i ne 0}; {/if}<a href="station.php?code={$code}">{$code}</a>{/foreach}.</p>
    <p>Les nombres de vélos empruntés et rendus sont obtenu en comparant deux relevés d'état d'une station. 
    Ainsi, le système considèrera de la même façon si deux vélos ont été pris à une station que si dans la même minute 
    quatres vélos ont été pris et deux rendus. 
    Les nombres ainsi obtenus ne sont que des minimums, et comprennent à la fois les locations comme les actions de régulations.<br />
    Les bornes inutilisables correspond au calcul pour chaque station : (nombre de bornes total) - (nombre de vélos mécaniques + nombre de vélos 
    électriques + nombres de bornes disponibles)<br />
    Le nombre de stations ne différencie pas les stations en travaux des stations ouvertes.
    </p>
    {include file="./credits.tpl"}
</div>
{include file="./footer.tpl"}
