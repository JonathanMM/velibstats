import urllib.request
import json
import os
import pymysql
from datetime import datetime
from config import getMysqlConnection
from adresseLib import getAdresse, getInsee


def getStationIdByLatLong(latitude, longitude):
    tmpFileName = 'gbfsStation.json'
    urllib.request.urlretrieve(
        "https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_information.json", tmpFileName)
    data = json.load(open(tmpFileName))
    os.remove(tmpFileName)
    for stat in data['data']['stations']:
        if abs(stat['lat'] - latitude) < 0.00002 and abs(stat['lon'] - longitude) < 0.00002:
            return stat['station_id']


def getGBFSData():
    tmpFileName = 'gbfsStatus.json'
    urllib.request.urlretrieve(
        "https://velib-metropole-opendata.smoove.pro/opendata/Velib_Metropole/station_status.json", tmpFileName)
    data = json.load(open(tmpFileName))
    os.remove(tmpFileName)
    return data['data']['stations']
