<div id="credits">
    Les données utilisées proviennent du jeu de données 
    <a href="https://opendata.paris.fr/explore/dataset/comptage-velo-donnees-compteurs/">Comptage vélo - Données compteurs</a>
    disponible sur le site <a href="https://opendata.paris.fr/">Paris Data</a> sous licence ODbL, et appartiennent à leur propriétaire. - 
    <a href="https://framagit.org/JonathanMM/velibstats">Site du projet et sources</a> - 
    Auteur : JonathanMM (<a href="https://twitter.com/Jonamaths">@Jonamaths</a>)
</div>