import pymysql
import datetime
from config import getMysqlConnection


def calculerStatsJournaliere(dateStats):
    dateConso = dateStats.strftime("%Y-%m-%d")
    dateConsoAvant = dateStats.strftime("%Y-%m-%d %H:%M:%S")
    finConso = dateStats + datetime.timedelta(days=1)
    dateConsoFin = finConso.strftime("%Y-%m-%d %H:%M:%S")

    proprietes = ['nbBikePris', 'nbBikeRendu', 'nbEbikePris', 'nbEbikeRendu']
    mysql = getMysqlConnection()
    # On récupère la minute d'avant pour les bases de conso
    requete = mysql.cursor()
    requete.execute("SELECT SUM(" + "), SUM(".join(proprietes) + ") \
    FROM resumeStatus \
    WHERE (`date` >= '"+dateConsoAvant+"' AND `date` < '"+dateConsoFin+"') \
    AND duree = 1440 GROUP BY date")
    resumeJour = requete.fetchone()

    if resumeJour != None:
        requete = mysql.cursor()
        requete.execute('INSERT INTO `statsJournaliere` (date, nbJour, '+", ".join(proprietes)+") VALUES \
            ('" + dateConso + "', 1, " + ", ".join(map(str, resumeJour)) + ")")
        mysql.commit()
