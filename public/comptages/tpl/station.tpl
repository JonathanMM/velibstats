{include file='./header.tpl'}
<header>
    <h1>Comptages Nocle (site non officiel) - {$stationTitre}</h1>
    <nav>
        <a href="index.php">&lt; Retour à l'accueil</a> |
        <a href="totem.php?code={$stationCode}">Voir le totem &gt;</a>
    </nav>
</header>
<div id="content">
    <ul>
        <li>Code : {$stationCode}</li>
        <li>Nom : {$stationNom}</li>
        <li>Date d'ouverture : {$stationDateOuverture}</li>
        {if $stationHasEnfant}
            <li>Points de comptage agrégés: 
                <ul>
                {foreach from=$stationEnfants item=$enfant}
                    <li><a href="station.php?code={$enfant['code']}">{$enfant['code']} - {$enfant['nom']}</a></li>
                {/foreach}
                </ul>
            </li>
        {/if}
        {if $stationHasParent}
            <li>Ce point de comptage fait parti du regroupement <a href="station.php?code={$stationParentCode}">{$stationParentCode} - {$stationParentNom}</a></li>
        {/if}
    </ul>
    <h2>Graphique</h2>
    <select id="typeGraphiqueSelect" style="display: none">
        <option value="Bike">Passages</option>
    </select>
    <select id="dureeGraphiqueSelect">
        <option value="septJours">Une semaine - Période d'une heure</option>
        <option value="unMois">Un mois - Période de six heures</option>
        <option value="unMoisParJour">Un mois - Période d'un jour</option>
        <option value="unTrimestre">Un trimestre - Période d'un jour</option>
        <option value="unAn">Un an - Période d'un jour</option>
        <option value="cinqAns">Cinq ans - Période d'un jour</option>
    </select>
    Graphique issu du site comptages.nocle.fr
    <canvas id="chartBikes" width="1000" height="400"></canvas>
    {include file="./credits.tpl"}
    <h2>Stats</h2>
    <table id="stats">
        <thead>
            <tr>
                <th>Date</th>
                <th>Passages</th>
            </tr>
        </thead>
    </table>
    <script type="text/javascript">
        var codeStation = "{$stationCode}";
    </script>
    <script type="application/javascript">
        $(document).ready( function () {
            var dt = $('#stats').DataTable({
                ajax: 'api.php?action=getDataStation&codeStation={$stationCode}',
                columns: [{
                    data: 'date',
                    render: function(data, type, row, meta)
                    {
                        var date = new Date(data);
                        return putZero(date.getDate()) + '/' + putZero(date.getMonth()+1) + '/' + date.getFullYear() + ' ' + putZero(date.getHours()) + ':' + putZero(date.getMinutes());
                    }
                },{
                    data: 'nombre'
                }],
                language: dtTraduction
            });
        } );
    </script>
</div>
{include file="./footer.tpl"}