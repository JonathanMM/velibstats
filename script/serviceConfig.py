class ServiceConfig:
    """Info de configuration d'un service"""

    dateOuverture = False
    eBike = False
    overflow = False

    def __init__(self, configDict):
        self.dateOuverture = configDict['dateOuverture']
        self.eBike = configDict['eBike']
        self.overflow = configDict['overflow']
