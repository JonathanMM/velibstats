{include file='./header.tpl'}
<header class="header">
    <h1>{$titre} Nocle</h1> 
    <h2>site non officiel</h2>
</header>
<div id="content">
    <div id="statsCarteArea">
        <div id="statsConsoArea">
            {include file='./index_statsConso.tpl'}
        </div>
        <div id="carteArea"></div>
    </div>
    <select id="typeGraphiqueSelect" style="display: none">
        <option value="_double">Conso</option>
    </select>
    <select id="dureeGraphiqueSelect">
        <option value="instantanee">Une heure - Instantanée</option>
        <option value="troisHeures">Trois heures - Période de 5 minutes</option>
        <option value="unJour">Un jour - Période de 15 minutes</option>
        <option value="septJours" selected>Une semaine - Période d'une heure</option>
        <option value="unMois">Un mois - Période de six heures</option>
        <option value="unAn">Un an - Période d'un jour</option>
    </select> - Graphique issu du site {$adresseSiteWeb}
    <div id="chartArea">
        <canvas id="chartNbStations" height="500px" width="800px"></canvas>
        <canvas id="chartBikes" height="500px" width="800px"></canvas>
    </div>
    {include file="./credits.tpl"}
    <h2>Stations</h2>
    Fitrer : État 
    <select id="filtreEtat">
        <option value="toutes">Toutes</option>
        <option value="ouverte" selected>Ouverte</option>
        <option value="fermee">Fermée</option>
    </select> - 
    Position 
    <select id="filtrePosition">
        <option value="toutes" selected>Indifféremment</option>
        <option value="proximite">À proximité</option>
    </select>
    <table id="stations">
        <thead>
            <tr>
                <th>Code</th>
                <th>Nom</th>
                {if $hasDateOuverture}<th>Date d'ouverture</th>{/if}
                <th>Statut</th>
                <th>Vélos mécaniques dispo</th>
                {if $hasEbike}<th>Vélos électriques dispo</th>{/if}
                {if $hasOverflow}<th>Vélos en overflow</th>{/if}
                <th>Bornes libres</th>
                {if $hasOverflow}<th>Park+ (overflow)</th>{/if}
            </tr>
        </thead>
    </table>
    <script type="text/javascript">
    var codeStation = -1;
    </script>
    <script type="application/javascript">
        function filtreDataTable()
        {
            var valeur = $("#filtreEtat").val();
            var dt = $('#stations').DataTable();
            var numColonne = {if $hasDateOuverture}3{else}2{/if};
            switch (valeur) {
                case "ouverte":
                    dt.column(numColonne).search("Ouverte").draw();
                    break;
                case "travaux":
                    dt.column(numColonne).search("En travaux").draw();
                    break;
                case "fermee":
                    dt.column(numColonne).search("Fermée").draw();
                    break;
                default:
                    dt.column(numColonne).search("").draw();
                    break;
            }
        }

        $(document).ready( function () {
            var dt = $('#stations').DataTable({
                ajax: 'api.php?action=getDataConso&idConso={$idConso}',
                columns: [{
                    data: 'codeStr',
                    render: function(data, type, row, meta)
                    {
                        return '<a href="station.php?code='+row.code+'">'+data+'</a>';
                    }
                },{
                    data: 'name',
                    render: function(data, type, row, meta)
                    {
                        return '<a href="station.php?code='+row.code+'">'+data+'</a>';
                    }
                }{if $hasDateOuverture},{
                    data: 'dateOuverture',
                    render: function(data, type, row, meta)
                    {
                        if(data == 'Non ouvert')
                            return data;
                        var date = new Date(data);
                        if(type == 'sort') //Pour le tri, on fait en sorte que la valeur soit triable
                            return date.getFullYear() + '-' + putZero(date.getMonth()+1) + '-' + putZero(date.getDate());
                        
                        return putZero(date.getDate()) + '/' + putZero(date.getMonth()+1) + '/' + date.getFullYear();
                    }
                }{/if},{
                    data: 'state',
                    render: function(data, type, row, meta)
                    {
                        if(data == 'OPEN' && row.nbEDock > 0)
                            return 'Ouverte';
                        else if(data == 'CLOSE' || data == 'Neutralised')
                            return 'Fermée';
                        else
                            return 'En travaux';
                    }
                },{
                    data: 'nbBike'
                }{if $hasEbike},{
                    data: 'nbEbike'
                }{/if}{if $hasOverflow},{
                    data: 'nbBikeOverflow',
                    render: function(data, type, row, meta)
                    {
                        return parseInt(row.nbBikeOverflow) + parseInt(row.nbEbikeOverflow);
                    }
                }{/if},{
                    data: 'nbFreeEDock',
                    render: function(data, type, row, meta)
                    {
                        if(type == 'sort') //Pour le tri, on utilise le nombre de bornes libres directement
                        {
                            if(data < 10)
                                return '00' + data.toString();
                            if(data < 100)
                                return '0' + data.toString();
                            return data;
                        }
                        return data+'/'+row.nbEDock;
                    }
                }{if $hasOverflow},{
                    data: 'overflow',
                    render: function(data, type, row, meta)
                    {
                        if(data == 'yes')
                            return 'oui';
                        else
                            return 'non';
                    }
                }{/if}],
                language: dtTraduction
            });
            filtreDataTable();
            $("#filtreEtat").change(filtreDataTable);
            $("#filtrePosition").change(filtrePosition);
            //$('#btn-carte-etat').click(voirCarteEtat);
            $('#btn-carte-signalement').click(voirCarteSignalement);
            $('#btn-carte-data').click(voirCarteData);
            //voirCarteEtat();
        });

        function filtrePosition()
        {
            var valeur = $("#filtrePosition").val();
            if(valeur == 'proximite')
                obtenirLocalisation();
            else
                resetLocalisation();
        }

        function obtenirLocalisation()
        {
            if (navigator.geolocation)
                navigator.geolocation.getCurrentPosition(traiterLocalisation);
            else
                alert("Votre navigateur ne supporte pas la fonction localisation");
        }

        function traiterLocalisation(position)
        {
            var dt = $('#stations').DataTable();
            dt.ajax.url('api.php?action=getDataConso&idConso={$idConso}&lat='+position.coords.latitude+'&long='+position.coords.longitude).load();
        }

        function resetLocalisation()
        {
            var dt = $('#stations').DataTable();
            dt.ajax.url('api.php?action=getDataConso&idConso={$idConso}').load();
        }

        function recupererCarte(url)
        {
            getData(url).then(function(svg) {
                $("#carteArea").html(svg);
            });
        }

        /*function voirCarteEtat()
        {
            recupererCarte('api.php?action=getCommunesCarte&idConso={$idConso}');
        }*/

        function voirCarteSignalement()
        {
            recupererCarte('api.php?action=getSignalementCarte&idConso={$idConso}');
        }

        function voirCarteData()
        {
            recupererCarte('api.php?action=getEtatCarte&idConso={$idConso}');
        }
    </script>
</div>
{include file="./footer.tpl"}