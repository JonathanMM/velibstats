<?php
require_once '../../config.php';
include_once '../../functions.php';
include_once 'carte.php';

function getStatusByIdConso_comptage($dateResume, $filtreWhere = null)
{
    global $pdo;
    $requete = $pdo->query('SELECT * FROM comptage_resumeStatus s INNER JOIN `comptage_stations` p ON p.code = s.code WHERE duree = 1440 and date = "' . $dateResume . '" AND parent IS NULL ' .
        (is_null($filtreWhere) ? '' : ' AND (' . $filtreWhere . ')') . ' ORDER BY s.code ASC');
    if ($requete === false) {
        return null;
    }

    $data = $requete->fetchAll(PDO::FETCH_ASSOC);
    $retour = [];
    foreach ($data as $station) {
        $retour[] = array(
            'code' => $station['code'],
            'codeStr' => $station['code'],
            'name' => $station['nom'],
            'dateOuverture' => is_null($station['dateOuverture']) ? 'Non ouvert' : $station['dateOuverture'],
            'nombre' => $station['nombre'],
            'latitude' => $station['latitude'],
            'longitude' => $station['longitude'],
            'type' => $station['type'],
        );
    }
    return $retour;
}

error_reporting(E_ALL);

if (!isset($_GET['action']) || strlen($_GET['action']) == 0) {
    exit();
}

if ((!isset($_GET['codeStation']) || intval($_GET['codeStation']) == 0) && (!isset($_GET['idConso']) || intval($_GET['idConso']) == 0) && (!isset($_GET['dateResume']) || strlen($_GET['dateResume']) == 0)) {
    exit();
}

header('Content-Type: application/json');

if (isset($_GET['codeStation'])) {
    $codeStation = intval($_GET['codeStation']);
}

if (isset($_GET['idConso'])) {
    $idConso = intval($_GET['idConso']);
}

if (isset($_GET['dateResume'])) {
    $dateResume = htmlspecialchars($_GET['dateResume']);
}

if (isset($_GET['lat'])) {
    $latitude = floatval($_GET['lat']);
} else {
    $latitude = null;
}

if (isset($_GET['long'])) {
    $longitude = floatval($_GET['long']);
} else {
    $longitude = null;
}

switch ($_GET['action']) {
    case 'getBikeResumeSeptJours':
        echo json_encode(getBikeInstantane($codeStation));
        exit();
        break;
    case 'getBikeResumeUnMois':
        echo json_encode(getBikeResume($codeStation, "-1month", 360));
        exit();
        break;
    case 'getBikeResumeUnMoisPerDay':
        echo json_encode(getBikeResume($codeStation, "-1month", 1440));
        exit();
        break;
    case 'getBikeResumeUnTrimestrePerDay':
        echo json_encode(getBikeResume($codeStation, "-3month", 1440));
        exit();
        break;
    case 'getBikeResumeUnAn':
        echo json_encode(getBikeResume($codeStation, "-1year", 1440));
        exit();
        break;
    case 'getBikeResumeCinqAns':
        echo json_encode(getBikeResume($codeStation, "-5year", 1440));
        exit();
        break;
    case 'getConsoBikeResumeSeptJours':
        echo json_encode(getConsoBikeInstantane());
        exit();
        break;
    case 'getConsoBikeResumeUnMois':
        echo json_encode(getConsoBikeResume("-1month", 360));
        exit();
        break;
    case 'getConsoBikeResumeUnMoisPerDay':
        echo json_encode(getConsoBikeResume("-1month", 1440));
        exit();
        break;
    case 'getConsoBikeResumeUnTrimestrePerDay':
        echo json_encode(getConsoBikeResume("-3month", 1440));
        exit();
        break;
    case 'getConsoBikeResumeUnAn':
        echo json_encode(getConsoBikeResume("-1year", 1440));
        exit();
        break;
    case 'getConsoBikeResumeCinqAns':
        echo json_encode(getConsoBikeResume("-5year", 1440));
        exit();
        break;
    case 'getDataConso':
        echo json_encode(getDataConso($dateResume, $longitude, $latitude));
        exit();
        break;
    case 'getDataStation':
        echo json_encode(getDataStation($codeStation));
        exit();
        break;
    case 'getCommunesCarte':
        echo json_encode(getCommunesCarte($dateResume));
        exit();
        break;
    case 'getDataTotem':
        echo json_encode(getDataTotem($codeStation));
        exit();
        break;
}

function getBikeInstantane($codeStation)
{
    $data = getDataBikeInstantane($codeStation);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => true),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataBikeInstantane($codeStation)
{
    global $pdo;

    //Filtre 1 heure
    $hier = new DateTime("-7days");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT c.date, s.nombre
    FROM comptage_status s
    INNER JOIN comptage_statusConso c ON c.id = s.idConso
    WHERE s.code = ' . $codeStation . ' AND c.date >= "' . $filtreDate . '"
    ORDER BY c.date ASC');
    $statusStation = $requete->fetchAll();

    $dates = [];
    $nbPassageData = [];
    foreach ($statusStation as $statut) {
        $dates[] = (new DateTime($statut['date']))->format("d/m H\hi");
        $nbPassageData[] = $statut['nombre'];
    }

    return array(
        'labels' => $dates,
        'datasets' => array(
            array(
                'label' => 'Nombre de passages',
                'backgroundColor' => 'rgba(104,221,46,0.5)',
                'data' => $nbPassageData,
            ),
        ),
    );
}

function getBikeResume($codeStation, $filtreDate, $periode)
{
    $data = getDataBikeResume($codeStation, $filtreDate, $periode);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => false),
            ),
        ),
    );
    return array(
        'type' => 'bar',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataBikeResume($codeStation, $filtre, $periode)
{
    global $pdo;

    //Filtre date
    $date = new DateTime($filtre);
    $filtreDate = $date->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT `date`, nombre
    FROM comptage_resumeStatus
    WHERE code = ' . $codeStation . ' AND `date` >= "' . $filtreDate . '" AND duree = ' . $periode . '
    ORDER BY date ASC');
    $resumeStatusStation = $requete->fetchAll();

    $datesResume = [];
    $nbPassageData = [];
    $displayYear = strpos($filtre, 'year') !== false;
    foreach ($resumeStatusStation as $statut) {
        $datesResume[] = (new DateTime($statut['date']))->format("d/m" . ($displayYear ? '/Y' : '') . " H\hi");
        $nbPassageData[] = $statut['nombre'];
    }

    return array(
        'labels' => $datesResume,
        'datasets' => array(
            array(
                'type' => 'line',
                'label' => 'Nombre de passages',
                'borderColor' => 'rgba(104,221,46,0.7)',
                'fill' => false,
                'data' => $nbPassageData,
            ),
        ),
    );
}

function getConsoBikeInstantane()
{
    $data = getDataConsoBikeInstantane();

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => true),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataConsoBikeInstantane()
{
    global $pdo;

    //Filtre 1 heure
    $hier = new DateTime("-7days");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT * FROM `comptage_statusConso` Where date >= "' . $filtreDate . '" Order by date asc');
    $allConso = $requete->fetchAll();
    $dates = [];
    $nbPassageData = [];
    foreach ($allConso as $i => $c) {
        if ($c['nbStation'] > 0) {
            $dates[] = (new DateTime($c['date']))->format("d/m H\hi");
            $nbPassageData[] = $c['nombre'];
        }
    }

    return array(
        'labels' => $dates,
        'datasets' => array(
            array(
                'label' => 'Nombre de passages',
                'backgroundColor' => 'rgba(104,221,46,0.5)',
                'data' => $nbPassageData,
            ),
        ),
    );
}

function getConsoBikeResume($filtreDate, $periode)
{
    $data = getDataConsoBikeResume($filtreDate, $periode);

    $dataReturn = array(
        'labels' => $data['labels'],
        'datasets' => $data['datasets'],
    );

    $options =
    array(
        'responsive' => false,
        'scales' => array(
            'yAxes' => array(
                array('stacked' => false),
            ),
        ),
    );
    return array(
        'type' => 'line',
        'data' => $dataReturn,
        'options' => $options,
    );
}

function getDataConsoBikeResume($filtre, $periode)
{
    global $pdo;

    //Filtre date
    $date = new DateTime($filtre);
    $filtreDate = $date->format('Y-m-d H:i:s');

    $requete = $pdo->query('SELECT *
    FROM comptage_resumeConso
    WHERE `date` >= "' . $filtreDate . '" AND duree = ' . $periode . '
    ORDER BY date ASC');
    $resumeStatusStation = $requete->fetchAll();

    $datesResume = [];
    $nbPassageData = [];
    $displayYear = strpos($filtre, 'year') !== false;
    foreach ($resumeStatusStation as $i => $c) {
        if ($c['nbStation'] > 0) {
            $datesResume[] = (new DateTime($c['date']))->format("d/m" . ($displayYear ? '/Y' : '') . " H\hi");
            $nbPassageData[] = $c['nombre'];
        }
    }

    return array(
        'labels' => $datesResume,
        'datasets' => array(
            array(
                'label' => 'Nombre de passages',
                'backgroundColor' => 'rgba(104,221,46,0.5)',
                'data' => $nbPassageData,
            ),
        ),
    );
}

function getDataConso($dateResume, $longitude = null, $latitude = null)
{
    global $pdo;
    $whereCoord = null;
    if (!is_null($longitude) && !is_null($latitude)) {
        $whereCoord = implode(' AND ', array(
            'stations.longitude >= ' . ($longitude - 0.015),
            'stations.longitude <= ' . ($longitude + 0.015),
            'stations.latitude >= ' . ($latitude - 0.01),
            'stations.latitude <= ' . ($latitude + 0.01),
        ));
    }

    return array('data' => getStatusByIdConso_comptage($dateResume, $whereCoord));
}

function getDataStation($codeStation)
{
    global $pdo;
    //Filtre 24 heures
    $hier = new DateTime("-7days");
    $filtreDate = $hier->format('Y-m-d H:i:s');

    //Stations
    $requete = $pdo->query('SELECT c.id, c.date, s.nombre
    FROM comptage_status s
    INNER JOIN comptage_statusConso c ON c.id = s.idConso
    WHERE s.code = ' . $codeStation . ' AND c.date >= "' . $filtreDate . '"
    ORDER BY c.date ASC');
    $statusStation = $requete->fetchAll(PDO::FETCH_ASSOC);
    $retour = [];
    foreach ($statusStation as $statut) {
        $retour[] = array(
            'idConso' => $statut['id'],
            'date' => $statut['date'],
            'nombre' => $statut['nombre'],
        );
    }
    return array('data' => $retour);
}

function getCommunesCarte($dateResume)
{
    global $pdo;

    $dataConso = getDataConso($dateResume);
    $statusStation = $dataConso['data'];

    $dataSvg = genererObjetsCarteSVG_comptage($statusStation);

    $svg = genererCarteSVG(800, 500, $dataSvg['communes'], '', '', '', $dataSvg['obj'], COMMUNE_AVM_VELIB);

    return $svg;
}

function getDataTotem($codeStation)
{
    global $pdo;

    // Il nous faut deux informations : le nombre de cycliste entre le 1er janvier et l'avant veille
    $premierJanvier = date('Y') . "-01-01 00:00:00";

    $avantHier = new DateTime("-2days");
    $filtreDateAvantHier = $avantHier->format('Y-m-d') . " 00:00:00";

    $requete = $pdo->query('SELECT SUM(nombre) as total
    FROM comptage_resumeStatus
    WHERE code = ' . $codeStation . ' AND `date` >= "' . $premierJanvier . '" AND date <= "' . $filtreDateAvantHier . '" AND duree = 1440');
    $data = $requete->fetch(PDO::FETCH_ASSOC);
    $totalAn = intval($data['total']);

    // Et la meilleure précision possible pour la veille
    $hier = new DateTime("-1days");
    $filtreDateHier = $hier->format('Y-m-d') . " 00:00:00";

    $requete = $pdo->query('SELECT c.date, s.nombre
    FROM comptage_status s
    INNER JOIN comptage_statusConso c ON c.id = s.idConso
    WHERE s.code = ' . $codeStation . ' AND c.date >= "' . $filtreDateHier . '"
    ORDER BY c.date ASC');
    $data = $requete->fetchAll(PDO::FETCH_ASSOC);

    $compteurHeure = array();
    foreach ($data as $item) {
        $dateComptage = new DateTime($item['date']);

        $compteurHeure[intval($dateComptage->format('H'))] = intval($item['nombre']);
    }

    return array(
        'totalAn' => $totalAn,
        'compteurHeure' => $compteurHeure,
    );
}
