import dumpLib
import datetime
import resumeLib
import os
import nettoyageLib

jour = datetime.datetime(2018, 9, 18)
dateMax = datetime.datetime(2020, 9, 18)
while jour <= dateMax:
    print("--- Jour traité : " + str(jour.strftime("%Y-%m-%d")))
    print("Nettoyage de la table station")
    dumpLib.cleanStationTable()
    print("Téléchargement du dump")
    nomFichier = dumpLib.downloadDumpData(jour)
    print("Importation du dump")
    dumpLib.importerDumpData(nomFichier)
    demain = jour + datetime.timedelta(days=1)
    print("Suppression du fichier dump")
    os.remove(nomFichier)
    print("Calcul des resumes")
    resumeLib.calculerToutResume(jour, demain)
    print("Nettoyage des données inutiles")
    nettoyageLib.nettoyerInstantanne(demain - datetime.timedelta(minutes=5))
    #nettoyageLib.nettoyerConso(demain, "duree <= 60")
    nettoyageLib.optimiserBDD()

    jour += datetime.timedelta(days=1)
