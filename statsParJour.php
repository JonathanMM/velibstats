<?php
require_once 'config.php';
include_once 'functions.php';
require_once 'libs/Smarty.class.php';
include_once 'includes/constantes.php';

//$smarty = new Smarty();

$minPeriode = new DateTime("2018-01-08");
$maxPeriode = new DateTime("2018-02-02");
$periodeEnCours = $minPeriode;

while ($periodeEnCours < $maxPeriode) {
    $h = intval($periodeEnCours->format('G'));
    $periodeEnCours->setTime($h - ($h % 6), 0);
    $filtreDate = $periodeEnCours->format('Y-m-d H:i:s');
    $maxDate = clone $periodeEnCours;
    $maxDate->add(new DateInterval('P1D'));
    $filtreMaxDate = $maxDate->format('Y-m-d H:i:s');

    //On récupère toutes les resume
    $requete = $pdo->query('SELECT r.*, s.insee
    FROM resumeStatus r
    INNER JOIN stations s ON s.code = r.code
    WHERE r.date >= "' . $filtreDate . '" AND r.date < "' . $filtreMaxDate . '" AND r.duree = 360 ORDER BY r.code, r.date ASC');
    $resumes = $requete->fetchAll();

    //On parcourt les résumes, et on agglomère par station
    $stations = array();
    $minDate = null;
    $maxDate = null;
    foreach ($resumes as $resume) {
        $codeStation = $resume['code'];
        if (!isset($stations[$codeStation])) {
            $stations[$codeStation] = array('pris' => 0, 'rendu' => 0, 'Epris' => 0, 'Erendu' => 0, 'bornesPerdues' => [], 'bornesPerduesMax' => 0, 'insee' => $resume['insee']);
        }

        $stations[$codeStation]['pris'] += $resume['nbBikePris'];
        $stations[$codeStation]['Epris'] += $resume['nbEBikePris'];
        $stations[$codeStation]['rendu'] += $resume['nbBikeRendu'];
        $stations[$codeStation]['Erendu'] += $resume['nbEBikeRendu'];
        if (isset($resume['nbEDockPerdusMoyenne']) && !is_null($resume['nbEDockPerdusMoyenne'])) {
            $stations[$codeStation]['bornesPerdues'][] = $resume['nbEDockPerdusMoyenne'];
        }

        if (isset($resume['nbEDockPerdusMax']) && !is_null($resume['nbEDockPerdusMax'])) {
            $stations[$codeStation]['bornesPerduesMax'] = max($resume['nbEDockPerdusMax'], $stations[$codeStation]['bornesPerduesMax']);
        }

        if ($minDate == null || $minDate > $resume['date']) {
            $minDate = $resume['date'];
        }

        if ($maxDate == null || $maxDate < $resume['date']) {
            $maxDate = $resume['date'];
        }

    }

    $minDateObj = new DateTime($minDate);
    $maxDateObj = new DateTime($maxDate);
    $maxDateObj->add(new DateInterval('PT6H')); //On ajoute 6 heures pour avoir la fin de la période
    /*$smarty->assign(array(
    'minDate' => $minDateObj->format('d/m à H:i'),
    'maxDate' => $maxDateObj->format('d/m à H:i')
    ));*/

    //On fait les stats sur les stations
    $stationsSansMouvement = 0;
    $totalPris = 0;
    $totalEPris = 0;
    $totalRendu = 0;
    $totalERendu = 0;
    $totalMoyenneBornesPerdues = 0;
    $totalMaxBornesPerdues = 0;
    $depts = array();
    foreach ($stations as $code => $stats) {
        if ($stats['pris'] + $stats['rendu'] == 0) {
            $stationsSansMouvement++;
        } else {
            $totalPris += $stats['pris'];
            $totalRendu += $stats['rendu'];
            $totalEPris += $stats['Epris'];
            $totalERendu += $stats['Erendu'];
        }

        $deptStation = floor($stats['insee'] / 1000);
        if (!isset($depts[$deptStation])) {
            $depts[$deptStation] = array('nb' => 1, 'nom' => (isset($nomDept[$deptStation]) ? $nomDept[$deptStation] : 'Inconnu'));
        } else {
            $depts[$deptStation]['nb']++;
        }

        if (count($stats['bornesPerdues']) > 0) {
            $totalMoyenneBornesPerdues += array_sum($stats['bornesPerdues']) / count($stats['bornesPerdues']);
        }

        $totalMaxBornesPerdues += $stats['bornesPerduesMax'];
    }

    echo $periodeEnCours->format('d/m') . ': Retirés : ' . ($totalPris + $totalEPris) . ' (mécanique : ' . $totalPris . ' | électrique : ' . $totalEPris . '); ' .
    'Rendus : ' . ($totalRendu + $totalERendu) . ' (mécanique : ' . $totalRendu . ' | électrique : ' . $totalERendu . '); ' .
    'Bornes inutilisables : ' . round($totalMoyenneBornesPerdues, 2) . ' (max : ' . $totalMaxBornesPerdues . '); Stations sans mouvements : ' . $stationsSansMouvement . '/' . count($stations) . "<br />";

    /*$smarty->assign(array(
    'totalPris' => $totalPris,
    'totalRendu' => $totalRendu,
    'totalStation' => count($stations),
    'stationsSansMouvement' => $stationsSansMouvement
    ));

    $smarty->assign(array(
    'departements' => $depts
    ));*/
    $periodeEnCours->add(new DateInterval('P1D'));
}

//$smarty->display('stats.tpl');
exit();
