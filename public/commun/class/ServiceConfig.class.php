<?php
class ServiceConfig
{
    public $dateOuverture;
    public $eBike;
    public $overflow;

    public function __construct(array $configToml)
    {
        $this->dateOuverture = $configToml['dateOuverture'];
        $this->eBike = $configToml['eBike'];
        $this->overflow = $configToml['overflow'];
    }
}
