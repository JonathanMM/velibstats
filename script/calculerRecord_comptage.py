from resumeLib_comptage import calculerToutResume
from comptageLib import nettoyerResume, nettoyerStatut, calculerFusion
import pymysql
import datetime
from config import getMysqlConnection

mysql = getMysqlConnection()
# On récupère la liste des stations déjà en base
requete = mysql.cursor()
requete.execute(
    'SELECT code FROM comptage_stations')
rep = requete.fetchall()
requete.close()
stations = []
for row in rep:
    codeStation = int(row[0])
    stations.append(codeStation)

# Pour chaque station, on va calculer son max
for code in stations:
    requete = mysql.cursor()
    requete.execute(
        'SELECT date, nombre FROM comptage_resumeStatus r JOIN (SELECT MAX(nombre) AS nb FROM comptage_resumeStatus where code = '+str(code)+' AND duree = 1440 order by date asc limit 0,1) max ON r.nombre = max.nb')
    rep = requete.fetchone()
    requete.close()
    print("Station "+str(code)+" : Le " +
          str(rep[0]) + ", "+str(rep[1])+" cyclistes")
    requete = mysql.cursor()
    requete.execute('INSERT INTO comptage_record (code, date, nombre) VALUES (' +
                    str(code)+', "'+str(rep[0])+'", '+str(rep[1])+')')
    requete.close()

mysql.commit()
mysql.close()
