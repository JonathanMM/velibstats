<?php
require_once '../../config.php';
include_once '../../functions.php';
require_once '../../libs/Smarty.class.php';

$smarty = new Smarty();

$smarty->assign(array(
    'pageId' => 'comptageBarometre',
));

$hierData = [];

$dateConso = null;
$isLive = true;
if (isset($_GET['date'])) {
    $dateConsoStr = htmlspecialchars($_GET['date']);
    $dateEstimation = DateTime::createFromFormat('Y-m-d', $dateConsoStr, new DateTimeZone('Europe/Paris'));
    $isLive = false;
} else {
    $dateEstimation = new DateTime('now', new DateTimeZone('Europe/Paris'));
}
$dateVeille = clone $dateEstimation;
$dateVeille->sub(new DateInterval('P1D'));
$dateLendemain = clone $dateEstimation;
$dateLendemain->add(new DateInterval('P1D'));

if (!$isLive) {
    $dateConso = new DateTime($dateConsoStr, new \DateTimeZone('Europe/Paris'));
    $lendemainConso = clone $dateConso;
    $lendemainConso->add(new DateInterval('P1D'));
    $requete = $pdo->query('SELECT date, TIME(date) as heure, nombre
FROM `comptage_statusConso`
WHERE date >= "' . ($dateConso->format('Y-m-d')) . '" AND date < "' . ($lendemainConso->format('Y-m-d')) . '"
ORDER BY date ASC');
    $donnees = $requete->fetchAll();

    foreach ($donnees as $donnee) {
        $jour = new DateTime($donnee['date'], new \DateTimeZone('Europe/Paris'));
        $heure = intval($jour->format('H'));
        $hierData[$heure] = array('t' => $donnee['heure'], 'y' => $donnee['nombre']);
    }
}

$cle = getTypeJour($dateEstimation->format("Y-m-d"));

$charts = array(
    20 => [],
    50 => [],
    80 => [],
    90 => [],
    95 => [],
    100 => []
);

$requete = $pdo->query('SELECT * FROM fond WHERE type = 1 AND periode = "' . $cle . '" ORDER BY heure ASC');
$donnees = $requete->fetchAll();

foreach ($donnees as $donnee) {
    $charts[20][] = array('t' => $donnee['heure'], 'y' => $donnee['centile20']);
    $charts[50][] = array('t' => $donnee['heure'], 'y' => $donnee['centile50']);
    $charts[80][] = array('t' => $donnee['heure'], 'y' => $donnee['centile80']);
    $charts[90][] = array('t' => $donnee['heure'], 'y' => $donnee['centile90']);
    $charts[95][] = array('t' => $donnee['heure'], 'y' => $donnee['centile95']);
    $charts[100][] = array('t' => $donnee['heure'], 'y' => $donnee['centile100']);
}

// Et on récupère les dernières estimations
$requete = $pdo->query('SELECT MIN(TIME(date)) as heure, AVG(estimation) as estimation
FROM `comptage_barometre`
WHERE date >= "' . ($dateEstimation->format('Y-m-d')) . '" AND date < "' . ($dateEstimation->format('Y-m-d')) . ' 23:59:00"
GROUP BY HOUR(date)
ORDER BY date ASC');
$donnees = $requete->fetchAll();

$estimationData = array();
foreach ($donnees as $donnee) {
    $estimationData[] = array('t' => $donnee['heure'], 'y' => intval($donnee['estimation']));
}

$smarty->assign(array(
    'centile20' => json_encode($charts[20]),
    'centile50' => json_encode($charts[50]),
    'centile80' => json_encode($charts[80]),
    'centile90' => json_encode($charts[90]),
    'centile95' => json_encode($charts[95]),
    'centile100' => json_encode($charts[100]),
    'estimation' => json_encode($estimationData),
    'dateJour' => $dateEstimation->format('d/m/Y'),
    'isLive' => $isLive,
    'dateVeille' => $dateVeille->format("Y-m-d"),
    'dateLendemain' => $dateLendemain->format("Y-m-d"),
));

if (!$isLive) {
    $smarty->assign(array(
        'hier' => json_encode($hierData),
        'dateHier' => $dateConso->format('d/m/Y'),
    ));
}

$smarty->display('tpl/barometre.tpl');
exit();
