import sqlite3
import pymysql
import datetime
import os
from config import getMysqlConnection, getURLDump
import urllib.request


def creerStationTable(c):
    c.execute('''
    CREATE TABLE `stations` (
    `code` int(11) NOT NULL,
    `name` varchar(256) NOT NULL,
    `latitude` decimal(16,14) NOT NULL,
    `longitude` decimal(16,14) NOT NULL,
    `type` varchar(256) NOT NULL,
    `dateOuverture` date DEFAULT NULL,
    `adresse` text,
    `insee` int(5) DEFAULT NULL
    );
    ''')


def val(valeur):
    if valeur is None:
        return 'NULL'
    return '"'+str(valeur)+'"'


def creerStationData(conn, mysql):
    c = conn.cursor()
    requete = mysql.cursor()
    requete.execute('''
    SELECT code, name, latitude, longitude, type, dateOuverture, adresse, insee
    FROM stations
    ''')
    stations = requete.fetchall()
    for station in stations:
        c.execute('INSERT INTO stations (code, name, latitude, longitude, type, dateOuverture, adresse, insee) VALUES \
        ('+str(station[0])+', "'+str(station[1])+'", '+str(station[2])+', '+str(station[3])+', "'+str(station[4])+'", '+val(station[5])+', "'+str(station[6])+'", '+val(station[7])+')')
    conn.commit()


def creerDumpData(dateDebut):
    dateDebut = dateDebut.replace(microsecond=0, second=0, minute=0, hour=0)
    dateDebutStr = dateDebut.strftime("%Y-%m-%d %H:%M:%S")
    dateDebutNomFichier = dateDebut.strftime("%Y-%m-%d")
    dateFin = dateDebut + datetime.timedelta(days=1)
    dateFinStr = dateFin.strftime("%Y-%m-%d %H:%M:%S")

    nomFichier = '../dump/'+dateDebutNomFichier + '-data.db'
    conn = sqlite3.connect(nomFichier)

    mysql = getMysqlConnection()

    # Creation des tables
    c = conn.cursor()
    creerStationTable(c)
    c.execute('''
    CREATE TABLE `status` (
    `id` int(11) NOT NULL,
    `code` int(11) NOT NULL,
    `idConso` int(11) NOT NULL,
    `state` varchar(32) NOT NULL,
    `nbBike` int(3) NOT NULL,
    `nbEBike` int(3) NOT NULL,
    `nbFreeEDock` int(3) NOT NULL,
    `nbEDock` int(3) NOT NULL,
    `nbBikeOverflow` int(3) NOT NULL,
    `nbEBikeOverflow` int(3) NOT NULL,
    `maxBikeOverflow` int(3) NOT NULL,
    `overflow` varchar(4) DEFAULT NULL,
    `overflowActivation` varchar(4) DEFAULT NULL
    );
    ''')
    c.execute('''
    CREATE TABLE `statusConso` (
    `id` int(11) NOT NULL,
    `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `nbStation` int(11) DEFAULT NULL,
    `nbStationDetecte` int(11) DEFAULT NULL,
    `nbBike` int(11) DEFAULT NULL,
    `nbEbike` int(11) DEFAULT NULL,
    `nbBikeOverflow` int(11) DEFAULT NULL,
    `nbEbikeOverflow` int(11) DEFAULT NULL,
    `nbFreeEDock` int(11) DEFAULT NULL,
    `nbEDock` int(11) DEFAULT NULL
    );
    ''')
    conn.commit()

    # On va récupérer chaque donnée de chaque table
    creerStationData(conn, mysql)

    c = conn.cursor()
    requete = mysql.cursor()
    requete.execute('SELECT id, date, nbStation, nbStationDetecte, nbBike, nbEbike, nbBikeOverflow, nbEbikeOverflow, nbFreeEDock, nbEDock \
    FROM statusConso \
    WHERE date >= "'+dateDebutStr+'" AND date < "'+dateFinStr+'"')
    stations = requete.fetchall()
    for station in stations:
        values = []
        for cell in station:
            values.append(val(cell))
        c.execute('INSERT INTO statusConso (id, date, nbStation, nbStationDetecte, nbBike, nbEbike, nbBikeOverflow, nbEbikeOverflow, nbFreeEDock, nbEDock) VALUES \
        ('+values[0]+', '+values[1]+', '+values[2]+', '+values[3]+', '+values[4]+', '+values[5]+', '+values[6]+', '+values[7]+', '+values[8]+', '+values[9]+')')
    conn.commit()

    c = conn.cursor()
    requete = mysql.cursor()
    requete.execute('SELECT s.id, s.code, s.idConso, s.state, s.nbBike, s.nbEBike, s.nbFreeEDock, s.nbEDock, s.nbBikeOverflow, s.nbEBikeOverflow, s.maxBikeOverflow, s.overflow, s.overflowActivation \
    FROM status s \
    INNER JOIN `statusConso` c ON c.id = s.idConso \
    WHERE (c.`date` >= "'+dateDebutStr+'" AND c.`date` < "'+dateFinStr+'")')
    statuts = requete.fetchall()
    for statut in statuts:
        values = []
        for cell in statut:
            values.append(val(cell))
        c.execute('INSERT INTO status (id, code, idConso, state, nbBike, nbEBike, nbFreeEDock, nbEDock, nbBikeOverflow, nbEBikeOverflow, maxBikeOverflow, overflow, overflowActivation) VALUES \
        ('+', '.join(values)+')')
    conn.commit()

    # Et on ferme le fichier
    c.close()


def creerDumpConso(dateDebut):
    dateDebut = dateDebut.replace(microsecond=0, second=0, minute=0, hour=0)
    dateDebutStr = dateDebut.strftime("%Y-%m-%d %H:%M:%S")
    dateDebutNomFichier = dateDebut.strftime("%Y-%m-%d")
    dateFin = dateDebut + datetime.timedelta(days=1)
    dateFinStr = dateFin.strftime("%Y-%m-%d %H:%M:%S")

    nomFichier = '../dump/'+dateDebutNomFichier + '-conso.db'
    conn = sqlite3.connect(nomFichier)

    mysql = getMysqlConnection()

    # Creation des tables
    c = conn.cursor()
    creerStationTable(c)
    c.execute('''
    CREATE TABLE `resumeConso` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `duree` int(4) NOT NULL,
  `nbStation` int(5) NOT NULL,
  `nbStationDetecte` int(5) DEFAULT NULL,
  `nbBikeMin` int(6) NOT NULL,
  `nbBikeMax` int(6) NOT NULL,
  `nbBikeMoyenne` decimal(8,2) NOT NULL,
  `nbEBikeMin` int(6) NOT NULL,
  `nbEBikeMax` int(6) NOT NULL,
  `nbEBikeMoyenne` decimal(8,2) NOT NULL,
  `nbBikeOverflowMin` int(6) NULL,
  `nbBikeOverflowMax` int(6) NULL,
  `nbBikeOverflowMoyenne` decimal(8,2) NULL,
  `nbEbikeOverflowMin` int(6) NULL,
  `nbEbikeOverflowMax` int(6) NULL,
  `nbEbikeOverflowMoyenne` decimal(8,2) NULL,
  `nbFreeEDockMin` int(6) NOT NULL,
  `nbFreeEDockMax` int(6) NOT NULL,
  `nbFreeEDockMoyenne` decimal(8, 2) NOT NULL,
  `nbEDock` int(6) NOT NULL
);
    ''')
    c.execute('''
    CREATE TABLE `resumeStatus` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `duree` int(4) NOT NULL,
  `nbBikeMin` int(3) NOT NULL,
  `nbBikeMax` int(3) NOT NULL,
  `nbBikeMoyenne` decimal(5,2) NOT NULL,
  `nbBikePris` int(3) NOT NULL,
  `nbBikeRendu` int(3) NOT NULL,
  `nbEBikeMin` int(3) NOT NULL,
  `nbEBikeMax` int(3) NOT NULL,
  `nbEBikeMoyenne` decimal(5,2) NOT NULL,
  `nbEBikePris` int(3) NOT NULL,
  `nbEBikeRendu` int(3) NOT NULL,
  `nbFreeEDockMin` int(3) NOT NULL,
  `nbFreeEDockMax` int(3) NOT NULL,
  `nbFreeEDockMoyenne` decimal(5,2) NOT NULL,
  `nbEDock` int(3) NOT NULL,
  `nbBikeOverflowMin` int(3) NOT NULL,
  `nbBikeOverflowMax` int(3) NOT NULL,
  `nbBikeOverflowMoyenne` decimal(5,2) NOT NULL,
  `nbEBikeOverflowMin` int(3) NOT NULL,
  `nbEBikeOverflowMax` int(3) NOT NULL,
  `nbEBikeOverflowMoyenne` decimal(5,2) NOT NULL,
  `maxBikeOverflow` int(3) NOT NULL,
  `nbEDockPerdusMin` int(3) DEFAULT NULL,
  `nbEDockPerdusMax` int(3) DEFAULT NULL,
  `nbEDockPerdusMoyenne` decimal(5,2) DEFAULT NULL
);
    ''')
    conn.commit()

    # On va récupérer chaque donnée de chaque table
    creerStationData(conn, mysql)
    c = conn.cursor()
    requete = mysql.cursor()
    requete.execute('SELECT id, date, duree, nbStation, nbStationDetecte, nbBikeMin, nbBikeMax, nbBikeMoyenne, nbEBikeMin, nbEBikeMax, nbEBikeMoyenne, nbBikeOverflowMin, nbBikeOverflowMax, nbBikeOverflowMoyenne, nbEbikeOverflowMin, nbEbikeOverflowMax, nbEbikeOverflowMoyenne, nbFreeEDockMin, nbFreeEDockMax, nbFreeEDockMoyenne, nbEDock \
    FROM resumeConso \
    WHERE date >= "'+dateDebutStr+'" AND date < "'+dateFinStr+'"')
    stations = requete.fetchall()
    for station in stations:
        values = []
        for cell in station:
            values.append(val(cell))
        c.execute('INSERT INTO resumeConso (id, date, duree, nbStation, nbStationDetecte, nbBikeMin, nbBikeMax, nbBikeMoyenne, nbEBikeMin, nbEBikeMax, nbEBikeMoyenne, nbBikeOverflowMin, nbBikeOverflowMax, nbBikeOverflowMoyenne, nbEbikeOverflowMin, nbEbikeOverflowMax, nbEbikeOverflowMoyenne, nbFreeEDockMin, nbFreeEDockMax, nbFreeEDockMoyenne, nbEDock) VALUES \
        ('+', '.join(values)+')')
    conn.commit()

    c = conn.cursor()
    requete = mysql.cursor()
    requete.execute('SELECT	id, code, date, duree, nbBikeMin, nbBikeMax, nbBikeMoyenne, nbBikePris, nbBikeRendu, nbEBikeMin, nbEBikeMax, nbEBikeMoyenne, nbEBikePris, nbEBikeRendu, nbFreeEDockMin, nbFreeEDockMax, nbFreeEDockMoyenne, nbEDock, nbBikeOverflowMin, nbBikeOverflowMax, nbBikeOverflowMoyenne, nbEBikeOverflowMin, nbEBikeOverflowMax, nbEBikeOverflowMoyenne, maxBikeOverflow, nbEDockPerdusMin, nbEDockPerdusMax, nbEDockPerdusMoyenne \
    FROM resumeStatus \
    WHERE date >= "'+dateDebutStr+'" AND date < "'+dateFinStr+'"')
    stations = requete.fetchall()
    for station in stations:
        values = []
        for cell in station:
            values.append(val(cell))
        c.execute('INSERT INTO resumeStatus (id, code, date, duree, nbBikeMin, nbBikeMax, nbBikeMoyenne, nbBikePris, nbBikeRendu, nbEBikeMin, nbEBikeMax, nbEBikeMoyenne, nbEBikePris, nbEBikeRendu, nbFreeEDockMin, nbFreeEDockMax, nbFreeEDockMoyenne, nbEDock, nbBikeOverflowMin, nbBikeOverflowMax, nbBikeOverflowMoyenne, nbEBikeOverflowMin, nbEBikeOverflowMax, nbEBikeOverflowMoyenne, maxBikeOverflow, nbEDockPerdusMin, nbEDockPerdusMax, nbEDockPerdusMoyenne) VALUES \
        ('+', '.join(values)+')')
    conn.commit()

    # Et on ferme le fichier
    c.close()


def importerDumpData(adresseDump):
    sqlite = sqlite3.connect(adresseDump)
    mysql = getMysqlConnection()
    importerTable("stations", mysql, sqlite)
    importerTable("status", mysql, sqlite)
    importerTable("statusConso", mysql, sqlite)


def importerDumpConso(adresseDump):
    sqlite = sqlite3.connect(adresseDump)
    mysql = getMysqlConnection()
    importerTable("stations", mysql, sqlite)
    importerTable("resumeStatus", mysql, sqlite)
    importerTable("resumeConso", mysql, sqlite)


def importerTable(nomTable, mysql, sqlite):
    print("Import de la table " + nomTable)
    # On récupère la liste des champs de stations
    c = sqlite.cursor()
    requete = c.execute("pragma table_info(" + nomTable + ")")
    fields = requete.fetchall()
    nomFields = []
    for field in fields:
        nomFields.append(field[1])

    # On récupère les données
    cDump = sqlite.cursor()
    requete = cDump.execute("SELECT "+', '.join(nomFields)+" FROM " + nomTable)
    valeurs = requete.fetchall()
    for valeur in valeurs:
        c = mysql.cursor()
        c.execute("INSERT INTO " + nomTable + " (" + ", ".join(nomFields) +
                  ") VALUES (" + ", ".join(list(map(val, valeur))) + ")")


def downloadDumpData(dateDump):
    nomFichier = dateDump.strftime("%Y-%m-%d") + '-data.db'
    urllib.request.urlretrieve(getURLDump() + nomFichier, nomFichier)
    return nomFichier


def downloadDumpConso(dateDump):
    nomFichier = dateDump.strftime("%Y-%m-%d") + '-conso.db'
    urllib.request.urlretrieve(getURLDump() + nomFichier, nomFichier)
    return nomFichier


def cleanStationTable():
    mysql = getMysqlConnection()
    c = mysql.cursor()
    c.execute("TRUNCATE stations")
