import comptageLib
import datetime
from resumeLib_comptage import calculerToutResume

# dateMin = datetime.datetime(2017, 12, 31)
# dateMax = datetime.datetime.combine(datetime.date.today(
# ), datetime.datetime.min.time())
dateMin = datetime.datetime(2020, 4, 3)
dateMax = datetime.datetime(2020, 4, 3) + datetime.timedelta(days=1)

print("******* Bornes *******")
print("Début (inclus) = "+str(dateMin))
print("Fin (exclu) = "+str(dateMax))
print("******* Bornes *******")

# On nettoie
comptageLib.nettoyerStatut(dateMin, dateMax)
comptageLib.nettoyerResume(dateMin, dateMax)

dateCourante = dateMin
while dateCourante < dateMax:
    urlDebut = "https://opendata.paris.fr/explore/dataset/comptage-velo-donnees-compteurs/download/?format=csv&disjunctive.id=true&disjunctive.name=true&timezone=Europe/Paris&use_labels_for_header=true&csv_separator=%3B"
    dateDebut = dateCourante.strftime("%Y-%m-%dT%H:%M:%SZ")
    print("Traitement de " + dateDebut)
    dateFin = (dateCourante + datetime.timedelta(days=1)
               ).strftime("%Y-%m-%dT%H:%M:%SZ")
    url = urlDebut + "&q=date:%5B" + dateDebut + "+TO+"+dateFin+"%5D"
    comptageLib.getAllStation(url)
    dateCourante += datetime.timedelta(days=1)

# Une fois fini, on calcule tous les résumes
calculerToutResume(dateMin, dateMax)
