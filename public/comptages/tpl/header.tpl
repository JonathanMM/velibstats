<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Comptages Nocle, site d'analyse et de mise en avant des comptages de cyclistes en Île-de-France.">
    <title>Comptages Nocle</title>
    <script type="application/javascript" src="js/Chart.min.js"></script>
    <script type="application/javascript" src="js/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>

    <script type="text/javascript" src="js/datatables.min.js"></script>
    <script type="text/javascript" src="js_comptage/script.js"></script> 
    <link rel="stylesheet" href="css/pure-min.css">
    
        <!--[if lte IE 8]>
            <link rel="stylesheet" href="css/layouts/side-menu-old-ie.css">
        <![endif]-->
        <!--[if gt IE 8]><!-->
            <link rel="stylesheet" href="css/layouts/side-menu.css">
        <!--<![endif]-->
</head>
<body>

<div id="layout" class="comptages">
    <!-- Menu toggle -->
    <a href="#menu" id="menuLink" class="menu-link">
        <!-- Hamburger icon -->
        <span></span>
    </a>

    <div id="menu">
        <div class="pure-menu">
            <a class="pure-menu-heading" href="index.php">Comptages Nocle</a>

            <ul class="pure-menu-list">
                <li class="pure-menu-item {if $pageId eq 'comptage'}pure-menu-selected{/if}"><a href="index.php" class="pure-menu-link">Comptages</a></li>
                <li class="pure-menu-item {if $pageId eq 'comptageBarometre'}pure-menu-selected{/if}"><a href="barometre.php" class="pure-menu-link">Baromètre</a></li>
                <li class="pure-menu-item"><a href="https://velib.nocle.fr" class="pure-menu-link">Vélib Nocle</a></li>
                <li class="pure-menu-item"><a href="https://cristolib.nocle.fr" class="pure-menu-link">CristoLib Nocle</a></li>
                <li class="pure-menu-item"><a href="https://velo2.nocle.fr" class="pure-menu-link">Vél'O2 Nocle</a></li>

                {* <li class="pure-menu-item menu-item-divided pure-menu-selected">
                    <a href="#" class="pure-menu-link">Services</a>
                </li>

                <li class="pure-menu-item"><a href="#" class="pure-menu-link">Contact</a></li> *}
            </ul>
        </div>
    </div>

    <div id="main">