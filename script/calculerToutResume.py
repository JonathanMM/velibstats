from resumeLib import calculerToutResume
import pymysql, datetime
from config import getMysqlConnection

#On commence par avoir la date min et max
mysql = getMysqlConnection()
requete = mysql.cursor()
requete.execute("SELECT MIN(date), MAX(date) FROM `statusConso`")
bornesDate = requete.fetchone()
dateMin = bornesDate[0].replace(microsecond = 0, second = 0)
dateMax = bornesDate[1].replace(microsecond = 0, second = 0)

print("******* Bornes *******")
print("Début = "+str(dateMin))
print("Fin = "+str(dateMax))
print("******* Bornes *******")

#Périodes à calculer
calculerToutResume(dateMin, dateMax)