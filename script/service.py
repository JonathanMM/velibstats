from serviceConfig import ServiceConfig


class Service:
    """Définition d'un service de location"""

    name = ""
    id = 0
    slug = ""
    config = None

    def __init__(self, configDict):
        self.name = configDict['name']
        self.id = configDict['id']
        self.slug = configDict['slug']
        self.config = ServiceConfig(configDict['config'])
